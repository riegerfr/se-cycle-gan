# todo: proper header
# Max Planck Institute of Neurobiology, Munich, Germany
# Authors: Anushka Vashishtha, Franz Rieger, Philipp Schubert
# Adapted from elektron3-dev/examples/train_unet_neurodata.py from 5be20bfd0ec041ee325bba880f35c8e7806af8b2
# based on: Segmentation-Enhanced CycleGAN
# Michał Januszewski, Viren Jain
# bioRxiv 548081; doi: https://doi.org/10.1101/548081

# At this moment, this file is a work in progress for the SECGAN training. todo: update


import logging
import os
import pprint
import random
import sys
from argparse import Namespace
from collections import deque
from datetime import datetime, timedelta
from typing import Any, Dict, Union, List

import elektronn3
import numpy as np
import torchvision
from elektronn3.data.knossos import KnossosRawData
from elektronn3.models.unet import UNet
from elektronn3.modules import DiceLoss, CombinedLoss
from elektronn3.training import metrics
from elektronn3.training.handlers import plot_image, get_cmap
from elektronn3.training.train_utils import Timer
from elektronn3.training.trainer import _worker_init_fn
from torch import nn
from torch.optim import SGD, Adam
from torch.utils import collect_env
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter

from parsing_old import parse_args
from secgan.generator_net import GeneratorNet
from secgan.resnet3d import ResNet3DFullyConvolutional
from secgan.dataset_specific.dataloading import get_transforms, load_labeled_dataset, tuple2bounds

from secgan.secgan_helper import calculate_num_parameters, subtract, \
    center_crop, detect_intensity_inversion
from shared import *

logger = logging.getLogger('se_cycle_gan_logs')


# torch.autograd.set_detect_anomaly(True)

# Notation:
# Dataset X: GT available, Segmentation Model S_X available (U-Net or FFN), Discriminator D_SX,
# Discriminator D_X (3D-ResNet-18)
# Dataset Y: no GT available, Discriminator D_Y
# Generator G_Y2X: Map from Y to X (8 Res Conv Modules)
# Generator G_X2Y: Map from X to Y
# x: batch from X, y: batch from Y
# x2y: G_X2Y(x), y2x: G_Y2X(y), y2x_2y: G_X2Y(y2x), d_y2x: D_X(y2x), s_y2x: S_X(y2x), d_s_y2x: D_SX(s_y2x)
# and so on

class cycleGAN_trainer:
    x2y_buffer: List[Any]
    y2x_buffer: List[Any]
    optimizer_G_X2Y: Union[Adam, SGD]
    device0: torch.device
    device1: torch.device
    multi_gpus: bool
    cycle_consistency_weight_increasing: bool
    current_cycle_consistency_lambda: float
    end_time: datetime
    start_time: datetime
    last_saved: datetime
    start_timestamp: str
    save_root: Union[Union[str, bytes], Any]
    optimizers_D_SX_D_X: Union[SGD, Adam]
    optimizer_D_Y: Union[SGD, Adam]
    optimizer_G_Y2X: Union[SGD, Adam]
    state: Dict[Union[str, Any], Union[Union[ResNet3DFullyConvolutional, GeneratorNet, UNet, SGD], Any]]
    G_X2Y: GeneratorNet
    G_Y2X: GeneratorNet
    D_X: ResNet3DFullyConvolutional
    D_Y: ResNet3DFullyConvolutional
    D_SX: ResNet3DFullyConvolutional
    S_X: UNet
    info: Dict[str, Union[int, None, str]]
    valid_loader_y: DataLoader
    valid_loader_x: DataLoader
    train_loader_y: DataLoader
    train_loader_x: DataLoader
    args: Namespace
    tb: SummaryWriter
    max_iter_epoch_train: int
    step: int
    epoch: int
    exp_name: str
    save_path: str  # Full path to where training files are stored
    tb_path: str
    max_iter_epoch_train: int
    max_iter_epoch_val: int
    inversion_count_train: deque

    def __init__(self, args: Namespace):
        self.setup_start_time = time.time()
        self.args = args

        self.setup_misc()

        # printing memory stats for all GPUs available
        self.print_memory_stats("init")

        self.train_loader_x, self.train_loader_y, self.valid_loader_x, self.valid_loader_y = \
            self.setup_dataloaders(self.args.patch_shape, self.args.batch_size)
        self.log_hparams()
        # todo: this would need to be called after training (https://github.com/pytorch/pytorch/issues/37738 )

        self.load_models_optimizers()

    def train(self):
        self.print_memory_stats("train")

        # keeping track on training time
        self.start_time = datetime.now()
        self.end_time = self.start_time + timedelta(seconds=self.args.max_runtime)

        # Saving initial state of the model
        torch.save(self.state, self.save_path + f"initial_model.pt")
        self.last_saved = self.start_time

        # time taken by each epoch during training and validation respectively
        epoch_train_times = []
        epoch_valid_times = []

        self.setup_logging_constant_samples()  # todo: put this in init?
        self.setup_segmenter_training_constant_samples_X()
        if self.args.gt_available:
            self.setup_ground_truth_constant_samples_Y()
        self.best_y_segmentation_loss = float('inf')  # smooth?

        self.compute_const_segmented_samples()

        self.setup_cycle_consistency_weight()
        self.print_memory_stats("seg_train/valid_x put to cpu")
        print(f"Start training. Setup took {time.time() - self.setup_start_time} seconds")

        try:
            for i in range(self.state['epoch'], self.args.num_epochs):  # todo: tqdm?
                timer_epoch = Timer()
                # todo: time whole epochs, including logging
                self.epoch = self.epoch + 1  # todo: count epoch here or with i?
                if self.args.verbose:
                    print("Epoch:", self.epoch)

                # todo: save checkpoints,
                #   select best (on segmentation metric like precision, F1?) afterwards
                #   for final save?

                # todo: set up logging instead of printing,
                #  LR scheduling on models (except for S_X)

                # todo: update discriminators based on a history of generated images/patches:
                #  https://arxiv.org/pdf/1703.10593.pdf
                # todo try dropout in generators/discriminators (for more stability?),
                #  AvgPooling instead of MaxPooling, etc. (ganhacks: https://github.com/soumith/ganhacks )

                if not self.run_epoch(self.train_loader_x, self.train_loader_y, is_training=True,
                                      # todo: only return after validation, logging, saving and time-checking!
                                      epoch_times=epoch_train_times,
                                      identity_loss=(i <= self.args.identity_loss_epochs)):
                    return False, self.best_y_segmentation_loss  # todo: this side-effect is not obvious

                self.gradient_logging()

                with torch.no_grad():
                    if not self.run_epoch(self.valid_loader_x, self.valid_loader_y, is_training=False,
                                          epoch_times=epoch_valid_times):
                        return False, self.best_y_segmentation_loss  # todo: delete folder?

                self.update_cycle_consistency_lambda()

                if self.args.gt_available:
                    self.log_ground_truth_loss(is_training_data=True)
                    self.log_ground_truth_loss(is_training_data=False)

                if i <= 5 or i % 10 == 0:  # to save memory
                    self.constant_sample_logging()

                # todo: add validation metrics

                self.save_state(i)
                self.tb.add_scalar("total_epoch_time", timer_epoch.t_passed, global_step=self.step)

                self.tb.flush()

                if datetime.now() + 2 * (
                        timedelta(seconds=(max(epoch_train_times) + max(epoch_valid_times)))) >= self.end_time:
                    print(f'max_runtime ({self.args.max_runtime} seconds) exceeded soon. Terminating...')
                    break
        except KeyboardInterrupt:  # todo: catch any exception?
            print("keyboard interrupt, saving model")
        #  Saving final state of the model
        # todo: add best validation losses (what is it for se cycle gan ?)
        self.log_hparams(current_loss=timer_epoch.t_passed)  # todo: pass proper loss
        # todo: don't log every epoch (or delete all but the last of the subfolders after final epoch/break)

        torch.save(self.state, self.save_path + f"final.pt")

        self.tb.close()
        self.tb.flush()

        return True, self.best_y_segmentation_loss

    def log_ground_truth_loss(self, is_training_data=True):
        # todo: use elektronn3.metrics (like in valid)
        # todo: log crossentropy and dice seperately
        gt_loss = 0
        dataloader = self.y_ground_truth_dataloader_train if is_training_data else self.y_ground_truth_dataloader_valid

        self.set_train_eval_mode(False)

        valid_metrics = {  # mean metrics
            'val_accuracy_mean': metrics.Accuracy(),
            'val_precision_mean': metrics.Precision(),
            'val_recall_mean': metrics.Recall(),
            'val_DSC_mean': metrics.DSC(),
            'val_IoU_mean': metrics.IoU(),
        }
        if self.args.number_classes > 2:
            # Add separate per-class accuracy metrics only if there are more than 2 classes
            valid_metrics.update({
                f'val_IoU_c{i}': metrics.Accuracy(i)
                for i in range(self.args.number_classes)
            })
        stats = {name: [] for name in valid_metrics.keys()}
        for criterion_name in self.y_ground_truth_criteria:
            stats['loss_' + criterion_name] = []

        for y_gt in dataloader:
            with torch.no_grad():
                y = y_gt['inp'].to(self.device0)
                if self.args.verbose:
                    print("y_gt shape:", y.shape)
                y2x = self.G_Y2X(y).to(self.select_device())
                s_y2x = self.S_X(y2x)

                for name, evaluator in valid_metrics.items():
                    # value = evaluator(*center_crop( label_predicted, label_gt))
                    value = evaluator(*center_crop(y_gt['target'].cpu(), s_y2x.cpu()))
                    stats[name].append(value)
                for criterion_name in self.y_ground_truth_criteria:
                    stats['loss_' + criterion_name].append(
                        self.y_ground_truth_criteria[criterion_name](
                            *center_crop(s_y2x.detach().cpu(), y_gt['target'].long().detach().cpu())))

        for name in stats.keys():
            stats[name] = np.nanmean(stats[name])
            self.tb.add_scalar(f'({"train" if is_training_data else "valid"}) groundtruth s(s_y2x) {name}',
                               stats[name],
                               global_step=self.step)
        if self.args.verbose:
            pprint.pprint(stats, indent=4, width=100, compact=False)

        if not is_training_data:
            if stats['loss_combined'] < self.best_y_segmentation_loss:
                self.best_y_segmentation_loss = stats['loss_combined']

    # todo: calculate loss on all s_y2x, history for discriminators, single step generators update

    def setup_segmenter_training_constant_samples_X(self):  # todo: pull this in a logging-file/class
        # todo: split up this method in logging / loss calculation just like setup_ground_truth_constant_samples_Y
        x_class_weights_normalised, train_dataset, valid_dataset, _ = load_labeled_dataset(
            datatype='cellorganelle',
            # todo: put these in args
            dataset_name='j0126',
            epoch_size_train=8,
            calc_stat=True,
            class_wt_mode='inverse', patch_shape=self.args.patch_shape_logging_segmenter_training_constant_samples_X,
            deactivate_random_transforms=False, label_names = self.args.label_names)
        crossentropy = nn.CrossEntropyLoss(
            weight=x_class_weights_normalised)
        dice = DiceLoss(apply_softmax=True,
                        weight=x_class_weights_normalised)  # todo: verify softmax is not applied in segmenter
        # todo: weights should be average?
        self.s_x_training_criterion = CombinedLoss([crossentropy, dice], weight=[0.5, 0.5],
                                                   )  # todo: get this from the unet_training instead of re-creating?

        train_loader = DataLoader(
            train_dataset, self.args.batch_size, shuffle=True,
            worker_init_fn=_worker_init_fn
        )
        valid_loader = DataLoader(
            valid_dataset, self.args.batch_size, shuffle=True,
            worker_init_fn=_worker_init_fn
        )
        self.x_preview_batch_train_from_segmenter_training = next(iter(train_loader))
        self.x_preview_batch_valid_from_segmenter_training = next(iter(valid_loader))
        self.assert_value_range(self.x_preview_batch_valid_from_segmenter_training['inp'])
        self.assert_value_range(self.x_preview_batch_train_from_segmenter_training['inp'])

    def setup_ground_truth_constant_samples_Y(self):
        # (Not required for now) would it make sense to calculate y_class_weights_normalised
        # (analogously to x_class_weights_normalised)?
        #  Then, the losses would be weighted accordingly (requires seperate s_training_criterion for y)

        y_class_weights_normalised, train_dataset, valid_dataset, _ = load_labeled_dataset(
            datatype='cellorganelle',
            dataset_name='j0251',
            epoch_size_train=self.args.ground_truth_epoch_size,  # todo: increase epoch size
            epoch_size_valid=self.args.ground_truth_epoch_size,
            calc_stat=True,
            class_wt_mode='inverse', label_offset=self.args.label_offset,
            knossos_bounds={'train_bounds': self.args.train_bounds,
                            'valid_bounds': self.args.valid_bounds}, patch_shape=self.args.patch_shape_logging,
            deactivate_random_transforms=False, label_order=self.args.label_order, label_names = self.args.label_names)

        crossentropy = nn.CrossEntropyLoss(
            weight=y_class_weights_normalised)
        dice = DiceLoss(apply_softmax=True,
                        weight=y_class_weights_normalised)  # todo: verify softmax is not applied in segmenter
        # todo: weights should be average?

        combined = CombinedLoss([crossentropy, dice], weight=[0.5, 0.5])
        self.y_ground_truth_criteria = {"crossentropy": crossentropy, "dice": dice, "combined": combined}

        train_loader = DataLoader(
            train_dataset, self.args.batch_size, shuffle=True,
            worker_init_fn=_worker_init_fn
        )
        self.y_constant_preview_batch_ground_truth_train = next(iter(train_loader))
        self.assert_value_range(self.y_constant_preview_batch_ground_truth_train['inp'])

        self.y_ground_truth_dataloader_train = DataLoader(train_dataset, batch_size=self.args.batch_size,
                                                          drop_last=True, num_workers=self.args.num_workers,
                                                          worker_init_fn=_worker_init_fn)
        self.y_ground_truth_dataloader_valid = DataLoader(valid_dataset, batch_size=self.args.batch_size,
                                                          drop_last=True, num_workers=self.args.num_workers,
                                                          worker_init_fn=_worker_init_fn)

    def gradient_logging(self):
        self.gradient_tb_logging(self.G_X2Y, 'G_X2Y')
        self.gradient_tb_logging(self.G_Y2X, 'G_Y2X')
        self.gradient_tb_logging(self.D_X, 'D_X')
        self.gradient_tb_logging(self.D_SX, 'D_SX')
        self.gradient_tb_logging(self.D_Y, 'D_Y')

    def setup_cycle_consistency_weight(self):
        assert self.args.cycle_consistency_lambda_min > 0
        # exponential increase will never lead to values >0 (todo: introduce option for linear scheduling instead?)
        self.current_cycle_consistency_lambda = self.args.cycle_consistency_lambda_min
        # start with low weight on cycle consistency loss to avoid intensity inversion
        self.cycle_consistency_weight_increasing = True

    def compute_const_segmented_samples(self):
        try:
            # calculated only once
            with torch.no_grad():
                self.seg_train_x = self.S_X(
                    self.const_sample_train_x.to(self.select_device())).detach().cpu()
                self.seg_valid_x = self.S_X(
                    self.const_sample_valid_x.to(self.select_device())).detach().cpu()
                self.seg_train_x_from_segmenter_training = self.S_X(
                    self.x_preview_batch_train_from_segmenter_training['inp'].to(self.select_device())).detach().cpu()
                self.seg_valid_x_from_segmenter_training = self.S_X(
                    self.x_preview_batch_valid_from_segmenter_training['inp'].to(self.select_device())).detach().cpu()
        except RuntimeError as e:
            print(e)

    def constant_sample_logging(self):

        timer = Timer()
        # todo: all images in one grid?
        self.set_train_eval_mode(False)
        self.image_tb_logging_x(self.const_sample_train_x, is_training=True, seg_data_sample=self.seg_train_x,
                                ground_truth=None)
        self.image_tb_logging_x(self.x_preview_batch_train_from_segmenter_training['inp'], is_training=True,
                                seg_data_sample=self.seg_train_x_from_segmenter_training,
                                ground_truth=self.x_preview_batch_train_from_segmenter_training['target'])
        self.image_tb_logging_x(self.const_sample_valid_x, is_training=False, seg_data_sample=self.seg_valid_x,
                                ground_truth=None)
        self.image_tb_logging_x(self.x_preview_batch_valid_from_segmenter_training['inp'], is_training=False,
                                seg_data_sample=self.seg_valid_x_from_segmenter_training,
                                ground_truth=self.x_preview_batch_valid_from_segmenter_training['target'])
        self.image_tb_logging_y(self.const_sample_train_y, is_training=True,
                                ground_truth=None)
        # todo: just log those y with labels? (i.e. delete this and the following line)?
        self.image_tb_logging_y(self.const_sample_valid_y, is_training=False, ground_truth=None)
        if self.args.gt_available:
            self.image_tb_logging_y(self.y_constant_preview_batch_ground_truth_train['inp'], is_training=True,
                                    ground_truth=self.y_constant_preview_batch_ground_truth_train['target'])
        self.tb.add_scalar("constant_sample_logging_time", timer.t_passed, global_step=self.step)

    def save_state(self, i):
        if datetime.now() >= self.last_saved + timedelta(
                hours=self.args.saving_duration):
            # save after given args hours
            self.last_saved = datetime.now()
            current_state = self.state
            current_state['epoch'] = i
            torch.save(current_state,
                       self.save_path + f"epoch_{i}.pt")

    def setup_logging_constant_samples(self):
        train_loader_x, train_loader_y, valid_loader_x, valid_loader_y = \
            self.setup_dataloaders(self.args.patch_shape_logging, self.args.batch_size_logging)

        iter_train_x = iter(train_loader_x)
        iter_train_y = iter(train_loader_y)
        iter_valid_x = iter(valid_loader_x)
        iter_valid_y = iter(valid_loader_y)
        # todo: introduce option to load saved constant samples
        self.const_sample_train_x = next(iter_train_x)[
            "inp"]  # todo: maybe use elektronn3.data.get_preview_batch instead
        self.const_sample_train_y = next(iter_train_y)["inp"]
        self.const_sample_valid_x = next(iter_valid_x)[
            "inp"]  # put on gpu when image tb logging is called
        self.const_sample_valid_y = next(iter_valid_y)["inp"]
        self.info['const_sample_train_x'] = self.const_sample_train_x
        self.info['const_sample_train_y'] = self.const_sample_train_y
        self.info['const_sample_valid_x'] = self.const_sample_valid_x
        self.info['const_sample_valid_y'] = self.const_sample_valid_y

    def run_epoch(self, loader_x, loader_y, is_training, epoch_times, identity_loss=False):

        timer_epoch = Timer()
        loading_time_counter = TimeCounter()  # for dataloading

        self.set_train_eval_mode(is_training)

        iter_x = iter(loader_x)
        iter_y = iter(loader_y)

        epoch_value_logger = EpochValueLogger()

        for j in range(self.max_iter_epoch_train if is_training else self.max_iter_epoch_val):
            # todo: data loader might run out early if not divisible by 3 (as 3 batches of x and y are called in each
            #  iteration)
            try:

                if is_training:
                    self.step = self.step + 1

                if self.args.combined_mode:

                    loss_D_SX_D_X, loss_D_Y, x, y = self.step_discriminators(epoch_value_logger, is_training, iter_x,
                                                                             iter_y, loading_time_counter)
                    loss_G_Y2X, loss_G_X2Y = self.step_generators(epoch_value_logger, is_training, iter_x, iter_y,
                                                                  loading_time_counter,
                                                                  identity_loss=identity_loss)

                else:

                    loss_G_X2Y = self.step_1(epoch_value_logger, is_training, iter_y, loading_time_counter,
                                             identity_loss=identity_loss)
                    loss_D_SX_D_X = self.step_2(epoch_value_logger, is_training, iter_x, iter_y, loading_time_counter)
                    loss_G_Y2X, x = self.step_3(epoch_value_logger, is_training, iter_x, loading_time_counter,
                                                identity_loss=identity_loss)
                    loss_D_Y, y = self.step_4(epoch_value_logger, is_training, iter_x, iter_y, loading_time_counter)

                if self.args.verbose:
                    print(f"iteration {j} generator x to y loss: {loss_G_X2Y} ")
                    print(f"iteration {j} discriminator loss Ds and Dx: {loss_D_SX_D_X} ")
                    print(f"iteration {j} generator y to x loss: {loss_G_Y2X} ")
                    print(f"iteration {j} discriminator loss Dy: {loss_D_Y} ")
                    print(f"x, y: min/max {x.min()}/{x.max()}, {y.min()}/{y.max()}")

            except StopIteration:
                print("Break epoch due to stop iteration, this should't happen. Make sure dataloaders are big enough")
                break

        # memory summary after each epoch
        if self.args.memory_stats:
            print('summary device 0', torch.cuda.memory_summary(self.device0))
            if self.multi_gpus:
                print('summary device 1', torch.cuda.memory_summary(self.device1))

        # self.assert_value_range(x)
        # self.assert_value_range(y)

        if is_training: self.loss_grad_logging(x.to(self.device0))

        self.log_losses_timings(epoch_value_logger, j, epoch_times, is_training, loading_time_counter, timer_epoch)

        # if intensity inversion is detected self.args.inversion_threshold % of the time in the last 10 epochs
        #  then restart training
        if is_training:
            intensity_inversion_ratio = epoch_value_logger.epoch_intensity_inversion_ratio / (j + 1)
            self.inversion_count_train.append(intensity_inversion_ratio)
            if len(self.inversion_count_train) >= self.args.inversion_epochs:
                if np.array(self.inversion_count_train).mean() > self.args.inversion_threshold:
                    print("restarting training due to intensity inversion")
                    return False
                self.inversion_count_train.popleft()

        return True  # epoch ran successfully

    def loss_grad_logging(self, x):
        mode = "training"
        x2y = self.G_X2Y(x)
        x2y.retain_grad()
        x2y_2x = self.G_Y2X(x2y)
        d_x2y = self.D_Y(x2y)
        x2y_2x, x = center_crop(x2y_2x, x)
        cycle_consistency_loss = torch.abs(x2y_2x - x).mean()
        discriminator_loss = torch.pow(d_x2y - 1, 2).mean()
        loss_G_Y2X = self.current_cycle_consistency_lambda * cycle_consistency_loss + \
                     self.args.discriminator_loss_weight * discriminator_loss

        x2y.grad = None
        self.G_X2Y.zero_grad()
        self.G_Y2X.zero_grad()
        self.D_Y.zero_grad()
        cycle_consistency_loss.backward(retain_graph=True)
        abs_grad_mean_cycle_consistency_x2y = x2y.grad.abs().mean().detach().cpu().numpy()
        self.gradient_tb_logging(self.G_X2Y, "G_Y2X only cycle consistency")

        x2y.grad = None
        self.G_X2Y.zero_grad()
        self.G_Y2X.zero_grad()
        self.D_Y.zero_grad()
        discriminator_loss.backward(retain_graph=True)
        abs_grad_mean_discriminator_x2y = x2y.grad.abs().mean().detach().cpu().numpy()
        self.gradient_tb_logging(self.G_X2Y, "G_Y2X only discriminator loss")

        x2y.grad = None
        self.G_X2Y.zero_grad()
        self.G_Y2X.zero_grad()
        self.D_Y.zero_grad()
        loss_G_Y2X.backward(retain_graph=True)
        abs_grad_mean_total_loss_x2y = x2y.grad.abs().mean().detach().cpu().numpy()
        self.gradient_tb_logging(self.G_X2Y, "G_Y2X cycle consitency + discriminator")

        if self.args.verbose:
            print(f"{mode} mean of absolute gradient on x2y from cycle_consistency_loss:"
                  f" {abs_grad_mean_cycle_consistency_x2y}, from discriminator_loss: {abs_grad_mean_discriminator_x2y},"
                  f" from total_loss:{abs_grad_mean_total_loss_x2y}")

        self.tb.add_scalar(f'{mode} mean absolute gradient x2y_2x cycle_consistency_loss',
                           abs_grad_mean_cycle_consistency_x2y,
                           global_step=self.step)
        self.tb.add_scalar(f'{mode} mean absolute gradient x2y discriminator_loss',
                           abs_grad_mean_discriminator_x2y,
                           global_step=self.step)
        self.tb.add_scalar(f'{mode} mean absolute gradient x2y total loss',
                           abs_grad_mean_total_loss_x2y,
                           global_step=self.step)

    def assert_value_range(self, x):
        if not self.args.skip_asserts:
            assert x.min() >= 0  # or greater (not equal)?
            assert x.max() <= 1
            assert x.max() > x.min()  # otherwise everything has same intensity (does it make sense to learn here?)

    def log_losses_timings(self, epoch_value_logger, j, epoch_times, is_training, loading_time_counter, timer_epoch):
        # todo: simplify this function
        average_loss_D_SX_D_X = float(
            epoch_value_logger.epoch_loss_D_SX_D_X / (j + 1))  # this assumes that all batches have same size
        average_loss_D_Y = float(epoch_value_logger.epoch_loss_D_Y / (j + 1))
        average_loss_D_X = float(epoch_value_logger.epoch_loss_D_X / (j + 1))
        average_loss_D_SX = float(epoch_value_logger.epoch_loss_D_SX / (j + 1))
        average_loss_G_X2Y = float(epoch_value_logger.epoch_loss_G_X2Y / (j + 1))
        average_loss_G_Y2X = float(epoch_value_logger.epoch_loss_G_Y2X / (j + 1))
        average_dx_x = float(epoch_value_logger.epoch_dx_x / (j + 1))
        average_dx_y2x = float(epoch_value_logger.epoch_dx_y2x / (j + 1))
        average_dy_y = float(epoch_value_logger.epoch_dy_y / (j + 1))
        average_dy_x2y = float(epoch_value_logger.epoch_dy_x2y / (j + 1))
        average_ds_s_x = float(epoch_value_logger.epoch_ds_s_x / (j + 1))
        average_ds_s_y2x = float(epoch_value_logger.epoch_ds_s_y2x / (j + 1))
        average_G_Y2X_cycle_consistency_loss = float(epoch_value_logger.epoch_G_Y2X_cycle_consistency_loss / (j + 1))
        average_G_X2Y_from_discriminator_loss = float(epoch_value_logger.epoch_G_X2Y_from_discriminator_loss / (j + 1))
        average_G_X2Y_cycle_consistency_loss = float(epoch_value_logger.epoch_G_X2Y_cycle_consistency_loss / (j + 1))
        average_G_Y2X_from_discriminator_loss_x = float(
            epoch_value_logger.epoch_G_Y2X_from_discriminator_loss_x / (j + 1))
        average_G_Y2X_from_discriminator_loss_sx = float(
            epoch_value_logger.epoch_G_Y2X_from_discriminator_loss_sx / (j + 1))
        average_intensity_inversion_ratio = float(epoch_value_logger.epoch_intensity_inversion_ratio / (j + 1))
        average_epoch_loss_D = float(epoch_value_logger.epoch_loss_D / (j + 1))
        average_epoch_loss_G = float(epoch_value_logger.epoch_loss_G / (j + 1))

        mode = "training" if is_training else "validation"
        passed_time = timer_epoch.t_passed
        epoch_times.append(passed_time)
        dataloading_time = loading_time_counter.counted_time
        ratio = dataloading_time * 1.0 / passed_time
        print(
            f"epoch {self.epoch}/{self.args.num_epochs} ({mode}) losses: D_SX {average_loss_D_SX_D_X:.4f}, "
            f"D_Y {average_loss_D_Y:.4f}, G_X2Y {average_loss_G_X2Y:.4f}, G_Y2X {average_loss_G_Y2X:.4f}, "
            f"time passed: {passed_time:.4f},"
            f" of that dataloading time: {dataloading_time:.4f},"
            f" ratio: {ratio:.4f}")
        # Adding values to tensorboard
        self.tb.add_scalar(f'Step 1: Generator loss ({mode}) x to y', average_loss_G_X2Y,
                           global_step=self.step)
        self.tb.add_scalar(f'Step 2: Discriminator loss ({mode}) Ds and Dx', average_loss_D_SX_D_X,
                           global_step=self.step)
        self.tb.add_scalar(f'Discriminator loss ({mode}) Ds', average_loss_D_SX, global_step=self.step)
        self.tb.add_scalar(f'Discriminator loss ({mode}) Dx', average_loss_D_X, global_step=self.step)
        self.tb.add_scalar(f'Step 3: Generator loss ({mode}) y to x', average_loss_G_Y2X, global_step=self.step)
        self.tb.add_scalar(f'Step 4: Discriminator loss ({mode}) Dy', average_loss_D_Y, global_step=self.step)
        self.tb.add_scalar(f'Passed time total epoch ({mode})', passed_time, global_step=self.step)
        self.tb.add_scalar(f'Dataloading time epoch ({mode})', dataloading_time, global_step=self.step)
        self.tb.add_scalar(f'Ratio dataloading time ({mode})', ratio, global_step=self.step)
        self.tb.add_scalar(f'average_dx_x ({mode})', average_dx_x, global_step=self.step)
        self.tb.add_scalar(f'average_dx_y2x ({mode})', average_dx_y2x, global_step=self.step)
        self.tb.add_scalar(f'average_dy_y ({mode})', average_dy_y, global_step=self.step)
        self.tb.add_scalar(f'average_dy_x2y ({mode})', average_dy_x2y, global_step=self.step)
        self.tb.add_scalar(f'average_ds_s_x ({mode})', average_ds_s_x, global_step=self.step)
        self.tb.add_scalar(f'average_ds_s_y2x ({mode})', average_ds_s_y2x, global_step=self.step)
        self.tb.add_scalar(f'average_G_Y2X_cycle_consistency_loss ({mode})', average_G_Y2X_cycle_consistency_loss,
                           global_step=self.step)
        self.tb.add_scalar(f'average_G_X2Y_from_discriminator_loss ({mode})', average_G_X2Y_from_discriminator_loss,
                           global_step=self.step)
        self.tb.add_scalar(f'average_G_X2Y_cycle_consistency_loss ({mode})', average_G_X2Y_cycle_consistency_loss,
                           global_step=self.step)
        self.tb.add_scalar(f'average_G_Y2X_from_discriminator_loss_x ({mode})', average_G_Y2X_from_discriminator_loss_x,
                           global_step=self.step)
        self.tb.add_scalar(f'average_G_Y2X_from_discriminator_loss_sx ({mode})',
                           average_G_Y2X_from_discriminator_loss_sx, global_step=self.step)
        self.tb.add_scalar(f'average_intensity_inversion_ratio ({mode})',
                           average_intensity_inversion_ratio, global_step=self.step)
        self.tb.add_scalar(f'average_epoch_loss_D ({mode})', average_epoch_loss_D, global_step=self.step)
        self.tb.add_scalar(f'average_epoch_loss_G ({mode})', average_epoch_loss_G, global_step=self.step)
        self.tb.add_scalar(f'current_cycle_consistency_weight ({mode})',
                           self.current_cycle_consistency_lambda, global_step=self.step)

    def set_train_eval_mode(self, is_training):
        if is_training:
            self.G_X2Y.train()
            self.G_Y2X.train()
            self.D_X.train()
            self.D_SX.train()
            self.D_Y.train()

        else:
            self.G_X2Y.eval()
            self.G_Y2X.eval()
            self.D_X.eval()
            self.D_SX.eval()
            self.D_Y.eval()
        self.S_X.eval()  # S_X is not trained

    def add_get_buffer(self, elem, buffer):
        if self.args.discriminator_buffer_size > 1:
            buffer.append(elem.cpu().detach())
            random.shuffle(buffer)
            if len(buffer) > self.args.discriminator_buffer_size:
                elem = buffer.pop()
            else:
                elem = buffer[0]
            assert len(buffer) <= self.args.discriminator_buffer_size
            elem = elem.to(self.device0)
        return elem

    def step_4(self, epoch_value_logger, is_training, iter_x, iter_y, loading_time_counter):
        # Step 4: Discriminator loss Dy
        self.print_memory_stats("before step 4")
        x = timed_next(iter_x, loading_time_counter)["inp"].to(self.device0)
        y = timed_next(iter_y, loading_time_counter)["inp"].to(self.device0)
        x2y = self.G_X2Y(x)

        x2y = self.add_get_buffer(x2y, self.x2y_buffer)

        y, x2y = center_crop(y, x2y)
        dy_x2y = self.D_Y(x2y)
        dy_y = self.D_Y(y)
        loss_D_Y = torch.pow(dy_x2y - 0, 2).mean() + torch.pow(dy_y - 1, 2).mean()  # log std as well?
        if is_training:
            self.G_X2Y.zero_grad()
            self.D_Y.zero_grad()
            loss_D_Y.backward()
            self.optimizer_D_Y.step()
        epoch_value_logger.epoch_loss_D_Y += loss_D_Y
        epoch_value_logger.epoch_dy_y += dy_y.mean()
        epoch_value_logger.epoch_dy_x2y += dy_x2y.mean()
        return loss_D_Y.detach().cpu(), y.detach().cpu()

    def step_3(self, epoch_value_logger, is_training, iter_x, loading_time_counter, identity_loss=False):
        # Step 3: : Generator loss x to y
        self.print_memory_stats("before step 3")
        x = timed_next(iter_x, loading_time_counter)["inp"].to(self.device0)
        x2y = self.G_X2Y(x)
        x2y_2x = self.G_Y2X(x2y)
        d_x2y = self.D_Y(x2y)
        cycle_consistency_loss = torch.abs(subtract(*center_crop(x2y_2x, x))).mean()
        discriminator_loss = torch.pow(d_x2y - 1, 2).mean()
        loss_G_X2Y = (self.current_cycle_consistency_lambda * cycle_consistency_loss) + (
                self.args.discriminator_loss_weight * discriminator_loss)
        if identity_loss:
            identity_loss = torch.abs(subtract(*center_crop(x, x2y))).mean()
            loss_G_X2Y = loss_G_X2Y + self.current_cycle_consistency_lambda * 10 * identity_loss

        # todo: try L2 distance instead of L1?
        # todo: verify whether same lambda should be used, if not, perform hyperparam. search
        if is_training:
            self.G_X2Y.zero_grad()
            self.G_Y2X.zero_grad()
            self.D_Y.zero_grad()
            loss_G_X2Y.backward()
            self.optimizer_G_X2Y.step()
        epoch_value_logger.epoch_intensity_inversion_ratio += detect_intensity_inversion(x2y, x2y_2x)
        epoch_value_logger.epoch_loss_G_X2Y += loss_G_X2Y
        epoch_value_logger.epoch_G_X2Y_cycle_consistency_loss += cycle_consistency_loss
        epoch_value_logger.epoch_G_X2Y_from_discriminator_loss += discriminator_loss
        return loss_G_X2Y.detach().cpu(), x.detach().cpu()

    def step_2(self, epoch_value_logger, is_training, iter_x,
               iter_y, loading_time_counter):
        # Step 2: Discriminator losses Ds and Dx
        self.print_memory_stats("before step 2")
        x = timed_next(iter_x, loading_time_counter)["inp"].to(self.device0)
        y = timed_next(iter_y, loading_time_counter)["inp"].to(self.device0)
        y2x = self.G_Y2X(y)

        y2x = self.add_get_buffer(y2x, self.y2x_buffer)

        x, y2x = center_crop(x, y2x)
        s_y2x = self.S_X(y2x.to(self.select_device()))
        s_x = self.S_X(x.to(self.select_device()))
        ds_s_y2x = self.D_SX(s_y2x.to(self.device0))
        # todo: use same nomenclature for variables everywhere (in all steps and tb logging)
        dx_y2x = self.D_X(y2x)
        dx_x = self.D_X(x)
        ds_s_x = self.D_SX(s_x.to(self.device0))
        if self.args.verbose:
            print(f"dx_y2x min/max: {dx_y2x.max():.4f}, {dx_y2x.min():.4f}")
            print(f"dx_x min/max: {dx_x.max():.4f}, {dx_x.min():.4f}")
            print(f"d_s_y2x min/max: {ds_s_y2x.max():.4f}, {ds_s_y2x.min():.4f}")
            print(f"d_s_x min/max: {ds_s_x.max():.4f}, {ds_s_x.min():.4f}")
        loss_D_X = torch.pow(dx_y2x - 0, 2).mean() + torch.pow(dx_x - 1, 2).mean()
        loss_D_SX = torch.pow(ds_s_y2x - 0, 2).mean() + torch.pow(ds_s_x - 1, 2).mean()
        loss_D_SX_D_X = loss_D_X + loss_D_SX

        if is_training:
            self.G_Y2X.zero_grad()
            self.D_SX.zero_grad()
            self.D_X.zero_grad()
            self.S_X.zero_grad()
            loss_D_SX_D_X.backward()
            self.optimizers_D_SX_D_X.step()
        epoch_value_logger.epoch_loss_D_SX_D_X += loss_D_SX_D_X
        epoch_value_logger.epoch_loss_D_SX += loss_D_SX
        epoch_value_logger.epoch_loss_D_X += loss_D_X
        epoch_value_logger.epoch_ds_s_x += ds_s_x.mean()
        epoch_value_logger.epoch_ds_s_y2x += ds_s_y2x.mean()
        epoch_value_logger.epoch_dx_x += dx_x.mean()
        epoch_value_logger.epoch_dx_y2x += dx_y2x.mean()
        return loss_D_SX_D_X.detach().cpu()

    def step_1(self, epoch_value_logger, is_training, iter_y, loading_time_counter, identity_loss=False):
        # Step 1: Generator loss y to x
        self.print_memory_stats("before step 1")
        y = timed_next(iter_y, loading_time_counter)["inp"].to(self.device0)
        y2x = self.G_Y2X(y)
        y2x_2y = self.G_X2Y(y2x)
        d_y2x = self.D_X(y2x)
        s_y2x = self.S_X(y2x.to(self.select_device()))
        d_s_y2x = self.D_SX(s_y2x.to(self.device0))
        cycle_consistency_loss = torch.abs(subtract(*center_crop(y2x_2y, y))).mean()
        from_discriminator_loss_x = torch.pow(d_y2x - 1, 2).mean()
        from_discriminator_loss_sx = torch.pow(d_s_y2x - 1, 2).mean()

        loss_G_Y2X = (self.current_cycle_consistency_lambda * cycle_consistency_loss) + (
                self.args.discriminator_loss_weight * from_discriminator_loss_x) + (
                             self.args.discriminator_loss_weight * from_discriminator_loss_sx)
        if identity_loss:
            identity_loss = torch.abs(subtract(*center_crop(y, y2x))).mean()
            loss_G_Y2X = loss_G_Y2X + self.current_cycle_consistency_lambda * 10 * identity_loss
        # todo: log different loss parts separately
        if is_training:
            self.G_Y2X.zero_grad()
            self.G_X2Y.zero_grad()
            self.D_X.zero_grad()
            self.S_X.zero_grad()
            self.D_SX.zero_grad()
            loss_G_Y2X.backward()
            self.optimizer_G_Y2X.step()

        epoch_value_logger.epoch_loss_G_Y2X += loss_G_Y2X
        epoch_value_logger.epoch_G_Y2X_cycle_consistency_loss += cycle_consistency_loss
        epoch_value_logger.epoch_G_Y2X_from_discriminator_loss_x += from_discriminator_loss_x
        epoch_value_logger.epoch_G_Y2X_from_discriminator_loss_sx += from_discriminator_loss_sx

        return loss_G_Y2X.detach().cpu()

    def step_discriminators(self, epoch_value_logger, is_training, iter_x, iter_y, loading_time_counter):
        x = timed_next(iter_x, loading_time_counter)["inp"].to(self.device0)
        y = timed_next(iter_y, loading_time_counter)["inp"].to(self.device0)

        x2y = self.G_X2Y(x)
        y2x = self.G_Y2X(y)
        x2y = self.add_get_buffer(x2y, self.x2y_buffer)
        y2x = self.add_get_buffer(y2x, self.y2x_buffer)

        y, x2y = center_crop(y, x2y)
        x, y2x = center_crop(x, y2x)

        dy_x2y = self.D_Y(x2y)
        dy_y = self.D_Y(y)

        s_y2x = self.S_X(y2x.to(self.select_device()))
        s_x = self.S_X(x.to(self.select_device()))

        ds_s_y2x = self.D_SX(s_y2x.to(self.device0))
        ds_s_x = self.D_SX(s_x.to(self.device0))

        dx_y2x = self.D_X(y2x)
        dx_x = self.D_X(x)

        loss_D_Y = torch.pow(dy_x2y - 0, 2).mean() + torch.pow(dy_y - 1, 2).mean()
        loss_D_X = torch.pow(dx_y2x - 0, 2).mean() + torch.pow(dx_x - 1, 2).mean()
        loss_D_SX = torch.pow(ds_s_y2x - 0, 2).mean() + torch.pow(ds_s_x - 1, 2).mean()

        loss_D_SX_D_X = loss_D_X + loss_D_SX
        loss_D = loss_D_Y + loss_D_SX_D_X

        if is_training:
            self.G_X2Y.zero_grad()
            self.G_Y2X.zero_grad()
            self.D_Y.zero_grad()
            self.D_X.zero_grad()
            self.D_SX.zero_grad()
            loss_D.backward()
            self.optimizer_D_Y.step()
            self.optimizers_D_SX_D_X.step()

        epoch_value_logger.epoch_loss_D_Y += loss_D_Y
        epoch_value_logger.epoch_dy_y += dy_y.mean()
        epoch_value_logger.epoch_dy_x2y += dy_x2y.mean()

        epoch_value_logger.epoch_loss_D_SX_D_X += loss_D_SX_D_X
        epoch_value_logger.epoch_loss_D_SX += loss_D_SX
        epoch_value_logger.epoch_loss_D_X += loss_D_X
        epoch_value_logger.epoch_ds_s_x += ds_s_x.mean()
        epoch_value_logger.epoch_ds_s_y2x += ds_s_y2x.mean()
        epoch_value_logger.epoch_dx_x += dx_x.mean()
        epoch_value_logger.epoch_dx_y2x += dx_y2x.mean()

        epoch_value_logger.epoch_loss_D += loss_D

        return loss_D_SX_D_X.detach().cpu(), loss_D_Y.detach().cpu(), x.detach().cpu(), y.detach().cpu()

    def step_generators(self, epoch_value_logger, is_training, iter_x, iter_y, loading_time_counter,
                        identity_loss=False):
        x = timed_next(iter_x, loading_time_counter)["inp"].to(self.device0)
        y = timed_next(iter_y, loading_time_counter)["inp"].to(self.device0)

        x2y = self.G_X2Y(x)
        x2y_2x = self.G_Y2X(x2y)
        d_x2y = self.D_Y(x2y)

        y2x = self.G_Y2X(y)
        y2x_2y = self.G_X2Y(y2x)
        d_y2x = self.D_X(y2x)
        s_y2x = self.S_X(y2x.to(self.select_device()))
        d_s_y2x = self.D_SX(s_y2x.to(self.device0))

        cycle_consistency_loss_x = torch.abs(subtract(*center_crop(x2y_2x, x))).mean()
        cycle_consistency_loss_y = torch.abs(subtract(*center_crop(y2x_2y, y))).mean()

        from_discriminator_loss_x = torch.pow(d_y2x - 1, 2).mean()
        from_discriminator_loss_sx = torch.pow(d_s_y2x - 1, 2).mean()
        from_discriminator_loss_y = torch.pow(d_x2y - 1, 2).mean()

        loss_G_X2Y = (self.current_cycle_consistency_lambda * cycle_consistency_loss_x) + (
                self.args.discriminator_loss_weight * from_discriminator_loss_y)
        loss_G_Y2X = (self.current_cycle_consistency_lambda * cycle_consistency_loss_y) + (
                self.args.discriminator_loss_weight * from_discriminator_loss_x) + (
                             self.args.discriminator_loss_weight * from_discriminator_loss_sx)
        if identity_loss:
            identity_loss_y = torch.abs(subtract(*center_crop(y, y2x))).mean()
            loss_G_Y2X = loss_G_Y2X + self.current_cycle_consistency_lambda * 10 * identity_loss_y
            identity_loss_x = torch.abs(subtract(*center_crop(x, x2y))).mean()
            loss_G_X2Y = loss_G_X2Y + self.current_cycle_consistency_lambda * 10 * identity_loss_x

        loss_G = loss_G_X2Y + loss_G_Y2X

        if is_training:
            self.G_Y2X.zero_grad()
            self.G_X2Y.zero_grad()
            self.D_X.zero_grad()
            self.S_X.zero_grad()
            self.D_SX.zero_grad()
            self.D_Y.zero_grad()
            loss_G.backward()
            self.optimizer_G_Y2X.step()
            self.optimizer_G_X2Y.step()

        epoch_value_logger.epoch_loss_G_Y2X += loss_G_Y2X
        epoch_value_logger.epoch_G_Y2X_cycle_consistency_loss += cycle_consistency_loss_y
        epoch_value_logger.epoch_G_Y2X_from_discriminator_loss_x += from_discriminator_loss_x
        epoch_value_logger.epoch_G_Y2X_from_discriminator_loss_sx += from_discriminator_loss_sx

        epoch_value_logger.epoch_intensity_inversion_ratio += detect_intensity_inversion(x2y, x2y_2x)
        epoch_value_logger.epoch_loss_G_X2Y += loss_G_X2Y
        epoch_value_logger.epoch_G_X2Y_cycle_consistency_loss += cycle_consistency_loss_x
        epoch_value_logger.epoch_G_X2Y_from_discriminator_loss += from_discriminator_loss_y

        epoch_value_logger.epoch_loss_G += loss_G

        return loss_G_Y2X.detach().cpu(), loss_G_X2Y.detach().cpu()

    def print_memory_stats(self, idx):
        # print('id of current device', torch.cuda.current_device())
        # print('name of current device', torch.cuda.get_device_name())
        # print('properties', torch.cuda.get_device_properties(self.device2))
        # torch.cuda.set_device(self.device)

        if self.args.memory_stats:
            # Returns the current GPU memory occupied by tensors in bytes for a given device
            print(f'device 0 memory status: {idx}', torch.cuda.memory_allocated(self.device0))
            # Returns the current GPU memory managed by the caching allocator in bytes for a given device
            print(f'device 0 memory reserved: {idx}', torch.cuda.memory_reserved(device=self.device0))
            if self.multi_gpus:
                print(f'device 1 memory status: {idx}', torch.cuda.memory_allocated(self.device1))
                print(f'device 1 memory reserved: {idx}',
                      torch.cuda.memory_reserved(device=self.device1))

    def update_cycle_consistency_lambda(self):
        if self.cycle_consistency_weight_increasing:
            self.current_cycle_consistency_lambda = self.current_cycle_consistency_lambda * \
                                                    (1. + self.args.cycle_consistency_lambda_change_factor)
            if self.current_cycle_consistency_lambda > \
                    (self.args.cycle_consistency_lambda_min + self.args.cycle_consistency_lambda_max):
                self.cycle_consistency_weight_increasing = False
        else:
            self.current_cycle_consistency_lambda = self.current_cycle_consistency_lambda * \
                                                    (1. / (1. + self.args.cycle_consistency_lambda_change_factor))
            if self.current_cycle_consistency_lambda < self.args.cycle_consistency_lambda_min:
                self.cycle_consistency_weight_increasing = True
        if self.args.verbose:
            print(f"new current_cycle_consistency_lambda: {self.current_cycle_consistency_lambda}")

    def setup_misc(self):
        self.step = 0
        self.epoch = 0
        self.inversion_count_train = deque()
        self.start_timestamp = datetime.now().strftime('%y-%m-%d_%H-%M-%S-%f')
        self.exp_name = self.start_timestamp + "/"
        self.save_root = os.path.expanduser(self.args.save_path)
        self.save_path = os.path.join(self.save_root, self.exp_name)
        os.makedirs(self.save_path, exist_ok=True)
        self.tb_path = self.save_path  # change if required, right now the same path as that of saved models
        self.tb = SummaryWriter(log_dir=self.tb_path, flush_secs=20)
        self.tb.add_text("args", str(self.args))

        self.max_iter_epoch_train = self.args.epoch_size_train // (3 * self.args.batch_size)
        assert self.max_iter_epoch_train > 0
        self.max_iter_epoch_val = self.args.epoch_size_val // (3 * self.args.batch_size)
        assert self.max_iter_epoch_val > 0

        os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'  # helps in assigning multiple address via os.path.expanduser
        torch.manual_seed(self.args.seed)
        np.random.seed(self.args.seed)
        random.seed(self.args.seed)
        if self.args.deterministic:
            torch.backends.cudnn.deterministic = True
        else:
            torch.backends.cudnn.benchmark = True
        elektronn3.select_mpl_backend('Agg')  # to set agg backend for running matplotlib
        self.multi_gpus = False
        if torch.cuda.is_available():
            self.device0 = torch.device('cuda:0')
            if torch.cuda.device_count() == 2:
                self.multi_gpus = True
                if self.args.verbose:
                    print("Using 2 GPUs")
                self.device1 = torch.device('cuda:1')
                logger.info(f'Running on devices: {self.device0} and {self.device1}')
            else:
                self.device1 = None
                logger.info(f'Running on device: {self.device0}')
        else:
            self.device0 = torch.device('cpu')
            self.device1 = None
            logger.info(f'Running on device: {self.device0}')

        self.x2y_buffer = []
        self.y2x_buffer = []

    def load_S_X(self, args, device):
        # todo: allow several S_X (i.e. one for synapsetype, one for cellorganelle, one flood-filling-networks)
        pre_trained = os.path.expanduser(args.load_segmenter)
        self.S_X = torch.load(pre_trained, map_location=device).to(device)
        # todo: is it possible to flag here that no gradient is required for this model?

    def setup_dataloaders(self, patch_shape, batch_size):

        train_transform, valid_transform = get_transforms(self.args.deactivate_transforms, self.args.normalize_zero_one)

        # todo: deal with resolution mismatch -> (biqubic?) upsample to small voxel-size?/downsample to big? Or
        #  adjust model architecture accordingly such that both patches from X and Y cover (almost) exactly the same
        #  real-world volume whilst having a different number of voxels (and also different height/width ratio)? (How?)

        # todo: more transforms (RandomRotate2d axes) or use above

        print("loading datasets (this might take a while, depending on the cache_size)")
        dataset_mode = 'memory' if self.args.in_memory else 'caching'  # todo: from disk!
        X_train = KnossosRawData(conf_path=self.args.knossos_path_x,  # todo: maybe as a path, not string?
                                 # "/wholebrain/songbird/j0126/areaxfs_v5/knossosdatasets/mag1/knossos.conf",
                                 # todo: verify that this is from the same domain as input to segmenter (e.g. same
                                 #  value range)
                                 patch_shape=patch_shape,  # [z]yx
                                 transform=train_transform,
                                 bounds=tuple2bounds(self.args.x_train_bounds),  # xyz
                                 mag=1,
                                 mode=dataset_mode,
                                 epoch_size=self.args.epoch_size_train,
                                 disable_memory_check=False,
                                 verbose=self.args.verbose,
                                 cache_size=self.args.cache_size,
                                 cache_reuses=self.args.cache_reuses)

        X_valid = KnossosRawData(conf_path=self.args.knossos_path_x,
                                 patch_shape=patch_shape,  # [z]yx
                                 transform=valid_transform,
                                 bounds=tuple2bounds(self.args.x_val_bounds),  # xyz
                                 mag=1,
                                 mode=dataset_mode,
                                 epoch_size=self.args.epoch_size_val,
                                 disable_memory_check=False,
                                 verbose=self.args.verbose,
                                 cache_size=self.args.epoch_size_val,
                                 cache_reuses=sys.maxsize)  # always use same validation set,
        # todo: this is hacky, include this functionality in KnossosRawData

        Y_train = KnossosRawData(conf_path=self.args.knossos_path_y,
                                 patch_shape=patch_shape,  # [z]yx
                                 transform=train_transform,
                                 bounds=tuple2bounds(self.args.y_train_bounds),  # xyz
                                 mag=1,
                                 mode=dataset_mode,
                                 epoch_size=self.args.epoch_size_train,
                                 disable_memory_check=False,
                                 verbose=self.args.verbose,
                                 cache_size=self.args.cache_size,
                                 cache_reuses=self.args.cache_reuses)

        Y_valid = KnossosRawData(conf_path=self.args.knossos_path_y,  # todo: save validation samples
                                 patch_shape=patch_shape,  # [z]yx
                                 transform=valid_transform,
                                 bounds=tuple2bounds(self.args.y_val_bounds),  # xyz
                                 mag=1,
                                 mode=dataset_mode,
                                 epoch_size=self.args.epoch_size_val,
                                 disable_memory_check=False,
                                 verbose=self.args.verbose,
                                 cache_size=self.args.epoch_size_val,  # todo: introduce argument?
                                 cache_reuses=sys.maxsize)
        # todo: remove this hack (put all into memory with bigger batchsize?)

        train_loader_x = DataLoader(X_train, batch_size=batch_size, drop_last=True,
                                    num_workers=self.args.num_workers, worker_init_fn=_worker_init_fn)
        train_loader_y = DataLoader(Y_train, batch_size=batch_size, drop_last=True,
                                    num_workers=self.args.num_workers, worker_init_fn=_worker_init_fn)
        valid_loader_x = DataLoader(X_valid, batch_size=batch_size, drop_last=True,
                                    num_workers=self.args.num_workers, worker_init_fn=_worker_init_fn)
        valid_loader_y = DataLoader(Y_valid, batch_size=batch_size, drop_last=True,
                                    num_workers=self.args.num_workers, worker_init_fn=_worker_init_fn)

        return train_loader_x, train_loader_y, valid_loader_x, valid_loader_y

    def load_models_optimizers(self):
        if self.args.resume != "":  # Load pretrained network
            self.load_saved_state()
        else:
            self.setup_models_optimizers()
        self.print_num_params()

    def setup_models_optimizers(self):
        self.load_S_X(self.args, self.select_device())
        # for now putting only segmenter to the second gpu

        # self.D_SX = SimpleConvNet(num_input_channels=self.args.number_classes).to(self.device)
        # self.D_Y = SimpleConvNet().to(self.device)
        # self.D_X = SimpleConvNet().to(self.device)
        self.D_SX = ResNet3DFullyConvolutional(
            num_input_channels=self.args.number_classes,
            width_per_group=self.args.resnet_multiplier,
            padding_mode=self.args.discriminator_padding_mode,
            activation=self.args.activation_function,
            normalization_layer=self.args.normalization_layer,
            groupnorm_num_groups=self.args.groupnorm_num_groups,
            input_normalization=self.args.input_normalization,
            input_gaussian_noise=self.args.input_gaussian_noise,
            dropout=self.args.discriminator_dropout,
            final_activation=self.args.final_activation_discriminator,
            final_normalization=self.args.discriminator_final_normalization,
            final_layer_fast_init=self.args.final_layer_fast_init
        ).to(self.device0)
        self.D_Y = ResNet3DFullyConvolutional(num_input_channels=1,
                                              width_per_group=self.args.resnet_multiplier,
                                              padding_mode=self.args.discriminator_padding_mode,
                                              activation=self.args.activation_function,
                                              zero_init_residual=self.args.zero_init_residuals,
                                              normalization_layer=self.args.normalization_layer,
                                              groupnorm_num_groups=self.args.groupnorm_num_groups,
                                              input_normalization=self.args.input_normalization,
                                              input_gaussian_noise=self.args.input_gaussian_noise,
                                              dropout=self.args.discriminator_dropout,
                                              final_activation=self.args.final_activation_discriminator,
                                              final_normalization=self.args.discriminator_final_normalization,
                                              final_layer_fast_init=self.args.final_layer_fast_init
                                              ).to(self.device0)
        self.D_X = ResNet3DFullyConvolutional(num_input_channels=1,  # try GeneratorNet
                                              width_per_group=self.args.resnet_multiplier,
                                              padding_mode=self.args.discriminator_padding_mode,
                                              activation=self.args.activation_function,
                                              zero_init_residual=self.args.zero_init_residuals,
                                              normalization_layer=self.args.normalization_layer,
                                              groupnorm_num_groups=self.args.groupnorm_num_groups,
                                              input_normalization=self.args.input_normalization,
                                              input_gaussian_noise=self.args.input_gaussian_noise,
                                              dropout=self.args.discriminator_dropout,
                                              final_activation=self.args.final_activation_discriminator,
                                              final_normalization=self.args.discriminator_final_normalization,
                                              final_layer_fast_init=self.args.final_layer_fast_init
                                              ).to(self.device0)

        self.G_Y2X = GeneratorNet(num_blocks=self.args.num_generator_blocks,
                                  planes=self.args.planes_generator,
                                  padding_mode=self.args.generator_padding_mode,
                                  activation=self.args.activation_function,
                                  zero_init_residual=self.args.zero_init_residuals,
                                  resnet_weight_init=self.args.generator_resnet_weight_init,
                                  final_activation=self.args.final_activation_generator,
                                  normalization_layer=self.args.normalization_layer,
                                  groupnorm_num_groups=self.args.groupnorm_num_groups,
                                  identity_init_last_layer=self.args.identity_init_last_layer
                                  ).to(
            self.device0)  # todo: ensure different height-scale (resolution mismatch) is handeled properly
        self.G_X2Y = GeneratorNet(num_blocks=self.args.num_generator_blocks,
                                  planes=self.args.planes_generator,
                                  padding_mode=self.args.generator_padding_mode,
                                  activation=self.args.activation_function,
                                  zero_init_residual=self.args.zero_init_residuals,
                                  resnet_weight_init=self.args.generator_resnet_weight_init,
                                  final_activation=self.args.final_activation_generator,
                                  normalization_layer=self.args.normalization_layer,
                                  groupnorm_num_groups=self.args.groupnorm_num_groups,
                                  identity_init_last_layer=self.args.identity_init_last_layer
                                  ).to(self.device0)
        print('Using generator optimizer:', self.args.generator_optimizer)
        if self.args.generator_optimizer == 'Adam':  # todo: adam for generator,  sgd for discriminator
            generator_optimizer_class = torch.optim.Adam  # todo: set beta1 to 0.5? see:
            # https://www.kaggle.com/c/generative-dog-images/discussion/98993
            # or https://machinelearningmastery.com/how-to-train-stable-generative-adversarial-networks/
        elif self.args.generator_optimizer == "SGD":
            generator_optimizer_class = torch.optim.SGD
        else:
            raise ValueError(f"optimizer must be either 'Adam' or 'SGD', not {self.args.generator_optimizer}")

        print('Using discriminator optimizer', self.args.discriminator_optimizer)
        if self.args.discriminator_optimizer == 'Adam':
            discriminator_optimizer_class = torch.optim.Adam
        elif self.args.discriminator_optimizer == "SGD":
            discriminator_optimizer_class = torch.optim.SGD
        else:
            raise ValueError(f"optimizer must be either 'Adam' or 'SGD', not {self.args.discriminator_optimizer}")

        # todo: maybe too easy for discriminators? real, unperturbed input always have same values in range(0,255)/255

        self.optimizer_G_X2Y = generator_optimizer_class(self.G_X2Y.parameters(),
                                                         self.args.learning_rate_generator,
                                                         weight_decay=self.args.weight_decay)
        self.optimizer_G_Y2X = generator_optimizer_class(self.G_Y2X.parameters(), self.args.learning_rate_generator,
                                                         weight_decay=self.args.weight_decay)
        self.optimizer_D_Y = discriminator_optimizer_class(self.D_Y.parameters(), self.args.learning_rate_discriminator,
                                                           weight_decay=self.args.weight_decay)
        self.optimizers_D_SX_D_X = discriminator_optimizer_class(
            list(self.D_SX.parameters()) + list(self.D_X.parameters()),
            self.args.learning_rate_discriminator, weight_decay=self.args.weight_decay)
        # todo: use stochastic weight averaging? https://pytorch.org/blog/stochastic-weight-averaging-in-pytorch/
        self.state = {
            'D_X': self.D_X,
            'D_Y': self.D_Y,
            'D_SX': self.D_SX,
            'G_X2Y': self.G_X2Y,
            'G_Y2X': self.G_Y2X,
            'S_X': self.S_X,  # not really required but more convenient
            'optimizers_D_SX_D_X': self.optimizers_D_SX_D_X,
            'optimizer_D_Y': self.optimizer_D_Y,
            'optimizer_G_X2Y': self.optimizer_G_X2Y,
            'optimizer_G_Y2X': self.optimizer_G_Y2X,
            'epoch': 0,
            'info': self.info
        }

    def select_device(self):
        return self.device1 if self.multi_gpus else self.device0

    def load_saved_state(self):
        pretrained_path = os.path.expanduser(self.args.resume)
        self.state = torch.load(pretrained_path)
        self.D_X = self.state['D_X'].to(self.device0)  # todo: consider working with state dicts instead
        self.D_Y = self.state['D_Y'].to(self.device0)
        self.S_X = self.state['S_X'].to(self.select_device())
        self.D_SX = self.state['D_SX'].to(self.device0)
        self.G_X2Y = self.state['G_X2Y'].to(self.device0)
        self.G_Y2X = self.state['G_Y2X'].to(self.device0)
        self.optimizers_D_SX_D_X = self.state['optimizers_D_SX_D_X']
        self.optimizer_D_Y = self.state['optimizer_D_Y']
        self.optimizer_G_Y2X = self.state['optimizer_G_X2Y']
        self.optimizer_G_X2Y = self.state['optimizer_G_X2Y']

    def print_num_params(self):
        print("Number of trainable parameters:")
        print(f"D_X: {calculate_num_parameters(self.D_X)}")
        print(f"D_Y: {calculate_num_parameters(self.D_Y)}")
        print(f"D_SX: {calculate_num_parameters(self.D_SX)}")
        print(f"G_Y2X: {calculate_num_parameters(self.G_Y2X)}")
        print(f"G_X2Y: {calculate_num_parameters(self.G_X2Y)}")
        print(f"S_X (is not trained): {calculate_num_parameters(self.S_X)}")

    def log_hparams(self, current_loss=-1):
        hparams = {'lrd': str(self.args.learning_rate_discriminator), 'lrg': str(self.args.learning_rate_generator),
                   'cycle_consistency_lambda_min': str(self.args.cycle_consistency_lambda_min),
                   'cycle_consistency_lambda_max': str(self.args.cycle_consistency_lambda_max),
                   'num_epochs': str(self.args.num_epochs),
                   'batch_size': str(self.args.batch_size), 'epoch_size_train': str(self.args.epoch_size_train),
                   'epoch_size_val': str(self.args.epoch_size_val), 'aniso_factor': str(self.args.aniso_factor),
                   'patch_shape': str(self.args.patch_shape)}
        hparams.update(self.args.__dict__)
        # required while saving a model
        self.info = {
            'global_step': self.step,
            'epoch': self.epoch,
            'best_val_loss': None,
            'val_loss': None,
            # 'elektronn3.__version__': elektronn3.__version__,
            'env_info': collect_env.get_pretty_env_info()}
        hparams.update(self.info)
        # Validation metrics: to be used in case Y has labels
        # valid_metrics = {  # mean metrics
        #     'val_accuracy_mean': metrics.Accuracy(),
        #     'val_precision_mean': metrics.Precision(),
        #     'val_recall_mean': metrics.Recall(),
        #     'val_DSC_mean': metrics.DSC(),
        #     'val_IoU_mean': metrics.IoU(),
        # } todo: use these metrics for s(x2y_2x) and s(y2x) when GT available
        # if self.args.number_classes > 2:
        #     # Add separate per-class accuracy metrics as there are more than 2 classes
        #     valid_metrics.update({
        #         f'val_IoU_c{i}': metrics.Accuracy(i)
        #         for i in range(self.args.number_classes)
        #     })
        hparams = {key: str(value) if (type(value) is not int and type(value) is not float) else value for key, value in
                   hparams.items()}
        self.tb.add_hparams(hparam_dict=hparams, metric_dict={"hparam/test_lr": 1.0, "hparam/test_accuracy": 1.5,
                                                              "hparam/current_loss": current_loss})
        # todo: use different losses (ideally just best loss?)

    def image_tb_logging_x(self, x, is_training, seg_data_sample, ground_truth):
        try:
            self.set_train_eval_mode(False)

            x = x.to(self.device0)
            images_row = x.shape[0]
            mode = ("training" if is_training else "validation") + "_from_" + (
                "secgan_training" if ground_truth is None else "segmenter_training")

            with torch.no_grad():
                x2y = self.G_X2Y(x)
                x2y_2x = self.G_Y2X(x2y)
                s_x2y_2x = self.S_X(x2y_2x.to(self.select_device()))
                dx_x = self.D_X(x)
                dx_x2y_2x = self.D_X(x2y_2x)
                dy_x2y = self.D_Y(x2y)
                ds_x_s_x2y_2x = self.D_SX(s_x2y_2x.to(self.device0))
                ds_x_s_x = self.D_SX(seg_data_sample.to(self.device0))
            x_cropped, x2y_2x_cropped = center_crop(x, x2y_2x)
            x_diff_x2y_2x = ((x_cropped - x2y_2x_cropped) + 1) / 2  # to ensure value range in [0,1]

            if self.epoch == 1:
                self.tb.add_image(f'{mode} x',
                                  torchvision.utils.make_grid(x[:, :, x.shape[2] // 2, :, :],
                                                              nrow=images_row),
                                  global_step=self.step)

                self.tb.add_figure(f'{mode} s_x',
                                   [plot_image(seg_data_sample[i, :, seg_data_sample.shape[2] // 2, :,
                                               :].cpu().data.numpy().argmax(0), vmin=0, vmax=self.args.number_classes,
                                               cmap=get_cmap(self.args.number_classes)) for i in
                                    range(seg_data_sample.shape[0])],
                                   global_step=self.step)
                self.tb.add_figure(f'{mode} s_x overlay',  # todo: put this in extra function
                                   [plot_image(image=x[i, :, x.shape[2] // 2, :, :].cpu().data.numpy().squeeze(),
                                               overlay=seg_data_sample[i, :, seg_data_sample.shape[2] // 2, :,
                                                       :].cpu().data.numpy().argmax(0), vmin=0,
                                               vmax=self.args.number_classes, cmap=get_cmap(self.args.number_classes),
                                               overlay_alpha=0.4) for i in
                                    range(seg_data_sample.shape[0])],
                                   global_step=self.step)
                if ground_truth is not None:
                    self.tb.add_figure(f'{mode} target (for x)',
                                       [plot_image(
                                           ground_truth[i, ground_truth.shape[1] // 2, :,
                                           :].cpu().data.numpy(), vmin=0, vmax=self.args.number_classes,
                                           cmap=get_cmap(self.args.number_classes)) for i in
                                           range(ground_truth.shape[0])],
                                       global_step=self.step)
                    self.tb.add_figure(f'{mode} x target overlay',  # todo: put this in extra function
                                       [plot_image(image=x[i, :, x.shape[2] // 2, :, :].cpu().data.numpy().squeeze(),
                                                   overlay=ground_truth[i, ground_truth.shape[1] // 2, :,
                                                           :].cpu().data.numpy(), vmin=0, vmax=self.args.number_classes,
                                                   cmap=get_cmap(self.args.number_classes),
                                                   overlay_alpha=0.4) for i in
                                        range(ground_truth.shape[0])],
                                       global_step=self.step)
                    self.tb.add_scalar(f'{mode} s(x) loss', self.s_x_training_criterion(seg_data_sample, ground_truth),
                                       global_step=self.step)
                    # this loss should be close to the loss in the training of s(x)

            self.tb.add_image(f'{mode} x2y',
                              torchvision.utils.make_grid(x2y[:, :, x2y.shape[2] // 2, :, :], nrow=images_row),
                              global_step=self.step)

            self.tb.add_image(f'{mode} x2y_2x',
                              torchvision.utils.make_grid(x2y_2x[:, :, x2y_2x.shape[2] // 2, :, :],
                                                          nrow=images_row),
                              global_step=self.step)
            self.tb.add_image(f'{mode} dx_x',
                              torchvision.utils.make_grid(dx_x[:, :, dx_x.shape[2] // 2, :, :],
                                                          nrow=images_row),
                              global_step=self.step)
            self.tb.add_image(f'{mode} dy_x2y',
                              torchvision.utils.make_grid(dy_x2y[:, :, dy_x2y.shape[2] // 2, :, :],
                                                          nrow=images_row),
                              global_step=self.step)
            self.tb.add_image(f'{mode} dx_x2y_2x',
                              torchvision.utils.make_grid(dx_x2y_2x[:, :, dx_x2y_2x.shape[2] // 2, :, :],
                                                          nrow=images_row),
                              global_step=self.step)
            self.tb.add_image(f'{mode} x_diff_x2y_2x',
                              torchvision.utils.make_grid(x_diff_x2y_2x[:, :, x_diff_x2y_2x.shape[2] // 2, :, :],
                                                          nrow=images_row),
                              global_step=self.step)
            self.tb.add_image(f'{mode} ds_x_s_x2y_2x',
                              torchvision.utils.make_grid(ds_x_s_x2y_2x[:, :, ds_x_s_x2y_2x.shape[2] // 2, :, :],
                                                          nrow=images_row),
                              global_step=self.step)
            self.tb.add_image(f'{mode} ds_x_s_x',
                              torchvision.utils.make_grid(ds_x_s_x[:, :, ds_x_s_x.shape[2] // 2, :, :],
                                                          nrow=images_row),
                              global_step=self.step)
            self.tb.add_figure(f'{mode} s_x2y_2x',  # for sanity check: how far away is the segmentation?
                               [plot_image(
                                   s_x2y_2x[i, :, s_x2y_2x.shape[2] // 2, :, :].cpu().data.numpy().argmax(0), vmin=0,
                                   vmax=self.args.number_classes, cmap=get_cmap(self.args.number_classes)) for i in
                                   range(s_x2y_2x.shape[0])],
                               global_step=self.step)
            self.tb.add_figure(f'{mode} s_x2y_2x overlay',
                               [plot_image(image=x2y_2x[i, :, x2y_2x.shape[2] // 2, :, :].cpu().data.numpy().squeeze(),
                                           overlay=s_x2y_2x[i, :, s_x2y_2x.shape[2] // 2, :,
                                                   :].cpu().data.numpy().argmax(0), vmin=0,
                                           vmax=self.args.number_classes, cmap=get_cmap(self.args.number_classes),
                                           overlay_alpha=0.4) for i in range(s_x2y_2x.shape[0])],
                               global_step=self.step)
            # todo: merge all the grids into one image: either concatenate tensors (padded not cropped) or with
            #  matplotlib

            if ground_truth is not None:
                self.tb.add_scalar(f'{mode} s(x2y_2x) loss',
                                   self.s_x_training_criterion(
                                       *center_crop(s_x2y_2x.detach().cpu(), ground_truth.detach().cpu())),
                                   global_step=self.step)
                # todo: also log different parts of loss seperately

        except RuntimeError as e:
            print(e)

    def image_tb_logging_y(self, y, is_training, ground_truth):
        try:
            self.set_train_eval_mode(False)
            y = y.to(self.device0)
            images_row = y.shape[0]
            mode = ("training" if is_training else "validation") + "_from_" + (
                "secgan_training" if ground_truth is None else "y_ground_truth")

            with torch.no_grad():
                y2x = self.G_Y2X(y)
                y2x_2y = self.G_X2Y(y2x)
                s_y2x = self.S_X(y2x.to(self.select_device()))
                dy_y = self.D_Y(y)
                dy_y2x_2y = self.D_Y(y2x_2y)
                dx_y2x = self.D_X(y2x)
                dsx_s_y2x = self.D_SX(s_y2x.to(self.device0))

            y_cropped, y2x_2y_cropped = center_crop(y, y2x_2y)
            y_diff_y2x_2y = ((y_cropped - y2x_2y_cropped) + 1) / 2

            if self.epoch == 1:
                self.tb.add_image(f'{mode} y',
                                  torchvision.utils.make_grid(y[:, :, y.shape[2] // 2, :, :],
                                                              nrow=images_row),
                                  global_step=self.step)
                if ground_truth is not None and self.args.gt_available:
                    self.tb.add_figure(f'{mode} target (for y)',
                                       [plot_image(
                                           ground_truth[i, ground_truth.shape[1] // 2, :,
                                           :].cpu().data.numpy(), vmin=0, vmax=self.args.number_classes,
                                           cmap=get_cmap(self.args.number_classes)) for i in
                                           range(ground_truth.shape[0])],
                                       global_step=self.step)
                    self.tb.add_figure(f'{mode} target (for y) overlay',
                                       [plot_image(
                                           image=y[i, :, y.shape[2] // 2, :, :].cpu().data.numpy().squeeze(),
                                           overlay=ground_truth[i, ground_truth.shape[1] // 2, :,
                                                   :].cpu().data.numpy(), vmin=0, vmax=self.args.number_classes,
                                           cmap=get_cmap(self.args.number_classes),
                                           overlay_alpha=0.4) for i in
                                           range(ground_truth.shape[0])],
                                       global_step=self.step)
            self.tb.add_image(f'{mode} y2x',
                              torchvision.utils.make_grid(y2x[:, :, y2x.shape[2] // 2, :, :], nrow=images_row),
                              global_step=self.step)
            self.tb.add_image(f'{mode} y2x_2y',
                              torchvision.utils.make_grid(y2x_2y[:, :, y2x_2y.shape[2] // 2, :, :],
                                                          nrow=images_row),
                              global_step=self.step)
            self.tb.add_image(f'{mode} dy_y',
                              torchvision.utils.make_grid(dy_y[:, :, dy_y.shape[2] // 2, :, :],
                                                          nrow=images_row),
                              global_step=self.step)
            self.tb.add_image(f'{mode} dy_y2x_2y',
                              torchvision.utils.make_grid(dy_y2x_2y[:, :, dy_y2x_2y.shape[2] // 2, :, :],
                                                          nrow=images_row),
                              global_step=self.step)
            self.tb.add_image(f'{mode} dx_y2x',
                              torchvision.utils.make_grid(dx_y2x[:, :, dx_y2x.shape[2] // 2, :, :],
                                                          nrow=images_row),
                              global_step=self.step)
            self.tb.add_image(f'{mode} dsx_s_y2x',
                              torchvision.utils.make_grid(dsx_s_y2x[:, :, dsx_s_y2x.shape[2] // 2, :, :],
                                                          nrow=images_row),
                              global_step=self.step)
            self.tb.add_image(f'{mode} y_diff_y2x_2y',
                              torchvision.utils.make_grid(y_diff_y2x_2y[:, :, y_diff_y2x_2y.shape[2] // 2, :, :],
                                                          nrow=images_row),
                              global_step=self.step)

            self.tb.add_figure(f'{mode} s_y2x',
                               [plot_image(s_y2x[i, :, s_y2x.shape[2] // 2, :, :].cpu().data.numpy().argmax(0), vmin=0,
                                           vmax=self.args.number_classes, cmap=get_cmap(self.args.number_classes)) for i
                                in range(s_y2x.shape[0])],
                               global_step=self.step)
            self.tb.add_figure(f'{mode} s_y2x overlay',
                               [plot_image(image=y2x[i, :, y2x.shape[2] // 2, :, :].cpu().data.numpy().squeeze(),
                                           overlay=s_y2x[i, :, s_y2x.shape[2] // 2, :,
                                                   :].cpu().data.numpy().argmax(0),
                                           overlay_alpha=0.4, vmin=0, vmax=self.args.number_classes,
                                           cmap=get_cmap(self.args.number_classes)) for i in range(s_y2x.shape[0])],
                               global_step=self.step)
            if ground_truth is not None and self.args.gt_available:
                y_segmentation_loss = self.y_ground_truth_criteria['combined'](
                    *center_crop(s_y2x.detach().cpu(), ground_truth.long().detach().cpu()))
                self.tb.add_scalar(f'{mode} s(s_y2x) loss',
                                   y_segmentation_loss,
                                   global_step=self.step)
                if y_segmentation_loss < self.best_y_segmentation_loss:
                    self.best_y_segmentation_loss = y_segmentation_loss

            del y  # todo: is this required?
            torch.cuda.empty_cache()
        except RuntimeError as e:
            print(e)

    def gradient_tb_logging(self, model, tag):
        for name, param in model.named_parameters():
            self.tb.add_histogram(tag=tag + f' param/{name}', values=param, global_step=self.step)
            grad = param.grad if param.grad is not None else torch.tensor(0)
            self.tb.add_histogram(tag=tag + f' grad/{name}', values=grad, global_step=self.step)


def main():
    args = parse_args()
    train_restart_intensity_inversion(args)


def train_restart_intensity_inversion(args):
    run_finished = False  # todo: better way to do restarts in case of intensity inversion? How to deal with time limit?
    while not run_finished:
        args.seed = args.seed + 1  # change seed for every run
        cycleGAN = cycleGAN_trainer(args)
        run_finished, best_y_segmentation_loss = cycleGAN.train()  # returns false if intensity inversion detected
    return best_y_segmentation_loss


# todo: mixed precision found on training.py (experimental technique)
# todo: docs, types
# todo: rename to follow conventions, make all methods <20 lines
# todo: deal with different voxel sizes: up/downsample automatically (biqubic?)

if __name__ == "__main__":
    main()
