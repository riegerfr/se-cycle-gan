import argparse
from argparse import ArgumentParser

from syconn.handler.config import initialize_logging
from syconn.handler.prediction import predict_dense_to_kd

from secgan.dataset_specific.constants import KNOSSOS_J0126_PATH
from secgan.dataset_specific.dataloading import get_local_path

parser: ArgumentParser = argparse.ArgumentParser(description='predict.')

parser.add_argument(
    '-mp', '--model-path', metavar='PATH',
    default='/wholebrain/scratch/riegerfr/e3training/Sequential__20-11-23_00-05-08/model_best.pt',
    help='path to the model.pt file of the segmentation network'
)
parser.add_argument(
    '-sp', '--save-path', metavar='PATH',
    default="/wholebrain/scratch/riegerfr/knossosdatasets_test/",
    help='path to the target folder'
)

parser.add_argument(
    '-lp', '--log-path', metavar='PATH',
    default="/wholebrain/scratch/riegerfr/knossosdatasets_log/",
    help='path to the target folder'
)
parser.add_argument(
    '-kx', '--knossos-path', metavar='PATH',
    default=KNOSSOS_J0126_PATH,
    help='Path to the dataset (knossos config)'
)

parser.add_argument(
    '-coi', '--cube-of-interest', nargs='+', type=int,
    # default=None,  # [88, 88, 88], ([1000, 1000, 1000],[1400, 1400, 1400])
    help='The shape of the patches which are sampled, zxy'
)

parser.add_argument('-nc', '--num-channels', type=int, default=3)

args = parser.parse_args()

print(args)

local_path = get_local_path()
log = initialize_logging("full_pred", log_dir=local_path + args.log_path + '/logs/')

predict_dense_to_kd(kd_path=local_path + args.knossos_path,
                    target_path=local_path + args.save_path,
                    model_path=local_path + args.model_path,
                    # global_params.config.mpath_syntype,
                    mag=1, n_channel=args.num_channels,  # target_names=['syntype_v2'],
                    # target_channels=[(1, 2)], #todo: uncomment?
                    cube_of_interest=(
                    args.cube_of_interest[:3], args.cube_of_interest[3:]) if args.cube_of_interest else None,
                    log=log,
                    overwrite=True
                    )
