# ELEKTRONN3 - Neural Network Toolkit
#
# Copyright (c) 2017 - now
# Max Planck Institute of Neurobiology, Munich, Germany
# Authors: Anushka Vashishtha and Franz Rieger


import torch
from elektronn3.data import transforms
from torch import nn


class LambdaModule(nn.Module):
    def __init__(self, lambda_fn):
        super().__init__()
        self.lambda_fn = lambda_fn

    def forward(self, x):
        return self.lambda_fn(x)


class TargetModule(torch.nn.Module):
    def __init__(self, target, label_id):
        super().__init__()
        self.target = target
        self.label_id = label_id


class TensorList(list):
    def __init__(self, tensor_list):
        super().__init__()
        self.tensor_list = tensor_list

    def to(self, device):
        self.tensor_list = [tensor.to(device) for tensor in self.tensor_list]


inference_kwargs = {
    'tile_shape': (32, 32, 32),
    'overlap_shape': (128, 128, 128),
    'offset': None,
    'apply_softmax': True,
    'transform': transforms.Identity(),  # transforms.Normalize(mean=dataset_mean, std=dataset_std),
    # 'report_input_stats': True,
    # 'verbose': True,
}
