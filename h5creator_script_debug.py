import h5py
import numpy as np

# input0 = np.random.rand(222, 661, 661)
# label0 = np.random.rand(148, 511, 511)
# input1 = np.random.rand(160, 661, 661)
# label1 = np.random.rand(148, 511, 511)
# input2 = np.random.rand(129, 661, 661)
# label2 = np.random.rand(55, 511, 511)

# For X dataset
input0 = np.random.rand(150, 201, 201)  # todo: make this float32
input1 = np.random.rand(152, 204, 204)
input2 = np.random.rand(160, 215, 215)
label0 = np.random.random_integers(0, 3, (36, 87, 87))
label1 = np.random.random_integers(0, 3, (55, 81, 81))
label2 = np.random.random_integers(0, 3, (35, 71, 71))

# For Y dataset
input3 = np.random.rand(65, 205, 205)
input4 = np.random.rand(57, 154, 154)
input5 = np.random.rand(47, 163, 163)

inputs = [input0, input1, input2, input3, input4, input5]
labels = [label0, label1, label2]

for i in range(6):
    if i < 3:
        f = h5py.File(f'test_file_{i}_x.h5', 'w')
        f.create_dataset(name="raw_x", data=inputs[i], dtype='f4')
        f.create_dataset(name="label_x", data=labels[i], dtype='u2')
        f.close()
    else:
        f = h5py.File(f'test_file_{i}_y.h5', 'w')
        f.create_dataset(name="raw_y", data=inputs[i], dtype='f4')
        f.close()

# f = h5py.File('test.h5', 'r')

# f.create_dataset(name="raw", data=input0, dtype='f4')

# input_datasets = [h5py.Group.create_dataset(name ="raw", data = input) for input in inputs]

# create_dataset()

print('done')
