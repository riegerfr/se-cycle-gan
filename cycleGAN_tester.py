# File used for inference from cycleGAN model
import os
from pathlib import Path

import h5py
import numpy as np
import torch
from elektronn3.data.knossos_labels import KnossosLabels
from elektronn3.inference.inference import tiled_apply
from knossos_utils import KnossosDataset
from torch.nn import Sequential
from torch.utils.data import DataLoader

from parsing_old import parse_args
from secgan.dataset_specific.constants import VAL_KNOSSOS_BOUNDS_J0251
from secgan.dataset_specific.dataloading import get_transforms, get_local_path

H5_NAMES = ["raw_y", "y2x", "label_y_gt", "label_y_predicted"]


def save_h5_preview(args):
    local_path = get_local_path()
    # todo: load x also if required
    if torch.cuda.is_available():
        device = torch.device('cuda')
    else:
        device = torch.device('cpu')
    test_transform, valid_transform = get_transforms(True)
    patch_shape = (128, 256, 256)
    label_names = ['sj', 'vc', 'mitos']
    test_dataset = KnossosLabels(label_order=(0, 1, 3, 2),
                                 conf_path_label=local_path + args.y_ground_truth_knossos,
                                 conf_path_raw_data=local_path + args.knossos_path_y,
                                 dir_path_label=local_path + args.y_ground_truth_dir,
                                 patch_shape=patch_shape,
                                 # (64,128,128),#(128, 256, 256), #(148, 297, 297),  #(149, 299, 299) todo: check the maximum size of a patch in a file
                                 transform=test_transform,
                                 mag=1, epoch_size=1, label_names=label_names,
                                 knossos_bounds=VAL_KNOSSOS_BOUNDS_J0251)
    dataloader = DataLoader(test_dataset, batch_size=1)
    y = next(iter(dataloader))  # getting a cube

    model_path = os.path.expanduser(local_path + args.load_cycleGAN_model)  # todo: uncomment
    # model_path = "/home/franz/se_cycle_gan_training/20-06-21_22-24-00/final.pt"
    model = torch.load(model_path, map_location=device)
    print("model loaded", model)
    S_X = model['S_X']
    G_Y2X = model['G_Y2X']
    final_model = Sequential(G_Y2X, S_X).eval().to(
        device)  # todo: use different gpus? https://pytorch.org/tutorials/intermediate/model_parallel_tutorial.html

    tile_shape = (32, 64, 64)  # todo: increase if possible
    sample_input_shape = (1, 1) + tile_shape

    # to determine offset: (todo: find more efficient way, without using the model)
    with torch.no_grad():
        sample_output = final_model(torch.rand(*sample_input_shape, device=device))
    tile_in_sh = np.array(sample_input_shape[2:])  # credits to tiled_apply
    tile_out_sh = np.array(sample_output.shape[2:])
    offset = (tile_in_sh - tile_out_sh) // 2

    # whole_out_shape = (1, len(label_names) + 1) + tuple(np.array(patch_shape) - (tile_in_sh - tile_out_sh))

    with torch.no_grad():  # todo: use pytorch's fold instead? https://pytorch.org/docs/master/generated/torch.nn.Unfold.html
        s_y2x = tiled_apply(func=final_model, inp=y['inp'].to(device), tile_shape=tile_shape,
                            # inference_kwargs['tile_shape'],# (74, 99, 99),
                            offset=offset,
                            overlap_shape=offset * 2,
                            # (16, 32, 32),  # inference_kwargs['overlap_shape'], #todo: how to set overlap shape?
                            out_shape=(1, len(label_names) + 1) + patch_shape,  # whole_out_shape
                            verbose=True)  # (1, 4, 128, 256, 256))
        # todo: shape is smaller

    # with torch.no_grad(): #todo: use this for small patches?
    #    y2x = G_Y2X(y['inp'].to(device)
    #    s_y2x = S_X(y2x)
    s_y2x = s_y2x.to("cpu")

    rescale_func = lambda x: (x.squeeze().squeeze() * 255).floor().clamp(0, 255).int()

    raw_y = rescale_func(y['inp'])
    # y2x_rescaled = rescale_func(y2x)

    label_y_gt = y['target'].squeeze().squeeze()
    label_y_predicted = s_y2x.squeeze().squeeze().argmax(dim=0)

    if os.getenv('CLUSTER') == 'WHOLEBRAIN':
        preview_path = "/wholebrain/scratch/riegerfr/secgan_preview/preview/"
    else:
        preview_path = str(Path.home()) + "/scratch/riegerfr/secgan_preview/preview/"

    os.makedirs(preview_path, exist_ok=True)

    f = h5py.File(preview_path + 'y_preview.h5', 'w')
    f.create_dataset(name="raw_y", data=raw_y, dtype='f4')
    # f.create_dataset(name="y2x", data=y2x_rescaled, dtype='f4')
    f.create_dataset(name="label_y_gt", data=label_y_gt, dtype='u2')
    f.create_dataset(name="label_y_predicted", data=label_y_predicted, dtype='u2')  # todo: center_crop ?
    f.close()


def save_h5_knossos(args):
    print("saving to knossos")
    if os.getenv('CLUSTER') == 'WHOLEBRAIN':
        preview_path = "/wholebrain/scratch/riegerfr/secgan_preview/preview/"
    else:
        preview_path = str(Path.home()) + "/scratch/riegerfr/secgan_preview/preview/"

    kd = KnossosDataset()
    kd._conf_path = preview_path + "knossos_dataset/knossos.conf"  # todo: bug in knossos utils?

    kd.initialize_from_matrix(preview_path + "knossos_dataset/", (25, 10, 10), "preview_knossos",
                              data_path=preview_path + "y_preview.h5",
                              hdf5_names=["raw_y"], verbose=True)

    f = h5py.File(preview_path + "y_preview.h5", 'r')
    ground_truth = f["label_y_gt"].value
    kd.from_matrix_to_cubes(offset=(0, 0, 0),
                            mags=[1],
                            data=ground_truth,
                            data_mag=1,
                            datatype=np.uint64, fast_downsampling=True,
                            force_unique_labels=False, verbose=True,
                            overwrite='area', kzip_path=preview_path + "knossos_dataset/ground_truth_kzip",
                            compress_kzip=True,
                            annotation_str=None, as_raw=False, nb_threads=0,
                            upsample=False, downsample=False, gen_mergelist=False)

    prediction = f["label_y_predicted"].value

    offset = tuple((np.array(ground_truth.shape) - np.array(prediction.shape)) // 2)

    kd.from_matrix_to_cubes(offset=offset,
                            mags=[1],
                            data=prediction,
                            data_mag=1,
                            datatype=np.uint64, fast_downsampling=True,
                            force_unique_labels=False, verbose=True,
                            overwrite='area', kzip_path=preview_path + "knossos_dataset/prediction_kzip",
                            compress_kzip=True,
                            annotation_str=None, as_raw=False, nb_threads=0,
                            upsample=False, downsample=False, gen_mergelist=False)
    print(kd)


if __name__ == "__main__":
    args = parse_args()

    save_h5_preview(args)
    save_h5_knossos(args)
