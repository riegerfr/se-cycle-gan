import os

from secgan.dataset_specific.parsing import parse_args


def get_hyperparams():
    # todo: use for hyperparameter search (tool to be decided)
    # layout from : {key: [value, value] for key, value  in vars(args).items()}
    if os.getenv('CLUSTER') == 'WHOLEBRAIN':
        args = {'resume': [""],
                'load_segmenter': ['/wholebrain/scratch/riegerfr/e3training/UNet__20-04-28_00-49-14/model_best.pt',
                                   '/wholebrain/scratch/riegerfr/e3training/UNet__20-04-19_10-26-14/model_best.pt'],
                # todo: update segmenters
                'knossos_path_x': ['/wholebrain/songbird/j0126/areaxfs_v5/knossosdatasets/mag1/knossos.conf'],
                'knossos_path_y': ['/wholebrain/songbird/j0251/j0251_72_clahe2/mag1/knossos.conf'],
                'y_ground_truth_dir': ['/wholebrain/songbird/j0251/groundtruth'],
                'y_ground_truth_knossos': ['/wholebrain/songbird/j0251/groundtruth/j0251.conf'],
                'save_path': ['/wholebrain/scratch/anushkav/se_cycle_gan_training/'],
                'deterministic': [False],
                'epoch_size_train': [500], 'epoch_size_val': [20], 'max_runtime': [3600],
                'learning_rate': [0.000001, 0.1], 'cycle_consistency_lambda_change_factor': [0, 1],
                'batch_size_logging': [4], 'num_epochs': [50000], 'seed': [0, 1, 2, 3], 'number_classes': [4],
                'num_generator_blocks': [4, 6, 8], 'planes_generator': [16, 32, 64], 'resnet_multiplier': [4, 8, 16],
                'patch_shape_logging': [(64, 200, 200)], 'batch_size': [1, 2, 4],
                'cycle_consistency_lambda_min': [0.000001, 100.], 'cycle_consistency_lambda_max': [0, 100],
                'aniso_factor': [2],
                'patch_shape': [(120, 120, 120), (100, 100, 100), (88, 88, 88)],
                'num_workers': [5], 'verbose': [False], 'memory_stats': [False],
                'skip_asserts': [True],
                'x_train_bounds': [(700, 700, 300, 10000, 10000, 5600)],
                'y_train_bounds': [(2000, 2000, 300, 25000, 24000, 15000)],
                'x_val_bounds': [(700, 700, 300, 10000, 10000, 5600)],
                'y_val_bounds': [(2000, 2000, 300, 25000, 24000, 15000)],
                'saving_duration': [0], 'cache_size': [1], 'cache_reuses': [1], 'valid_indices': [2],
                'in_memory': [False], 'deactivate_transforms': [False, True],
                'generator_resnet_weight_init': [False, True], 'inversion_epochs': [10],
                'inversion_threshold': [0.7], 'optimizer': ['SGD', 'Adam'],
                'generator_padding_mode': ['valid'], 'discriminator_padding_mode': ['same'],
                'activation_function': ['relu', 'leaky_relu', 'swish'],
                'final_activation': ['none'],
                'normalization_layer': ['batchnorm', 'groupnorm'], 'groupnorm_num_groups': [2],
                'zero_init_residuals': [False, True],
                'identity_init_last_layer': [False, True]
                }
    else:
        args = {'resume': [""],
                # 'load_segmenter': ['/wholebrain/scratch/riegerfr/e3training/UNet__20-04-28_00-49-14/model_best.pt',
                #                  '/wholebrain/scratch/riegerfr/e3training/UNet__20-04-19_10-26-14/model_best.pt'],
                'load_segmenter': ['/home/franz/e3training/UNet__20-04-25_21-41-27/model_best.pt'],
                # todo: update segmenters
                # 'knossos_path_x': ['/wholebrain/songbird/j0126/areaxfs_v5/knossosdatasets/mag1/knossos.conf'],
                'knossos_path_x': ['/home/franz/Downloads/e1088_small/knossos.conf'],
                # 'knossos_path_y': ['/wholebrain/songbird/j0251/j0251_72_clahe2/mag1/knossos.conf'],
                'knossos_path_y': ['/home/franz/Downloads/e1088_small/knossos.conf'],
                # 'y_ground_truth_dir': ['/wholebrain/songbird/j0251/groundtruth'],
                # 'y_ground_truth_knossos': ['/wholebrain/songbird/j0251/groundtruth/j0251.conf'],
                'y_ground_truth_dir': ['/home/franz/scratch/groundtruth/'],
                'y_ground_truth_knossos': ['/home/franz/scratch/groundtruth/j0251.conf'],
                # 'save_path': ['/wholebrain/scratch/anushkav/se_cycle_gan_training/'],
                'save_path': ['~/se_cycle_gan_training/'],
                'deterministic': [False],
                # 'epoch_size_train': [50], 'epoch_size_val': [20], 'max_runtime': [36000],
                'epoch_size_train': [10], 'epoch_size_val': [10], 'max_runtime': [100],
                'learning_rate': [0.000001, 0.1], 'cycle_consistency_lambda_change_factor': [0.01, 1.],
                'batch_size_logging': [2], 'num_epochs': [50000], 'seed': [0, 1, 2, 3], 'number_classes': [4],
                # 'num_generator_blocks': [1, 8], 'planes_generator': [2, 64], 'resnet_multiplier': [2, 16],'patch_shape_logging': [[64, 200, 200]],'batch_size': [1, 4],
                'num_generator_blocks': [1, 2], 'planes_generator': [2, 4, 6], 'resnet_multiplier': [1, 4],
                'patch_shape_logging': [(64, 100, 100)], 'batch_size': [1, 2],
                'cycle_consistency_lambda_min': [0.000001, 100.], 'cycle_consistency_lambda_max': [0, 100],
                'aniso_factor': [2],
                # 'patch_shape': [(100, 100, 100), (88, 88, 88)],
                'patch_shape': [(88, 88, 88), (80, 80, 80)],
                'num_workers': [5], 'verbose': [False], 'memory_stats': [False],
                # 'skip_asserts': [False],
                'skip_asserts': [True],
                # 'x_train_bounds': [(700, 700, 300, 10000, 10000, 5600)],
                # 'y_train_bounds': [(2000, 2000, 300, 25000, 24000, 15000)],
                # 'x_val_bounds': [(700, 700, 300, 10000, 10000, 5600)],
                # 'y_val_bounds': [(2000, 2000, 300, 25000, 24000, 15000)],
                'x_train_bounds': [(100, 100, 100, 500, 500, 500)],
                'y_train_bounds': [(100, 100, 100, 500, 500, 500)],
                'x_val_bounds': [(100, 100, 100, 500, 500, 500)],
                'y_val_bounds': [(100, 100, 100, 500, 500, 500)],
                'saving_duration': [0], 'cache_size': [1], 'cache_reuses': [1], 'valid_indices': [2],
                'in_memory': [False], 'deactivate_transforms': [False, True],
                'generator_resnet_weight_init': [False, True], 'inversion_epochs': [10],
                'inversion_threshold': [0.7], 'optimizer': ['SGD', 'Adam'],
                # 'generator_padding_mode': ['valid', 'same'], 'discriminator_padding_mode': ['same', 'valid'],
                'generator_padding_mode': ['valid'], 'discriminator_padding_mode': ['same'],
                'activation_function': ['relu', 'leaky_relu', 'swish'],
                # 'final_activation': ['sigmoid', 'none'],
                'final_activation': ['none'],
                'normalization_layer': ['batchnorm', 'groupnorm'], 'groupnorm_num_groups': [2],
                'zero_init_residuals': [False, True],
                'identity_init_last_layer': [False, True]
                }
    assert {key for key in vars(parse_args())} == {key for key in args}
    return args  # argparse.Namespace(**args)
# get_hyperparams()
