#!/usr/bin/env python3
# ELEKTRONN3 - Neural Network Toolkit
#
# Copyright (c) 2017 - now
# Max Planck Institute of Neurobiology, Munich, Germany
# Authors: Martin Drawitsch, Philipp Schubert
# Adapted from elektron3-dev/examples/train_unet_neurodata.py from 5be20bfd0ec041ee325bba880f35c8e7806af8b2
# by Anushka Vashishtha and Franz Rieger


import argparse
import logging
import os
import random
import zipfile

import numpy as np
import torch
from elektronn3.modules.loss import MixedCombinedLoss
from torch import nn
from torch import optim

from dataloading_shared import inference_kwargs
from secgan.dataset_specific.constants import TRAIN_KNOSSOS_BOUNDS_J0251, VAL_KNOSSOS_BOUNDS_J0251
from secgan.dataset_specific.dataloading import load_labeled_dataset, get_local_path
# argument parsing
from receptive_field_check import compute_receptive_field
from secgan.secgan_helper import calculate_num_parameters

parser = argparse.ArgumentParser(description='Train a network.')
parser.add_argument('--disable-cuda', action='store_true', help='Disable CUDA')
parser.add_argument('-n', '--exp-name', default=None, help='Manually set experiment name')
parser.add_argument(
    '-es', '--epoch-size', type=int, default=1000,
    help='How many training samples to process between '
         'validation/preview/extended-stat calculation phases.'
)
parser.add_argument(
    '-m', '--max-steps', type=int, default=500000,
    help='Maximum number of training steps to perform.'
)
parser.add_argument(
    '-t', '--max-runtime', type=int, default=3600 * 24 * 4,  # 4 days
    help='Maximum training time (in seconds).'
)
parser.add_argument(
    '-r', '--resume', metavar='PATH',
    help='Path to pretrained model state dict or a compiled and saved '
         'ScriptModule from which to resume training.'
)
parser.add_argument(
    '-j', '--jit', metavar='MODE', default='onsave',
    choices=['disabled', 'train', 'onsave'],
    help="""Options:
"disabled": Completely disable JIT tracing;
"onsave": Use regular Python model for training, but trace it on-demand for saving training state;
"train": Use traced model for training and serialize it on disk"""
)
parser.add_argument('--seed', type=int, default=0, help='Base seed for all RNGs.')
parser.add_argument('--num-channels', type=int, default=4,
                    help='Number of output channels: default 4 for cellorganelle (3 for synapsetype)')
parser.add_argument('--num-blocks', type=int, default=4, help='Number of blocks in the Unet')
parser.add_argument('--num-start-filters', type=int, default=32, help='Number of start filters in the Unet')
parser.add_argument('--calc-stat', action='store_true', help='Calculate statistics of the given dataset')
parser.add_argument(
    '--deterministic', action='store_true',
    help='Run in fully deterministic mode (at the cost of execution speed).'
)
parser.add_argument('-i', '--ipython', action='store_true',
                    help='Drop into IPython shell on errors or keyboard interrupts.'
                    )
parser.add_argument('-mt', '--mixed-training', action='store_true',
                    help='Train with a mixed dataset'
                    )
parser.add_argument('-clr', '--cycle-lr', action='store_true',
                    help='Cycle the learning rate'
                    )
parser.add_argument('-att', '--attention', action='store_true',
                    help='Use attention in the model'
                    )
parser.add_argument('-nzo', '--normalize-zero-one', action='store_true',
                    help='Normalize the data to [0,1]'
                    )
parser.add_argument('-drt', '--deactivate-random-transforms', action='store_true',
                    help='Deactivate random transforms'
                    )
parser.add_argument('-lrt', '--lr-range-test', action='store_true',
                    help='Set to True to perform Cyclical LR range test instead of normal training (see '
                         'https://arxiv.org/abs/1506.01186, sec. 3.3). '
                    )
parser.add_argument('-ds', '--dataset', type=str, default='j0126',
                    help='dataset on which segmenter is trained'
                    )
parser.add_argument('-dt', '--datatype', type=str, default='cellorganelle',
                    help='datatype on which segmenter is trained'
                    )
parser.add_argument('-cwm', '--class-wt-mode', type=str, default='inverse',
                    help='mode used for initialising class weights'
                    )
# Below for synapse type
# check tb UNet__20-05-10_00-48-05 for inverse
# Below for j0251 sub cellular
# UNet__20-06-07_12-32-13
parser.add_argument('-blr', '--base-lr', type=float, default=1e-7,
                    help='base lr obtained after cyclic lr test. Default for mode = inverse'
                    )
parser.add_argument('-mlr', '--max-lr', type=float, default=2.048e-4,
                    help='mode used for initialising class weights. Default for mode = inverse'
                    )
parser.add_argument('-op', '--optimizer', type=str, default="SGD",
                    help='Optimizer, one of "SGD" or "Adam"'
                    )
parser.add_argument('-no', '--normalization', type=str, default="batch",
                    help='Normalization method'
                    )
parser.add_argument('-kp', '--knossos-path', type=str,
                    help='Knossos path for preview'
                    )
parser.add_argument('-lof', '--label-offset', type=int, default=0,
                    help='Offset for knossos labels'
                    )
parser.add_argument('-bs', '--batch-size', type=int, default=4,
                    help='Batch size'
                    )
parser.add_argument('-urb', '--unet-res-blocks', type=int, default=0,
                    help='residual blocks in the unet'
                    )
parser.add_argument('-pi', '--preview-interval', type=int, default=20,
                    help='knossos preview every n epochs'
                    )
parser.add_argument('-wd', '--weight-decay', type=float, default=0.5e-4,
                    help='Weight decay for regularization in optimizer'
                    )
parser.add_argument('-tb', '--train-bounds', type=list, default=TRAIN_KNOSSOS_BOUNDS_J0251,
                    help='bounds for training data in knossos labels'
                    )
parser.add_argument('-vbounds', '--valid-bounds', type=list, default=VAL_KNOSSOS_BOUNDS_J0251,
                    help='bounds for validation data in knossos labels'
                    )
parser.add_argument('-ps', '--patch-shape', nargs='+', type=int, default=[44, 88, 88],
                    help='patch shape of train data'
                    )
parser.add_argument('-os', '--overlap-shape', nargs='+', type=int, default=[64, 64, 64],  # todo: use receptive field
                    help='inference overlap shape'
                    )
parser.add_argument('-ts', '--tile-shape', nargs='+', type=int, default=[64, 64, 64],
                    help='inference tile shape'
                    )
parser.add_argument('-kpo', '--knossos-preview-offset', nargs='+', type=int, default=[1000, 1000, 1000],
                    help='offset for knossos preview'
                    )
parser.add_argument('-kps', '--knossos-preview-size', nargs='+', type=int, default=[512, 512, 256],  # todo: z last?
                    help='size for knossos preview'
                    )
parser.add_argument('-mw', '--mixed-weight', nargs='+', type=float, default=[0.25, 0.75],
                    help='background/foreground weight for mixed training'
                    )
parser.add_argument('-cw', '--crit-weight', nargs='+', type=float, default=[0.5, 0.5],
                    help='criteria weight for training'
                    )
parser.add_argument('-cwn', '--class-weights-normalised', nargs='+', type=float, default=[],
                    help='class weights, use calculated if empty'
                    )
parser.add_argument('-lor', '--label-order', nargs='+', type=int, default=(0, 1, 3, 2),
                    help='The order of the labels for j0251'
                    )

args = parser.parse_args()
print("args: " + str(args))

# Set up all RNG seeds, set level of determinism
random_seed = args.seed
torch.manual_seed(random_seed)
np.random.seed(random_seed)
random.seed(random_seed)
deterministic = args.deterministic
if deterministic:
    torch.backends.cudnn.deterministic = True
else:
    torch.backends.cudnn.benchmark = True  # Improves overall performance in *most* cases

# Don't move this stuff, it needs to be run this early to work
import elektronn3

elektronn3.select_mpl_backend('Agg')
logger = logging.getLogger('Unet_logs')
from elektronn3.training import Trainer, Backup, metrics, SWA
from elektronn3.modules import DiceLoss, CombinedLoss
from elektronn3.models.unet import UNet

if not args.disable_cuda and torch.cuda.is_available():
    device = torch.device('cuda')
else:
    device = torch.device('cpu')

logger.info(f'Running on device: {device}')
logger.debug(args)
save_root = os.path.expanduser('/wholebrain/scratch/riegerfr/e3training/' if os.getenv(
    'CLUSTER') == 'WHOLEBRAIN' else '~/scratch/riegerfr/e3training/')
os.makedirs(save_root, exist_ok=True)

class_weights_normalised, train_dataset, valid_dataset, preview_batch = \
    load_labeled_dataset(datatype=args.datatype,
                         # todo: add patch shape
                         dataset_name=args.dataset,
                         epoch_size_train=args.epoch_size,  # todo: epoch_size_val
                         class_wt_mode=args.class_wt_mode,
                         calc_stat=args.calc_stat,
                         label_offset=args.label_offset,
                         knossos_bounds={
                             'train_bounds': args.train_bounds,
                             'valid_bounds': args.valid_bounds},
                         deactivate_random_transforms=args.deactivate_random_transforms, patch_shape=args.patch_shape,
                         label_order=args.label_order,
                         normalize_zero_one=args.normalize_zero_one)

if len(args.class_weights_normalised) > 0:
    class_weights_normalised = torch.tensor(args.class_weights_normalised)
class_weights_normalised = class_weights_normalised.to(device)

hparams = {}

out_channels = args.num_channels
model = nn.Sequential(  # LambdaModule(normalize_zero_one_no_target),  # workaround for knossos prediction
    UNet(
        out_channels=out_channels,
        n_blocks=args.num_blocks,
        start_filts=args.num_start_filters,
        # enc_res_blocks=args.unet_res_blocks,
        # dec_res_blocks=args.unet_res_blocks,
        planar_blocks=(0,),
        activation='leaky',
        normalization=args.normalization,
        attention=args.attention,
    )).to(device)

print(f"Number of trainable parameters: {calculate_num_parameters(model)}")

compute_receptive_field(model)
# Example for a model-compatible input.
example_input = torch.ones(1, 1, 32, 64, 64)
optimizer_state_dict = None
lr_sched_state_dict = None

if args.resume is not None:  # Load pretrained network
    pretrained = os.path.expanduser(args.resume)
    _warning_str = 'Loading model without optimizer state. Prefer state dicts'
    if zipfile.is_zipfile(pretrained):  # Zip file indicates saved ScriptModule
        logger.warning(_warning_str)
        model = torch.jit.load(pretrained, map_location=device)
    else:  # Either state dict or pickled model
        state = torch.load(pretrained)
        if isinstance(state, dict):
            model.load_state_dict(state['model_state_dict'])
            optimizer_state_dict = state.get('optimizer_state_dict')
            lr_sched_state_dict = state.get('lr_sched_state_dict')
            if optimizer_state_dict is None:
                logger.warning('optimizer_state_dict not found.')
            if lr_sched_state_dict is None:
                logger.warning('lr_sched_state_dict not found.')
        elif isinstance(state, nn.Module):
            logger.warning(_warning_str)
            model = state
        else:
            raise ValueError(f'Can\'t load {pretrained}.')

# todo: check implementation from elektronn's trainer.py
enable_save_trace = False if args.jit == 'disabled' else True
if args.jit == 'onsave':
    # Make sure that tracing works
    tracedmodel = torch.jit.trace(model, example_input.to(device))
elif args.jit == 'train':
    if getattr(model, 'checkpointing', False):
        raise NotImplementedError(
            'Traced models with checkpointing currently don\'t '
            'work, so either run with --disable-trace or disable '
            'checkpointing.')
    tracedmodel = torch.jit.trace(model, example_input.to(device))
    model = tracedmodel

if args.optimizer == "SGD":
    optimizer = optim.SGD(  #
        model.parameters(),
        lr=0,  # Learning rate is set by the lr_sched below
        momentum=0.9,
        weight_decay=args.weight_decay,
    )
else:
    optimizer = optim.Adam(model.parameters(), lr=args.base_lr, weight_decay=args.weight_decay)
optimizer = SWA(optimizer)  # Enable support for Stochastic Weight Averaging #todo: does this cause a problem?

# Set to True to perform Cyclical LR range test instead of normal training
#  (see https://arxiv.org/abs/1506.01186, sec. 3.3).
do_lr_range_test = args.lr_range_test
if do_lr_range_test:
    # Begin with a very small lr and double it every 1000 steps.
    for grp in optimizer.param_groups:
        grp['lr'] = 1e-7  # Note: lr will be > 1.0 after 24k steps.
    lr_sched = torch.optim.lr_scheduler.StepLR(optimizer, 1000, 2)
    print(f"lr_sched {lr_sched} for mode {args.class_wt_mode}")

else:
    lr_sched = torch.optim.lr_scheduler.CyclicLR(
        optimizer,
        base_lr=args.base_lr,
        max_lr=args.max_lr,
        step_size_up=10000,
        step_size_down=10000,
        cycle_momentum=True if 'momentum' in optimizer.defaults else False
    )
    if optimizer_state_dict is not None:
        optimizer.load_state_dict(optimizer_state_dict)
    if lr_sched_state_dict is not None:
        lr_sched.load_state_dict(lr_sched_state_dict)

lr_sched_dict = {'lr': lr_sched} if args.cycle_lr or do_lr_range_test else None

# Validation metrics
valid_metrics = {  # mean metrics
    'val_accuracy_mean': metrics.Accuracy(),
    'val_precision_mean': metrics.Precision(),
    'val_recall_mean': metrics.Recall(),
    'val_DSC_mean': metrics.DSC(),
    'val_IoU_mean': metrics.IoU(),
}
if out_channels > 2:
    # Add separate per-class accuracy metrics only if there are more than 2 classes
    valid_metrics.update({
        f'val_IoU_c{i}': metrics.Accuracy(i)
        for i in range(out_channels)
    })

if args.mixed_training:
    nllloss = nn.NLLLoss(weight=torch.tensor(args.mixed_weight).to(device))
    dice = DiceLoss(apply_softmax=False, weight=torch.tensor(args.mixed_weight).to(
        device))  # ,reduction = 'none' ) todo: different dice/crossentropy losses with different class weights?
    criterion = MixedCombinedLoss(criteria=[nllloss, dice], criteria_weight=args.crit_weight,
                                  class_weight=class_weights_normalised, device=device)  # todo weight handling!

else:
    crossentropy = nn.CrossEntropyLoss(weight=class_weights_normalised)
    dice = DiceLoss(apply_softmax=True, weight=class_weights_normalised)

    criterion = CombinedLoss([crossentropy, dice], weight=args.crit_weight, device=device)

if args.knossos_path is not None:
    knossos_preview_config = {
        'dataset': get_local_path() + args.knossos_path,
        'offset': args.knossos_preview_offset,  # Offset (min) coordinates
        'size': args.knossos_preview_size,  # Size (shape) of the region
        'mag': 1,  # source mag
        'target_mags': [1, 2, 3],  # List of target mags to which the inference results should be written
        # 'remap_ids': None,  # Config for class ID remapping (optional). See transforms.RemapTargetIDs
        'scale_brightness': 255  # knossos files have input
    }
else:
    knossos_preview_config = None

# todo: include percentage/ratio logging for predicted class?
inference_kwargs['overlap_shape'] = args.overlap_shape
inference_kwargs['tile_shape'] = args.tile_shape
# Create trainer
trainer = Trainer(
    model=model,
    criterion=criterion,
    optimizer=optimizer,
    device=device,
    train_dataset=train_dataset,
    valid_dataset=valid_dataset,
    batch_size=args.batch_size,
    num_workers=5 if os.getenv('CLUSTER') == 'WHOLEBRAIN' else 0,  # for local debugging
    save_root=save_root,
    exp_name=args.exp_name,
    example_input=example_input,
    enable_save_trace=enable_save_trace,
    schedulers=lr_sched_dict,
    valid_metrics=valid_metrics,
    preview_batch=preview_batch if knossos_preview_config is None else None,
    preview_interval=args.preview_interval,
    inference_kwargs=inference_kwargs,
    hparams=hparams,
    # enable_videos=True,  # Uncomment to enable videos in tensorboard
    out_channels=out_channels,
    ipython_shell=args.ipython,
    # extra_save_steps=range(0, max_steps, 10_000),
    # mixed_precision=True,  # Enable to use Apex for mixed precision training
    knossos_preview_config=knossos_preview_config,
)

if args.deterministic:
    assert trainer.num_workers <= 1, 'num_workers > 1 introduces indeterministic behavior'

# Archiving training script, src folder, env info
Backup(script_path=__file__, save_path=trainer.save_path).archive_backup()

# Start training
trainer.run(max_steps=args.max_steps, max_runtime=args.max_runtime)
