import argparse
import os
import time

import numpy as np
import ray
from ax.plot.trace import optimization_trace_single_method
from ax.service.ax_client import AxClient
from ax.utils.notebook.plotting import render
from ray import tune
from ray.tune import track
from ray.tune.suggest.ax import AxSearch

from cycleGAN_trainer import train_restart_intensity_inversion
from parsing import get_hyperparams


def hyperparameter_search(num_samples=2):
    if os.getenv('CLUSTER') == 'WHOLEBRAIN':
        ray.init(address='auto')

        @ray.remote
        def f():
            time.sleep(0.01)
            return ray.services.get_node_ip_address()

        print("Connected nodes:", set(ray.get([f.remote() for _ in range(1000)])))

    hyperparameters = get_hyperparams()
    assert all(isinstance(element, list) for element in hyperparameters.values())
    parameters = [{'name': name, 'type': 'range', 'bounds': [bounds[0], bounds[1]], 'log_scale': True} for
                  name, bounds in
                  hyperparameters.items() if
                  len(bounds) == 2 and all(isinstance(i, float) for i in bounds)]  # floats
    parameters.extend([{'name': name, 'type': 'range', 'bounds': [bounds[0], bounds[1]]} for name, bounds in
                       hyperparameters.items() if
                       len(bounds) == 2 and all(isinstance(i, int) for i in bounds) and any(
                           not isinstance(i, bool) for i in bounds)])  # ints
    names = {value['name'] for value in parameters}
    parameters.extend(
        [{'name': name, 'type': 'choice',
          'values': values if not isinstance(values[0], tuple) else [str(value) for value in values]} for
         name, values in hyperparameters.items() if
         name not in names and len(values) > 1])
    parameters.extend(
        [{'name': name, 'type': 'fixed', 'value': values[0] if not isinstance(values[0], tuple) else str(values[0])} for
         name, values in hyperparameters.items() if
         name not in names and len(values) <= 1])

    ax = AxClient(enforce_sequential_optimization=True)
    ax.create_experiment(name='SE_CYCLE_GAN', parameters=parameters, objective_name='y_segmentation_loss',
                         minimize=True,
                         # parameter_constraints=["cycle_consistency_lambda_min <= cycle_consistency_lambda_max"],
                         # status_quo=None
                         )

    def train_evaluate(parametrization):

        print(parametrization)
        for param in parametrization:
            if isinstance(parametrization[param], str):
                try:
                    value = eval(parametrization[param])
                    parametrization[param] = value
                except Exception as e:
                    print("Exception", e)
        print(parametrization)
        try:
            best_y_segmentation_loss = train_restart_intensity_inversion(
                args=argparse.Namespace(
                    **parametrization))  # todo: maybe use best loss calculation over all of y? choose different seeds(see comment below)?
        except Exception as e:
            print("Configuration failed: Exception: ", e)
            best_y_segmentation_loss = 10000
        track.log(y_segmentation_loss=best_y_segmentation_loss)

    tune.run(train_evaluate, num_samples=num_samples, search_alg=AxSearch(ax),  # checkpoint_freq=1,
             # checkpoint_at_end=True,
             resources_per_trial={"cpu": 10, "gpu": 2} if os.getenv('CLUSTER') == 'WHOLEBRAIN' else {"cpu": 5,
                                                                                                     "gpu": 0},
             with_server=True,  # stop={"training_iteration": 1}
             # todo: sample more often, change seed randomly?
             )
    best_parameters, values = ax.get_best_parameters()
    means, covariances = values
    print("best_parameters", best_parameters)
    print("means/covariances:", means, covariances)
    # render(
    #    plot_contour(
    #        model=ax.generation_strategy.model, param_x='learning_rate', param_y='cycle_consistency_lambda_min',
    #        metric_name='y_segmentation_loss'
    #    )
    # )
    best_objectives = np.array([[trial.objective_mean for trial in ax.experiment.trials.values()]])
    best_objective_plot = optimization_trace_single_method(
        y=np.maximum.accumulate(best_objectives, axis=1),
        title="Model performance vs. # of iterations",
        ylabel="best_y_segmentation_loss",
    )
    render(best_objective_plot)


def main():
    parser: argparse.ArgumentParser = argparse.ArgumentParser(description='Perform hyperparameter search')
    parser.add_argument(
        '-n', '--num-samples', default=72,
        help='Number of samples'
    )

    args = parser.parse_args()

    hyperparameter_search(num_samples=args.num_samples)


if __name__ == "__main__":
    main()
