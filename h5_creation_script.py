import argparse
import os
from pathlib import Path

from syconn.handler.prediction import create_h5_from_kzip

from secgan.dataset_specific.constants import KNOSSOS_J0126_PATH
from secgan.dataset_specific.dataloading import get_local_path

parser = argparse.ArgumentParser(description='Train a network.')
parser.add_argument('--knossos_path', default=KNOSSOS_J0126_PATH, help='Path to knossos file')
parser.add_argument('--kzip_folder', default='/wholebrain/scratch/riegerfr/Golgi GT/',
                    help='Path to kzip files')

args = parser.parse_args()
local_path = get_local_path()
data_root = local_path + args.kzip_folder
kzip_fnames = sorted([f for f in os.listdir(data_root) if f.endswith('.k.zip')])

for fname in kzip_fnames:
    create_h5_from_kzip(data_root + fname, local_path + args.knossos_path, mag=1,
                        apply_mops_seg=['binary_opening', 'binary_closing'])
