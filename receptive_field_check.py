import torch
from elektronn3.models.unet import UNet

if torch.cuda.is_available():
    device = torch.device('cuda')
else:
    device = torch.device('cpu')


def compute_receptive_field(model, input_shape=(1, 1, 50, 50, 200), pos_range = 1):
    model.eval()
    maxchange_grad = []
    maxchange_outp = []
    for i in range(-pos_range, pos_range+1):
        for j in range(-pos_range, pos_range+1):
            for k in range(-pos_range, pos_range+1):
                comp_for_pos(model, input_shape, i, j, k, maxchange_grad, maxchange_outp)

    print(f"final receptive field: {torch.stack(maxchange_grad).max(dim=0)[0]} , { torch.stack(maxchange_outp).max(dim=0)[0]}")

def comp_for_pos(model, input_shape, i, j, k, maxchange_grad, maxchange_outp):
    z, x, y = (input_shape[2] // 2) + i, (input_shape[3] // 2) + j, (input_shape[4] // 2) + k
    with torch.enable_grad():
        input = torch.randn(input_shape, requires_grad=True, device=device)
        output_unperturbed = model(input)

        model.zero_grad()
        loss = output_unperturbed[0, 0, z, x, y].square()

        loss.backward()

        affected_input = input.grad.nonzero()
        maxchange_grad.append(
            (affected_input - torch.tensor([0, 0, z, x, y], device=device)).abs().max(dim=0)[0].detach())
        print(f"input receptive field for perturbed output: {maxchange_grad[-1]}")

    input[0, 0, z, x, y] = 100
    output_perturbed = model(input)
    affected = output_perturbed != output_unperturbed
    maxchange_outp.append(
        (affected.nonzero() - torch.tensor([0, 0, z, x, y], device=device)).abs().max(dim=0)[0].detach())
    print(f"output receptive field for perturbed input: {maxchange_outp[-1]}")


model = UNet(
    out_channels=3,
    n_blocks=4,
    start_filts=4,
    planar_blocks=(0,),
    activation='relu',
    normalization='batch',
    # conv_mode='valid',
    # full_norm=False,  # Uncomment to restore old sparse normalization scheme
    # up_mode='resizeconv_nearest',  # Enable to avoid checkerboard artifacts
)

# compute_receptive_field(model, (1, 1, 50, 50, 200), pos_range=1)
