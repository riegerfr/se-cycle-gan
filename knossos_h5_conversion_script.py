# from mdraw

import h5py
import knossos_utils
# import imageio
import numpy as np

# from tqdm import tqdm

# kd = knossos_utils.KnossosDataset('/wholebrain/songbird/j0251/j0251_72_clahe2/mag1/knossos.conf')
kd = knossos_utils.KnossosDataset('/wholebrain/songbird/j0126/areaxfs_v5/knossosdatasets/mag1/knossos.conf')
# out_folder = '/u/riegerfr/j0251_h5_debug'
out_folder = '/u/riegerfr/j0126_h5_debug'

raw = []
raw.append(kd.load_raw(
    offset=[1000, 1000, 500],
    size=[1024, 1024, 256],
    mag=1
))
raw.append(kd.load_raw(
    offset=[3000, 3000, 1500],
    size=[1024, 1024, 256],
    mag=1
))
raw.append(kd.load_raw(
    offset=[5000, 5000, 3000],
    size=[1024, 1024, 256],
    mag=1
))
raw = [raw_patch.astype(np.uint8) for raw_patch in raw]  # todo: normalize to [0,1]?
# Save as png stack
# for z in tqdm(range(raw.shape[0])):
#    imageio.imsave(f'{out_folder}/{z:04d}_raw.png', raw[z, :, :])

# Or as HDF5
for i, raw_patch in enumerate(raw):
    with h5py.File(f'{out_folder}/raw_{i}.h5', mode='w') as f:
        f.create_dataset('raw', data=raw_patch)
