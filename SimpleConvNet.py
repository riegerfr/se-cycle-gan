# A simple two layer conv net for cycleGAN

import torch.nn.functional as F
from torch import nn


class SimpleConvNet(nn.Module):
    def __init__(self, num_input_channels=1):
        super(SimpleConvNet, self).__init__()
        self.conv1 = nn.Conv3d(in_channels=num_input_channels, out_channels=16, kernel_size=7, stride=2,
                               padding=3, dilation=1, groups=1,
                               bias=True, padding_mode='zeros')
        self.conv2 = nn.Conv3d(in_channels=16, out_channels=32, kernel_size=3, stride=1,
                               padding=3, dilation=1, groups=1,
                               bias=True, padding_mode='zeros')
        # pooling
        self.avgpool = nn.AdaptiveAvgPool3d(output_size=1)
        self.l1 = nn.Linear(32, 1)

    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x))
        x = self.avgpool(x)
        x = x.reshape(x.shape[0], -1)
        x = self.l1(x)
        return F.sigmoid(x)
