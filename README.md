
#Segmentation-Enhanced CycleGAN

This project implements Segmentation-Enhanced CycleGANs (SECGAN) to be used with SyConn.
A U-Net from elektronn3 is used as a segmentation model, the generation model which transfers images from one domain to another consists of several residual blocks.
The relevant code is found in /secgan/. The remaining files are legacy.

Paper: 
Segmentation-Enhanced CycleGAN
Michał Januszewski, Viren Jain
bioRxiv 548081; doi: https://doi.org/10.1101/548081 

## Steps
1. Train segmentation model (e.g. with train_unet_neurodata_segmentation)
2. Train SECGAN with
   ```python cycle_gan.py --load-segmenter /path/to/trained/segmentation_model.pt --save-path /path/to/save/directory/ ```
3. Use saved model in SyConn