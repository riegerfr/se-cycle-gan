import time


class TimeCounter:
    def __init__(self):
        self.counted_time = 0.


def timed_next(iterator, time_count):
    time_before = time.time()
    elem = next(iterator)
    time_count.counted_time += time.time() - time_before
    return elem


class EpochValueLogger:
    def __init__(self):
        self.epoch_loss_G_X2Y = 0.
        self.epoch_loss_G_Y2X = 0.
        self.epoch_loss_D_SX_D_X = 0.
        self.epoch_loss_D_SX = 0.
        self.epoch_loss_D_X = 0.
        self.epoch_loss_D_Y = 0.
        self.epoch_ds_s_x = 0.
        self.epoch_ds_s_y2x = 0.
        self.epoch_dx_y2x = 0.
        self.epoch_dx_x = 0.
        self.epoch_dy_y = 0.
        self.epoch_dy_x2y = 0.
        self.epoch_G_X2Y_from_discriminator_loss = 0.
        self.epoch_G_X2Y_cycle_consistency_loss = 0.
        self.epoch_G_Y2X_cycle_consistency_loss = 0.
        self.epoch_G_Y2X_from_discriminator_loss_x = 0.
        self.epoch_G_Y2X_from_discriminator_loss_sx = 0.
        self.epoch_intensity_inversion_ratio = 0.
        self.epoch_loss_D = 0.
        self.epoch_loss_G = 0.
