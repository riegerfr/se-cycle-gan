"""Example of an offline model validation script.

This example script does not just work as is, but is meant as a **template**
and needs to be adapted to the paths and configurations specific to the data and
training setup.

IMPORTANT: This example assumes that the model to be validated was trained on
the neuro_data_cdhw example dataset. For other trainings, remember to change
the data paths and the normalization values accordingly
"""

import os
import pprint
import random
import time
import zipfile
from pathlib import Path

import h5py
import numpy as np
import torch
from elektronn3.data.knossos import KnossosRawData
from elektronn3.inference.inference import tiled_apply
from elektronn3.modules import DiceLoss, CombinedLoss
from elektronn3.training import metrics
from knossos_utils import KnossosDataset
from torch import nn
from torch.nn import Sequential
from torch.utils.data import DataLoader
from tqdm import tqdm

import secgan.dataset_specific.dataloading
from parsing_old import parse_args
from secgan.dataset_specific.dataloading import get_transforms, load_labeled_dataset, get_local_path, tuple2bounds

from secgan.secgan_helper import center_crop


def load_unet_model(model_path: str, device: torch.device):  # from elektronn3.examples.validate
    if not os.path.isfile(model_path):
        raise ValueError(f'Model path {model_path} not found.')
    # TorchScript serialization can be identified by checking if
    #  it's a zip file. Pickled Python models are not zip files.
    #  See https://github.com/pytorch/pytorch/pull/15578/files
    if zipfile.is_zipfile(model_path):
        return torch.jit.load(model_path, map_location=device)
    else:
        return torch.load(model_path, map_location=device)


def run_prediction(args, folder_name):
    random_seed = args.seed
    torch.manual_seed(random_seed)
    np.random.seed(random_seed)
    random.seed(random_seed)
    if torch.cuda.is_available():
        device = torch.device('cuda')
    else:
        device = torch.device('cpu')
    print(f"using device {device}")
    model = load_model(args, device)

    criteria, dataloader = get_dataset(args)

    label_gt, label_predicted, raw = predict(args, criteria, dataloader, device, model)

    save_output(args, label_gt, label_predicted, raw, folder_name)


def save_output(args, label_gt, label_predicted, raw, folder_name):
    print("save output to h5")
    if os.getenv('CLUSTER') == 'WHOLEBRAIN':
        preview_path = f"/wholebrain/scratch/riegerfr/secgan_preview{folder_name}/preview/"
    else:
        preview_path = str(Path.home()) + f"/scratch/riegerfr/secgan_preview{folder_name}/preview/"
    os.makedirs(preview_path, exist_ok=True)
    f = h5py.File(preview_path + 'y_preview.h5', 'w')
    f.create_dataset(name="raw", data=raw, dtype='f4')
    f.create_dataset(name="label_predicted", data=label_predicted, dtype='u2')
    if args.labeled_dataset:
        f.create_dataset(name="label_gt", data=label_gt, dtype='u2')
    f.close()


def predict(args, criteria, dataloader, device, model):
    print(f"predicting on device {device}")
    label_gt = None
    # todo: increase if possible, to args
    sample_input_shape = (1, 1) + tuple(args.tile_shape)
    # to determine offset: (todo: find more efficient way, without using the model)
    with torch.no_grad():
        sample_output = model(torch.rand(*sample_input_shape, device=device))
    tile_in_sh = np.array(sample_input_shape[2:])  # credits to tiled_apply
    tile_out_sh = np.array(sample_output.shape[2:])
    offset = (tile_in_sh - tile_out_sh) // 2
    # whole_out_shape = (1, len(label_names) + 1) + tuple(np.array(patch_shape) - (tile_in_sh - tile_out_sh))
    if args.labeled_dataset:
        # Validation metrics
        valid_metrics = {  # mean metrics
            'val_accuracy_mean': metrics.Accuracy(),
            'val_precision_mean': metrics.Precision(),
            'val_recall_mean': metrics.Recall(),
            'val_DSC_mean': metrics.DSC(),
            'val_IoU_mean': metrics.IoU(),
        }

        if args.number_classes > 2:
            # Add separate per-class accuracy metrics only if there are more than 2 classes
            valid_metrics.update({
                f'val_IoU_c{i}': metrics.Accuracy(i)
                for i in range(args.number_classes)
            })

        stats = {name: [] for name in valid_metrics.keys()}
        for criterion_name in criteria:
            stats['loss_' + criterion_name] = []
    for sample in tqdm(dataloader):
        inp = sample['inp'].to(device)
        # rotate sample
        if args.swap_axes:
            inp = inp.permute(-5, -4, -1, -2, -3)
        if inp.numel() > 120 * 120 * 120:
            with torch.no_grad():  # todo: use pytorch's fold instead? https://pytorch.org/docs/master/generated/torch.nn.Unfold.html
                s = tiled_apply(func=model, inp=inp, tile_shape=args.tile_shape,
                                offset=offset,
                                overlap_shape=args.overlap_shape,  # offset * 2,
                                out_shape=(1, len(args.label_names) + 1) + tuple(args.patch_shape),
                                verbose=True, device=device)
        else:
            with torch.no_grad():
                s = model(inp)
                s = s.to("cpu")

        rescale_func = lambda x: (x.squeeze().squeeze() * 255).floor().clamp(0, 255).int()  # todo: why clamp? Pull up?

        raw = rescale_func(inp.cpu())  # only last sample is returned/saved

        label_predicted = s.squeeze().squeeze().argmax(dim=0).detach()
        if args.labeled_dataset:
            label_gt = sample['target'].squeeze().squeeze().long().detach()
            for name, evaluator in valid_metrics.items():
                # value = evaluator(*center_crop( label_predicted, label_gt))
                value = evaluator(*center_crop(sample['target'], s))  # todo: order?
                stats[name].append(value)
            for criterion_name in criteria:
                loss = criteria[criterion_name](*center_crop(s.detach(), sample['target'].detach()))
                stats['loss_' + criterion_name].append(loss)

    if args.labeled_dataset:
        for name in stats.keys():
            stats[name] = np.nanmean(stats[name])
        pprint.pprint(stats, indent=4, width=100, compact=False)

    return label_gt, label_predicted, raw


def get_dataset(args):
    print("load_dataset")
    if args.labeled_dataset:
        if args.merge_bounds:
            knossos_bounds = {
                'train_bounds': args.train_bounds + args.valid_bounds,
                # todo: train bounds are not really required here! Set them to empty? Use different load_labeled_dataset method?
                'valid_bounds': args.train_bounds + args.valid_bounds}
        else:
            knossos_bounds = {
                'train_bounds': args.train_bounds,
                'valid_bounds': args.valid_bounds}
        class_weights_normalised, _, valid_dataset, _ = \
            load_labeled_dataset(datatype='cellorganelle',
                                 dataset_name=args.dataset_name,
                                 epoch_size_train=10,
                                 epoch_size_valid=args.epoch_size_val,
                                 calc_stat=True,
                                 class_wt_mode='inverse',
                                 patch_shape=args.patch_shape,
                                 deactivate_random_transforms=False,
                                 knossos_bounds=knossos_bounds,
                                 label_order=args.label_order)
        crossentropy = nn.CrossEntropyLoss(weight=class_weights_normalised)
        dice = DiceLoss(apply_softmax=True, weight=class_weights_normalised)
        criterion = CombinedLoss([crossentropy, dice], weight=[0.5, 0.5])
        criteria = {"crossentropy": crossentropy, "dice": dice, "combined": criterion}

    else:
        train_transform, valid_transform = get_transforms(args.deactivate_transforms,
                                                          secgan.dataset_specific.dataloading.normalize_zero_one,
                                                          divide=255)
        dataset_mode = 'memory' if args.in_memory else 'caching'

        if args.dataset_name == "j0126":
            valid_dataset = KnossosRawData(conf_path=args.knossos_path_x,
                                           patch_shape=args.patch_shape,  # [z]yx
                                           transform=valid_transform,
                                           bounds=tuple2bounds(args.x_val_bounds),  # xyz
                                           mag=1,
                                           mode=dataset_mode,
                                           epoch_size=args.epoch_size_val,
                                           disable_memory_check=False,
                                           verbose=args.verbose,
                                           cache_size=1,
                                           cache_reuses=1)
        elif args.dataset_name == "j0251":
            valid_dataset = KnossosRawData(conf_path=args.knossos_path_y,
                                           patch_shape=args.patch_shape,  # [z]yx
                                           transform=valid_transform,
                                           bounds=tuple2bounds(args.y_val_bounds),  # xyz
                                           mag=1,
                                           mode=dataset_mode,
                                           epoch_size=args.epoch_size_val,
                                           disable_memory_check=False,
                                           verbose=args.verbose,
                                           cache_size=1,
                                           cache_reuses=1)
        else:
            raise NotImplementedError
        criteria = None
    dataloader = DataLoader(valid_dataset, batch_size=1)
    return criteria, dataloader


def load_model(args, device):
    # load model
    print("load model")
    if args.model_type == "UNET":
        model = load_unet_model(args.model_path, device).eval()
    elif args.model_type == "SECGAN":
        local_path = get_local_path()
        model_path = os.path.expanduser(local_path + args.model_path)  # todo: uncomment
        # model_path = "/home/franz/se_cycle_gan_training/20-06-21_22-24-00/final.pt"
        secgan = torch.load(model_path, map_location=device)
        # print("model loaded", secgan)
        S_X = secgan['S_X']
        G_Y2X = secgan['G_Y2X']
        model = Sequential(G_Y2X, S_X).eval().to(device)
    else:
        raise NotImplementedError
    return model.to(device)


def save_h5_knossos(args, folder_name):
    print("save to knossos")
    if os.getenv('CLUSTER') == 'WHOLEBRAIN':
        preview_path = f"/wholebrain/scratch/riegerfr/secgan_preview{folder_name}/preview/"
    else:
        preview_path = str(Path.home()) + f"/scratch/riegerfr/secgan_preview{folder_name}/preview/"

    os.makedirs(preview_path, exist_ok=True)

    kd = KnossosDataset()
    kd._conf_path = preview_path + "knossos_dataset/knossos.conf"  # todo: bug in knossos utils?

    kd.initialize_from_matrix(preview_path + f"knossos_dataset/", (25, 10, 10), "preview_knossos",
                              data_path=preview_path + "y_preview.h5",
                              hdf5_names=["raw"], verbose=True)

    f = h5py.File(preview_path + "y_preview.h5", 'r')
    if args.labeled_dataset:
        ground_truth = f["label_gt"].value
        kd.from_matrix_to_cubes(offset=(0, 0, 0),
                                mags=[1],
                                data=ground_truth,
                                data_mag=1,
                                datatype=np.uint64, fast_downsampling=True,
                                force_unique_labels=False, verbose=True,
                                overwrite='area', kzip_path=preview_path + "knossos_dataset/ground_truth_kzip",
                                compress_kzip=True,
                                annotation_str=None, as_raw=False, nb_threads=0,
                                upsample=False, downsample=False, gen_mergelist=False)

    prediction = f["label_predicted"].value

    offset = tuple((np.array(f['raw'].shape) - np.array(prediction.shape)) // 2)

    kd.from_matrix_to_cubes(offset=offset,
                            mags=[1],
                            data=prediction,
                            data_mag=1,
                            datatype=np.uint64, fast_downsampling=True,
                            force_unique_labels=False, verbose=True,
                            overwrite='area', kzip_path=preview_path + "knossos_dataset/prediction_kzip",
                            compress_kzip=True,
                            annotation_str=None, as_raw=False, nb_threads=0,
                            upsample=False, downsample=False, gen_mergelist=False)
    print(kd)


if __name__ == "__main__":
    args = parse_args()
    # todo:  args.patch_shape = (128, 256, 256)
    print(args)
    folder_name = int(time.time())
    print(folder_name)
    run_prediction(args, folder_name)
    save_h5_knossos(args, folder_name)

#
# def validate(
#         model: torch.nn.Module,
#         loader: torch.utils.data.DataLoader,
#         device: torch.device,
#         metrics: Dict[str, Callable] = {}
# ) -> Tuple[Dict[str, float], Dict[str, np.ndarray]]:
#     model.eval()
#     stats = {name: [] for name in metrics.keys()}
#     for batch in tqdm(loader):
#         # Everything with a "d" prefix refers to tensors on self.device (i.e. probably on GPU)
#         dinp = batch['inp'].to(device, non_blocking=True)
#         target = batch['target']
#         with torch.no_grad():
#             dout = model(dinp)
#             out = dout.detach().cpu()
#             for name, evaluator in metrics.items():
#                 stats[name].append(evaluator(target, out))
#     for name in metrics.keys():
#         stats[name] = np.nanmean(stats[name])
#     return stats
#
#
# if __name__ == '__main__':
#     parser = argparse.ArgumentParser(description='Validate.')
#     parser.add_argument('--disable-cuda', action='store_true', help='Disable CUDA')
#     parser.add_argument('--model', help='Path to the network model')
#     args = parser.parse_args()
#
#     if not args.disable_cuda and torch.cuda.is_available():
#         device = torch.device('cuda')
#     else:
#         device = torch.device('cpu')
#
#     data_root = os.path.expanduser('~/neuro_data_cdhw/')
#     input_h5data = [(os.path.join(data_root, f'raw_{i}.h5'), 'raw') for i in range(3)]
#     target_h5data = [(os.path.join(data_root, f'barrier_int16_{i}.h5'), 'lab') for i in range(3)]
#
#     aniso_factor = 2  # Anisotropy in z dimension. E.g. 2 means half resolution in z dimension.
#     common_data_kwargs = {  # Common options for training and valid sets.
#         'aniso_factor': aniso_factor,
#         'patch_shape': (44, 88, 88),
#         # 'offset': (8, 20, 20),
#         'out_channels': 2,
#         # 'in_memory': True  # Uncomment to avoid disk I/O (if you have enough host memory for the data)
#     }
#     norm_mean = (155.291411,)
#     norm_std = (42.599973,)
#     valid_transform = transforms.Normalize(mean=norm_mean, std=norm_std, inplace=True)
#
#     print('Loading dataset...')
#     valid_dataset = PatchCreator(
#         input_sources=input_h5data,
#         target_sources=target_h5data,
#         train=False,
#         epoch_size=40,  # How many samples to use for each validation run
#         warp_prob=0,
#         warp_kwargs={'sample_aniso': aniso_factor != 1},
#         transform=valid_transform,
#         **common_data_kwargs
#     )
#     valid_loader = DataLoader(valid_dataset, num_workers=4, pin_memory=True)
#
#
#     print('Loading model...')
#     model = load_model(args.model, device)
#
#     print('Calculating metrics...')
#     stats = validate(model=model, loader=valid_loader, device=device, metrics=valid_metrics)
#     print('Done. Results:\n')
#     pprint.pprint(stats, indent=4, width=100, compact=False)
