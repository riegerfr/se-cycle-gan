# Generator class for cycleGAN

import torch
from torch import nn

from secgan.models_shared import get_activation_function, initialize_weights_resnet, zero_init_residuals, BasicBlock, \
    get_norm_layer
from secgan.dataset_specific.dataloading import normalize_zero_one


class GeneratorNet(nn.Module):

    def __init__(self, num_blocks=8, planes=32, padding_mode="valid", activation="relu", zero_init_residual=False,
                 resnet_weight_init=False, identity_init_last_layer=False, final_activation="sigmoid",
                 normalization_layer="batchnorm",
                 groupnorm_num_groups=2):
        """
        A Generator model which transforms the input from one domain to another.
        Adapted from https://www.biorxiv.org/content/10.1101/548081v1
        :param num_blocks: The number of residual blocks to use
        :param planes: The number of channels per block
        :param padding_mode: The padding mode, either "valid" or "same".
        :param activation: The activation function. One of "relu", "leaky_relu" or "swish"
        :param zero_init_residual: whether to initialize the residual blocks as the identity
        :param resnet_weight_init: whether to use the standard resnet initialization for the residual blocks
        :param identity_init_last_layer: Whether to initialize the last layer to compute the identity
        :param final_activation: The final activation function. One of "sigmoid", "tanh", "algebraic_sigmoid", "none"
         or "rescale"
        :param normalization_layer: Which normalization layer to use. One of "batchnorm" or "groupnorm"
        :param groupnorm_num_groups:  In case of groupnorm: The number of groups
        """
        super(GeneratorNet, self).__init__()
        assert padding_mode in ['same', 'valid']
        assert final_activation in ['sigmoid', 'tanh', 'algebraic_sigmoid', 'none', 'rescale']
        self.final_activation = final_activation

        self.activation = get_activation_function(activation)
        self.block_list = nn.ModuleList()

        norm_layer = get_norm_layer(normalization_layer, groupnorm_num_groups)

        for i in range(num_blocks):
            res_block = BasicBlock(inplanes=1 if i == 0 else planes, planes=planes,
                                   padding=0 if padding_mode == 'valid' else 1,
                                   activation=self.activation, norm_layer=norm_layer)
            self.block_list.append(res_block)

        self.last_layer = nn.Conv3d(in_channels=planes, out_channels=1, kernel_size=1, padding=0)

        if resnet_weight_init:
            initialize_weights_resnet(activation, self.modules())
        if zero_init_residual:
            zero_init_residuals(self.modules())

        if identity_init_last_layer:  # to avoid intensity inversion
            # this only makes sense if zero_init_residual and not final_activation and input >=0
            #  (otherwise the relu in the blocks alters the input)
            with torch.no_grad():
                self.last_layer.bias = nn.Parameter(torch.zeros_like(self.last_layer.bias), requires_grad=True)
                self.last_layer.weight = nn.Parameter(
                    self.last_layer.weight.data - self.last_layer.weight.data.mean(dim=1) + (
                            1 / (self.last_layer.weight.data.numel())),
                    requires_grad=True)

    def forward(self, inp):
        x = inp
        for res_block in self.block_list:
            x = res_block(x)
        x = self.last_layer(x)
        if self.final_activation == 'sigmoid':
            x = torch.sigmoid(
                x)  # all images produced rescaled to [0,1]
        elif self.final_activation == 'tanh':
            x = torch.tanh(x)
        elif self.final_activation == 'algebraic_sigmoid':
            x = x / (x.abs() + 1)
        elif self.final_activation == 'rescale':
            x, _ = normalize_zero_one(x, None)
        else:  # identity
            pass
        return x
