# ELEKTRONN3 - Neural Network Toolkit
# Adapted from https://pytorch.org/docs/1.1.0/_modules/torchvision/models/resnet.html
# Copyright (c) 2017 - now
# Max Planck Institute of Neurobiology, Munich, Germany
# Authors: Anushka Vashishtha and Franz Rieger

import torch
from torch import nn

from secgan.models_shared import get_activation_function, initialize_weights_resnet, zero_init_residuals, BasicBlock, \
    conv1x1, get_norm_layer


class ResNet3DFullyConvolutional(nn.Module):

    def __init__(self, block=BasicBlock, layers=(2, 2, 2, 2), zero_init_residual=False,
                 groups=1, width_per_group=64, replace_stride_with_dilation=None,
                 norm_layer=None, num_input_channels=None, padding_mode="same", activation="relu",
                 normalization_layer="batchnorm",
                 groupnorm_num_groups=2, input_normalization=False, input_gaussian_noise=0, dropout=0.0,
                 final_activation="none", final_normalization=False, final_layer_fast_init=True):
        """
        A fully convolutional 3D-ResNet to use as a discriminator
         adapted from: https://pytorch.org/docs/1.1.0/_modules/torchvision/models/resnet.html
        :param block: The residual block class
        :param layers: The number of blocks per downsampling
        :param zero_init_residual: whether to initialize the residual blocks as the identity
        :param groups: The number of groups if block is Bottleneck
        :param width_per_group: The number of channels per group
        :param replace_stride_with_dilation: Whether to replace stride with dilation
        :param norm_layer: The class of the normalization_layer
        :param num_input_channels: The number of input channels
        :param padding_mode: The padding mode. Either "same" or "valid"
        :param activation: The activation function. One of "relu", "leaky_relu" or "swish"
        :param normalization_layer: Which normalization layer to use. One of "batchnorm" or "groupnorm"
        :param groupnorm_num_groups: In case of groupnorm: The number of groups
        :param input_normalization: Whether to normalize the input
        :param input_gaussian_noise: The variance of the gaussian noise to add to the input
        :param dropout: The dropout to use
        :param final_activation: The final activation function. One of "none" or "sigmoid"
        :param final_normalization: Whether to add a final normalization layer
        :param final_layer_fast_init: Whether to initialize the final layer s.t. initial output is close to 0.5
        """
        super(ResNet3DFullyConvolutional, self).__init__()
        assert padding_mode in ['same', 'valid']
        assert normalization_layer in ['batchnorm', 'groupnorm']

        self.padding_mode = padding_mode
        self.activation = get_activation_function(activation)

        if norm_layer is None:
            norm_layer = get_norm_layer(normalization_layer, groupnorm_num_groups)

        self.inplanes = width_per_group
        self.dilation = 1
        if replace_stride_with_dilation is None:
            # each element in the tuple indicates if we should replace
            # the 2x2 stride with a dilated convolution instead
            replace_stride_with_dilation = [False, False, False]
        if len(replace_stride_with_dilation) != 3:
            raise ValueError("replace_stride_with_dilation should be None "
                             "or a 3-element tuple, got {}".format(replace_stride_with_dilation))
        self.groups = groups
        self.base_width = width_per_group
        multiplier = width_per_group

        self.input_normalization = input_normalization
        self.input_gaussian_noise = input_gaussian_noise

        self.conv1 = nn.Conv3d(num_input_channels, self.inplanes, kernel_size=7, stride=2,
                               padding=0 if self.padding_mode == 'valid' else 3,
                               bias=False)
        self.bn1 = norm_layer(self.inplanes)
        self.maxpool = nn.MaxPool3d(kernel_size=3, stride=2, padding=0 if self.padding_mode == 'valid' else 1)
        self.layer1 = self._make_layer(block, multiplier * 1, layers[0], norm_layer=norm_layer)
        self.layer2 = self._make_layer(block, multiplier * 2, layers[1], stride=2,
                                       dilate=replace_stride_with_dilation[0], norm_layer=norm_layer)
        self.layer3 = self._make_layer(block, multiplier * 4, layers[2], stride=2,
                                       dilate=replace_stride_with_dilation[1], norm_layer=norm_layer)
        self.layer4 = self._make_layer(block, multiplier * 8, layers[3], stride=2,
                                       dilate=replace_stride_with_dilation[2], norm_layer=norm_layer)
        self.dropout = False
        if dropout > 0:
            self.dropout = True
            self.dropout_layer = nn.Dropout2d(dropout)

        final_layer = []
        if final_normalization:
            final_norm_layer = norm_layer(multiplier * 8)
            final_layer.append(final_norm_layer)
        final_conv = nn.Conv3d(multiplier * 8, 1, 1)
        final_layer.append(final_conv)
        if final_activation == "sigmoid":
            final_layer.append(nn.Sigmoid())
        self.layer5 = nn.Sequential(*final_layer)
        # to just get 1 "realness" score for each voxel (padding decreases # voxels)

        initialize_weights_resnet(activation, self.modules())

        if zero_init_residual:
            zero_init_residuals(self.modules())

        if final_layer_fast_init:
            nn.init.constant_(final_conv.bias, 0.5)
            nn.init.normal_(final_conv.weight, 0, 0.01)

    def _make_layer(self, block, planes, blocks, stride=1, dilate=False, norm_layer=None):

        downsample = None
        previous_dilation = self.dilation
        if dilate:
            self.dilation *= stride
            stride = 1
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                conv1x1(self.inplanes, planes * block.expansion, stride),
                norm_layer(planes * block.expansion),
            )

        layers = []
        layers.append(
            block(inplanes=self.inplanes, planes=planes, stride=stride, downsample=downsample, groups=self.groups,
                  dilation=previous_dilation, norm_layer=norm_layer,
                  padding=0 if self.padding_mode == 'valid' else 1,
                  activation=self.activation))
        self.inplanes = planes * block.expansion
        for _ in range(1, blocks):
            layers.append(block(inplanes=self.inplanes, planes=planes, groups=self.groups,
                                dilation=self.dilation,
                                norm_layer=norm_layer, padding=0 if self.padding_mode == 'valid' else 1,
                                activation=self.activation))

        return nn.Sequential(*layers)

    def forward(self, x):
        if self.input_gaussian_noise > 0:
            x = x + (torch.randn_like(x) * self.input_gaussian_noise)
        if self.input_normalization:
            x = ((x - x.mean()) / x.std())  # consider using instance/batch normalization here?

        x = self.conv1(x)
        x = self.bn1(x)
        x = self.activation(x)
        x = self.maxpool(x)

        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)
        if self.dropout:
            x = self.dropout_layer(x)
        x = self.layer5(x)

        return x
