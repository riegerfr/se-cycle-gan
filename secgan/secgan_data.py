# ELEKTRONN3 - Neural Network Toolkit
#
# Copyright (c) 2017 - now
# Max Planck Institute of Neurobiology, Munich, Germany
# Authors: Anushka Vashishtha and Franz Rieger


import sys
from collections import namedtuple
from typing import Sequence

import pytorch_lightning as pl
import torch
from elektronn3.data.knossos import KnossosRawData
from elektronn3.modules import DiceLoss, CombinedLoss
from elektronn3.training.trainer import _worker_init_fn
from torch import nn
from torch.utils.data import DataLoader

from secgan.dataset_specific.dataloading import get_transforms, load_labeled_dataset, tuple2bounds


class SECGANDataConfig:
    def __init__(self,
                 knossos_path_x: str,
                 knossos_path_y: str,
                 train_bounds: Sequence[int],
                 valid_bounds: Sequence[int],
                 x_train_bounds: Sequence[Sequence[int]],
                 x_val_bounds: Sequence[Sequence[int]],
                 y_train_bounds: Sequence[Sequence[int]],
                 y_val_bounds: Sequence[Sequence[int]],
                 batch_size=2,
                 batch_size_logging=2,
                 cache_size=1,
                 cache_reuses=1,
                 dataset_name_x="j0126",
                 dataset_name_y="j0251",
                 datatype="cellorganelle",
                 deactivate_transforms=False,
                 epoch_size_train=500,
                 epoch_size_val=20,
                 ground_truth_epoch_size=4,
                 gt_available=True,
                 in_memory=False,
                 label_names=('sj', 'vc', 'mitos'),
                 label_offset=0,
                 label_order=(0, 1, 3, 2),
                 normalize_zero_one=False,
                 num_workers=5,
                 patch_shape=(88, 88, 88),
                 patch_shape_logging=(64, 200, 200),
                 patch_shape_logging_segmenter_training_constant_samples_x=(44, 88, 88),
                 skip_asserts=False,
                 verbose=False,
                 **kwargs
                 ):
        """
        The configuration for the SECGAN Data
        :param batch_size: The batch size for training
        :param batch_size_logging: The batch size for logging
        :param cache_size: The cache size for the KnossosRawData
        :param cache_reuses: The number of reuses per cached element in KnossosRawData
        :param dataset_name_x: The name of the dataset X
        :param dataset_name_y: The name of the dataset Y
        :param datatype: The type of the data labels
        :param deactivate_transforms: Whether to deactivate the random transforms
        :param epoch_size_train: The training epoch size
        :param epoch_size_val: The validation epoch size
        :param ground_truth_epoch_size: The epoch size for ground truth evaluation
        :param gt_available: Whether ground truth is available
        :param in_memory: Whether to keep the data in memory for KnossosRawData
        :param knossos_path_x: The path to the Knossos Dataset for X
        :param knossos_path_y: The path to the Knossos Dataset for Y
        :param label_names: The names of the labels
        :param label_offset: The offset of the labels
        :param label_order: The order of the labels in the GT (in case different from X used  for S training)
        :param normalize_zero_one: Whether to normalize to [0,1] after augmentations
        :param num_workers: The number of workers per dataloader
        :param patch_shape: The patch shape for training/evaluation
        :param patch_shape_logging: The patch shape for logging
        :param patch_shape_logging_segmenter_training_constant_samples_x: The patch shape of the samples from S training
        :param skip_asserts: Whether to skip asserts
        :param train_bounds: The bounds of the training patches for Y (in case GT is available)
        :param valid_bounds: The bounds of the validation patches for Y (in case GT is available)
        :param verbose: Whether to be verbose
        :param x_train_bounds: The bounds of the whole area to sample from for X during training
        :param x_val_bounds: The bounds of the whole area to sample from for X during validation
        :param y_train_bounds: The bounds of the whole area to sample from for Y during training
        :param y_val_bounds: The bounds of the whole area to sample from for Y during validation
        """
        self.batch_size = batch_size
        self.batch_size_logging = batch_size_logging
        self.cache_size = cache_size
        self.cache_reuses = cache_reuses
        self.dataset_name_x = dataset_name_x
        self.dataset_name_y = dataset_name_y
        self.datatype = datatype
        self.deactivate_transforms = deactivate_transforms
        self.epoch_size_train = epoch_size_train
        self.epoch_size_val = epoch_size_val
        self.ground_truth_epoch_size = ground_truth_epoch_size
        self.gt_available = gt_available
        self.in_memory = in_memory
        self.knossos_path_x = knossos_path_x
        self.knossos_path_y = knossos_path_y
        self.label_names = label_names
        self.label_offset = label_offset
        self.label_order = label_order
        self.normalize_zero_one = normalize_zero_one
        self.num_workers = num_workers
        self.patch_shape = patch_shape
        self.patch_shape_logging = patch_shape_logging
        self.patch_shape_logging_segmenter_training_constant_samples_x = \
            patch_shape_logging_segmenter_training_constant_samples_x
        self.skip_asserts = skip_asserts
        self.train_bounds = train_bounds
        self.valid_bounds = valid_bounds
        self.verbose = verbose
        self.x_train_bounds = x_train_bounds
        self.x_val_bounds = x_val_bounds
        self.y_train_bounds = y_train_bounds
        self.y_val_bounds = y_val_bounds


class SECGANDataModule(pl.LightningDataModule):
    def __init__(self, args: SECGANDataConfig):
        super().__init__()
        self.train_loader, self.valid_loader = setup_dataloaders(args, args.patch_shape, args.batch_size)

    def train_dataloader(self):
        return self.train_loader

    def val_dataloader(self):
        return self.valid_loader


LoggingData = namedtuple('LoggingData', ['s_x_training_criterion', 'x_preview_batch_train_from_segmenter_training',
                                         'x_preview_batch_valid_from_segmenter_training', 'y_ground_truth_criteria',
                                         'y_constant_preview_batch_ground_truth_train',
                                         'y_ground_truth_dataloader_train', 'y_ground_truth_dataloader_valid',
                                         'const_sample_train_x', 'const_sample_train_y', 'const_sample_valid_x',
                                         'const_sample_valid_y'])


def setup_segmenter_training_constant_samples_X(args: SECGANDataConfig):
    x_class_weights_normalised, train_dataset, valid_dataset, _ = load_labeled_dataset(
        datatype=args.datatype,
        dataset_name=args.dataset_name_x,
        epoch_size_train=8,
        calc_stat=True,
        class_wt_mode='inverse', patch_shape=args.patch_shape_logging_segmenter_training_constant_samples_x,
        deactivate_random_transforms=False, label_names=args.label_names)
    crossentropy = nn.CrossEntropyLoss(
        weight=x_class_weights_normalised)
    dice = DiceLoss(apply_softmax=True,
                    weight=x_class_weights_normalised)
    s_x_training_criterion = CombinedLoss([crossentropy, dice], weight=[0.5, 0.5])

    train_loader = DataLoader(
        train_dataset, args.batch_size, shuffle=True,
        worker_init_fn=_worker_init_fn
    )
    valid_loader = DataLoader(
        valid_dataset, args.batch_size, shuffle=True,
        worker_init_fn=_worker_init_fn
    )
    x_preview_batch_train_from_segmenter_training = next(iter(train_loader))
    x_preview_batch_valid_from_segmenter_training = next(iter(valid_loader))
    assert_value_range(x_preview_batch_valid_from_segmenter_training['inp'], args)
    assert_value_range(x_preview_batch_train_from_segmenter_training['inp'], args)

    return s_x_training_criterion, \
           x_preview_batch_train_from_segmenter_training, x_preview_batch_valid_from_segmenter_training


def setup_ground_truth_constant_samples_Y(args: SECGANDataConfig):  # todo: merge with previous function?
    y_class_weights_normalised, train_dataset, valid_dataset, _ = load_labeled_dataset(
        datatype=args.datatype,
        dataset_name=args.dataset_name_y,
        epoch_size_train=args.ground_truth_epoch_size,
        epoch_size_valid=args.ground_truth_epoch_size,
        calc_stat=True,
        class_wt_mode='inverse', label_offset=args.label_offset,
        knossos_bounds={'train_bounds': args.train_bounds,
                        'valid_bounds': args.valid_bounds}, patch_shape=args.patch_shape_logging,
        deactivate_random_transforms=False, label_order=args.label_order, label_names=args.label_names)

    crossentropy = nn.CrossEntropyLoss(
        weight=y_class_weights_normalised)
    dice = DiceLoss(apply_softmax=True,
                    weight=y_class_weights_normalised)

    combined = CombinedLoss([crossentropy, dice], weight=[0.5, 0.5])
    y_ground_truth_criteria = {'crossentropy': crossentropy, 'dice': dice, 'combined': combined}

    train_loader = DataLoader(
        train_dataset, args.batch_size, shuffle=True,
        worker_init_fn=_worker_init_fn
    )
    y_constant_preview_batch_ground_truth_train = next(iter(train_loader))
    assert_value_range(y_constant_preview_batch_ground_truth_train['inp'], args)

    y_ground_truth_dataloader_train = DataLoader(train_dataset, batch_size=args.batch_size,
                                                 drop_last=True, num_workers=args.num_workers,
                                                 worker_init_fn=_worker_init_fn)
    y_ground_truth_dataloader_valid = DataLoader(valid_dataset, batch_size=args.batch_size,
                                                 drop_last=True, num_workers=args.num_workers,
                                                 worker_init_fn=_worker_init_fn)
    return y_ground_truth_criteria, y_constant_preview_batch_ground_truth_train, \
           y_ground_truth_dataloader_train, y_ground_truth_dataloader_valid


def setup_logging_constant_samples(args: SECGANDataConfig):
    train_loader, valid_loader = setup_dataloaders(args, args.patch_shape_logging,
                                                   args.batch_size_logging)

    iter_train = iter(train_loader)
    iter_valid = iter(valid_loader)

    train_samples = next(iter_train)
    const_sample_train_x = train_samples[0]['inp']  # todo: use elektronn3.data.get_preview_batch instead?
    const_sample_train_y = train_samples[1]['inp']

    valid_samples = next(iter_valid)
    const_sample_valid_x = valid_samples[0]['inp']
    const_sample_valid_y = valid_samples[1]['inp']

    return const_sample_train_x, const_sample_train_y, const_sample_valid_x, const_sample_valid_y


def setup_logging_data(args: SECGANDataConfig):
    s_x_training_criterion, x_preview_batch_train_from_segmenter_training, \
    x_preview_batch_valid_from_segmenter_training = setup_segmenter_training_constant_samples_X(args)
    if args.gt_available:
        y_ground_truth_criteria, y_constant_preview_batch_ground_truth_train, \
        y_ground_truth_dataloader_train, y_ground_truth_dataloader_valid = setup_ground_truth_constant_samples_Y(args)
    else:
        y_ground_truth_criteria, y_constant_preview_batch_ground_truth_train, \
        y_ground_truth_dataloader_train, y_ground_truth_dataloader_valid = None, None, None, None

    const_sample_train_x, const_sample_train_y, const_sample_valid_x, const_sample_valid_y = \
        setup_logging_constant_samples(args)
    logging_data = \
        LoggingData(s_x_training_criterion=s_x_training_criterion,
                    x_preview_batch_train_from_segmenter_training=x_preview_batch_train_from_segmenter_training,
                    x_preview_batch_valid_from_segmenter_training=x_preview_batch_valid_from_segmenter_training,
                    y_ground_truth_criteria=y_ground_truth_criteria,
                    y_constant_preview_batch_ground_truth_train=y_constant_preview_batch_ground_truth_train,
                    y_ground_truth_dataloader_train=y_ground_truth_dataloader_train,
                    y_ground_truth_dataloader_valid=y_ground_truth_dataloader_valid,
                    const_sample_train_x=const_sample_train_x, const_sample_train_y=const_sample_train_y,
                    const_sample_valid_x=const_sample_valid_x, const_sample_valid_y=const_sample_valid_y)
    return logging_data


class ConcatDataset(torch.utils.data.Dataset):
    # https://discuss.pytorch.org/t/train-simultaneously-on-two-datasets/649/2

    def __init__(self, *datasets):
        self.datasets = datasets

    def __getitem__(self, i):
        return tuple(d[i] for d in self.datasets)

    def __len__(self):
        return min(len(d) for d in self.datasets)


def setup_dataloaders(args: SECGANDataConfig, patch_shape: Sequence[int], batch_size: int):
    train_transform, valid_transform = get_transforms(args.deactivate_transforms, args.normalize_zero_one, divide=255)

    print('loading datasets (this might take a while, depending on the cache_size)')
    dataset_mode = 'memory' if args.in_memory else 'caching'
    X_train = KnossosRawData(conf_path=args.knossos_path_x,
                             patch_shape=patch_shape,  # [z]yx
                             transform=train_transform,
                             bounds=tuple2bounds(args.x_train_bounds),  # xyz
                             mag=1,
                             mode=dataset_mode,
                             epoch_size=args.epoch_size_train,
                             disable_memory_check=False,
                             verbose=args.verbose,
                             cache_size=args.cache_size,
                             cache_reuses=args.cache_reuses)

    X_valid = KnossosRawData(conf_path=args.knossos_path_x,
                             patch_shape=patch_shape,  # [z]yx
                             transform=valid_transform,
                             bounds=tuple2bounds(args.x_val_bounds),  # xyz
                             mag=1,
                             mode=dataset_mode,
                             epoch_size=args.epoch_size_val,
                             disable_memory_check=False,
                             verbose=args.verbose,
                             cache_size=args.epoch_size_val,
                             cache_reuses=sys.maxsize)  # always use same validation set,

    Y_train = KnossosRawData(conf_path=args.knossos_path_y,
                             patch_shape=patch_shape,  # [z]yx
                             transform=train_transform,
                             bounds=tuple2bounds(args.y_train_bounds),  # xyz
                             mag=1,
                             mode=dataset_mode,
                             epoch_size=args.epoch_size_train,
                             disable_memory_check=False,
                             verbose=args.verbose,
                             cache_size=args.cache_size,
                             cache_reuses=args.cache_reuses)

    Y_valid = KnossosRawData(conf_path=args.knossos_path_y,
                             patch_shape=patch_shape,  # [z]yx
                             transform=valid_transform,
                             bounds=tuple2bounds(args.y_val_bounds),  # xyz
                             mag=1,
                             mode=dataset_mode,
                             epoch_size=args.epoch_size_val,
                             disable_memory_check=False,
                             verbose=args.verbose,
                             cache_size=args.epoch_size_val,
                             cache_reuses=sys.maxsize)
    train_dataset = ConcatDataset(X_train, Y_train)
    valid_dataset = ConcatDataset(X_valid, Y_valid)

    train_loader = DataLoader(train_dataset, batch_size=batch_size, drop_last=True,
                              num_workers=args.num_workers, worker_init_fn=_worker_init_fn)

    valid_loader = DataLoader(valid_dataset, batch_size=batch_size, drop_last=True,
                              num_workers=args.num_workers, worker_init_fn=_worker_init_fn)

    return train_loader, valid_loader


def assert_value_range(x, args):
    if not args.skip_asserts:
        assert x.min() >= 0  # or greater (not equal)?
        assert x.max() <= 1
        assert x.max() > x.min()  # otherwise everything has same intensity (does it make sense to learn here?)
