# ELEKTRONN3 - Neural Network Toolkit
# Parts adapted from: https://pytorch.org/docs/1.1.0/_modules/torchvision/models/resnet.html
# Copyright (c) 2017 - now
# Max Planck Institute of Neurobiology, Munich, Germany
# Authors: Anushka Vashishtha and Franz Rieger

from torch import nn

from secgan.secgan_helper import center_crop


class Swish(nn.Module):
    def __init__(self):
        super(Swish, self).__init__()

    def forward(self, x):
        return x * x.sigmoid()


def get_activation_function(activation_name):
    assert activation_name in ['relu', 'leaky_relu', 'swish']
    if activation_name == 'relu':
        return nn.ReLU(inplace=True)
    elif activation_name == 'leaky_relu':
        return nn.LeakyReLU(inplace=True)
    else:
        return Swish()


def initialize_weights_resnet(activation, modules):
    for m in modules:
        if isinstance(m, nn.Conv3d):
            nn.init.kaiming_normal_(m.weight, mode='fan_out',
                                    nonlinearity="relu" if activation == "swish" else activation)
            # todo: use diffetent initialization for swish?
        elif isinstance(m, (nn.BatchNorm3d, nn.GroupNorm)):
            nn.init.constant_(m.weight, 1)
            nn.init.constant_(m.bias, 0)


def zero_init_residuals(modules):
    # Zero-initialize the last BN in each residual branch,
    # so that the residual branch starts with zeros, and each residual block behaves like an identity.
    # This improves the model by 0.2~0.3% according to https://arxiv.org/abs/1706.02677

    for m in modules:
        if isinstance(m, Bottleneck):
            nn.init.constant_(m.bn3.weight, 0)
        elif isinstance(m, BasicBlock):
            nn.init.constant_(m.bn2.weight, 0)


class Bottleneck(nn.Module):  # todo: try this instead of basic blocks for discriminators/generators
    expansion = 4

    def __init__(self, inplanes, planes, stride=1, downsample=None, groups=1,
                 base_width=64, dilation=1, norm_layer=None):
        super(Bottleneck, self).__init__()
        if norm_layer is None:
            norm_layer = nn.BatchNorm3d
        width = int(planes * (base_width / 64.)) * groups
        # Both self.conv2 and self.downsample layers downsample the input when stride != 1
        self.conv1 = conv1x1(inplanes, width)
        self.bn1 = norm_layer(width)
        self.conv2 = conv3x3(width, width, stride, groups, dilation)
        self.bn2 = norm_layer(width)
        self.conv3 = conv1x1(width, planes * self.expansion)
        self.bn3 = norm_layer(planes * self.expansion)
        self.relu = nn.ReLU(inplace=True)
        self.downsample = downsample
        self.stride = stride

    def forward(self, x):
        identity = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)
        out = self.relu(out)

        out = self.conv3(out)
        out = self.bn3(out)

        if self.downsample is not None:
            identity = self.downsample(x)

        out += identity
        out = self.relu(out)

        return out


class BasicBlock(nn.Module):
    expansion = 1

    def __init__(self, inplanes, planes, stride=1, downsample=None, groups=1,
                 dilation=1, norm_layer=None, padding=1, activation=nn.ReLU(inplace=True)):
        super(BasicBlock, self).__init__()
        if norm_layer is None:
            norm_layer = nn.BatchNorm3d
        if groups != 1:
            raise ValueError('BasicBlock only supports groups=1')
        if dilation > 1:
            raise NotImplementedError("Dilation > 1 not supported in BasicBlock")
        self.padding = padding
        self.conv1 = conv3x3(inplanes, planes, stride, padding=padding)
        self.bn1 = norm_layer(planes)
        self.activation = activation
        self.conv2 = conv3x3(planes, planes, padding=padding)
        self.bn2 = norm_layer(planes)
        self.downsample = downsample
        self.stride = stride

    def forward(self, x):

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.activation(out)

        out = self.conv2(out)
        out = self.bn2(out)

        if self.downsample is not None:
            identity = self.downsample(x)
            out, identity = center_crop(out, identity)
        else:
            if self.padding in [0, 1]:
                identity = x if self.padding == 1 else x[:, :, 2:-2, 2:-2, 2:-2]
            else:
                raise ValueError(f"padding is {self.padding} should be in [0,1]")
        out += identity
        out = self.activation(out)

        return out


def conv3x3(in_planes, out_planes, stride=1, groups=1, dilation=1, padding=1):
    """3x3 convolution with padding"""
    return nn.Conv3d(in_planes, out_planes, kernel_size=3, stride=stride,
                     padding=padding, groups=groups, bias=False, dilation=dilation)


def conv1x1(in_planes, out_planes, stride=1):
    """1x1 convolution"""
    return nn.Conv3d(in_planes, out_planes, kernel_size=1, stride=stride, bias=False)


def get_norm_layer(normalization_layer, groupnorm_num_groups=-1):
    return nn.BatchNorm3d if normalization_layer == "batchnorm" else (lambda num_channels: nn.GroupNorm(
        num_groups=groupnorm_num_groups, num_channels=num_channels))
