# ELEKTRONN3 - Neural Network Toolkit
#
# Copyright (c) 2017 - now
# Max Planck Institute of Neurobiology, Munich, Germany
# Authors: Anushka Vashishtha and Franz Rieger


import logging
import os
import pprint
import random
from argparse import Namespace
from collections import OrderedDict
from typing import Union, Any, List

import elektronn3
import numpy as np
import pytorch_lightning as pl
import torch
import torchvision
from elektronn3.training import metrics
from elektronn3.training.handlers import plot_image, get_cmap
from elektronn3.training.train_utils import Timer
from torch import nn
from torch.optim import SGD, Adam

from secgan.generator_net import GeneratorNet
from secgan.resnet3d import ResNet3DFullyConvolutional
from secgan.secgan_data import LoggingData
from secgan.secgan_helper import SECGANConfig, get_optimizer_class, calculate_num_parameters, subtract, \
    center_crop, detect_intensity_inversion


class SECGANModule(pl.LightningModule):
    optimizers_D: Union[Adam, SGD]
    x_preview_batch_train_from_segmenter_training: object
    s_x_training_criterion: object
    x2y_buffer: List[Any]
    y2x_buffer: List[Any]
    optimizer_G_X2Y: Union[Adam, SGD]
    device0: torch.device
    device1: torch.device
    multi_gpus: bool
    cycle_consistency_weight_increasing: bool
    current_cycle_consistency_lambda: float
    optimizers_D_SX_D_X: Union[SGD, Adam]
    optimizer_D_Y: Union[SGD, Adam]
    optimizer_G_Y2X: Union[SGD, Adam]
    G_X2Y: GeneratorNet
    G_Y2X: GeneratorNet
    D_X: ResNet3DFullyConvolutional
    D_Y: ResNet3DFullyConvolutional
    D_SX: ResNet3DFullyConvolutional
    S_X: nn.Module
    args: Namespace

    def __init__(self, print_logger: logging.Logger, logging_data: LoggingData, config: SECGANConfig):
        """
        This is an implementation of Segmentation-Enhanced CycleGAN
        (https://www.biorxiv.org/content/10.1101/548081v1 Michał Januszewski, Viren Jain 2019).
        
        Notation:
        Dataset X: GT available, Discriminator D_X, pre-trained segmentation model S_X, Discriminator D_SX
        Dataset Y: no GT available, Discriminator D_Y
        Generator G_Y2X: Map from Y to X
        Generator G_X2Y: Map from X to Y
        x: batch from X, y: batch from Y
        x2y: G_X2Y(x), y2x: G_Y2X(y), y2x_2y: G_X2Y(y2x), d_y2x: D_X(y2x), s_y2x: S_X(y2x), d_s_y2x: D_SX(s_y2x)

        Goal: Without GT for Y train G_Y2X s.t. S_X(G_Y2X(y)) is meaningful 
        
        :param logging_data (:obj: LoggingData): optional GT data and criteria from S_X training for logging
        :param config (:obj: SECGANConfig): The configuration for training
        """
        super().__init__()
        self.print_logger = print_logger
        self.args = config
        self.logging_data = logging_data

        self.setup_misc()
        self.load_models_optimizers()
        self.compute_const_segmented_samples()
        self.models_to_gpu()

    def forward(self, y):
        self.models_to_gpu()
        y2x = self.G_Y2X(y)
        s_y2x = self.S_X(y2x)
        return s_y2x

    def training_step(self, batch, batch_idx, optimizer_idx):
        self.models_to_gpu()

        self.set_train_eval_mode(True)
        x, y = batch[0]['inp'].to(self.device0), batch[1]['inp'].to(self.device0)

        if optimizer_idx == 0:  # train discriminators
            loss, log = self.step_discriminators(x, y)
        elif optimizer_idx == 1:  # train generators
            loss, log = self.step_generators(x, y)
        else:
            raise NotImplementedError(f'Error: optimizer_idx must be one of [0,1], not {optimizer_idx}')

        named_stats = OrderedDict({**{'loss': loss}, **{'train_' + k: v for k, v in log.items()}})

        for key in named_stats.keys():
            self.log(key, named_stats[key], on_step=True, on_epoch=False, logger=True)
        return named_stats

    def validation_step(self, batch, batch_idx):
        self.models_to_gpu()
        self.set_train_eval_mode(False)
        x, y = batch[0]['inp'].to(self.device0), batch[1]['inp'].to(self.device0)
        result_D, log_D = self.step_discriminators(x, y)
        result_G, log_G = self.step_generators(x, y)
        result = {**log_D, **log_G}

        named_stats = {'validation_' + k: v for k, v in result.items()}
        for key in named_stats.keys():
            self.log(key, named_stats[key])  # todo: why is new plot created?
        return named_stats

    def on_train_epoch_start(self):
        self.models_to_gpu()
        self.update_cycle_consistency_lambda()

    def on_train_epoch_end(self, outputs):
        self.gradient_logging()
        self.loss_grad_logging()

    def models_to_gpu(self):
        self.S_X.to(self.select_device())  # todo: optimize gpu load for multi-gpu training
        self.G_X2Y.to(self.device0)
        self.G_Y2X.to(self.device0)
        self.D_X.to(self.device0)
        self.D_Y.to(self.device0)
        self.D_SX.to(self.device0)

    def on_validation_epoch_end(self):
        if self.args.gt_available:  # todo: log gt x as well
            self.log_ground_truth_loss(is_training_data=True)
            self.log_ground_truth_loss(is_training_data=False)
        self.constant_sample_logging()
        self.logger.experiment.flush()

    def on_fit_start(self):
        self.models_to_gpu()

    def setup(self, stage):
        self.models_to_gpu()

    def on_train_start(self):
        self.models_to_gpu()
        self.log_hparams()

    def log_ground_truth_loss(self, is_training_data=True):  # todo: also log x2y2x gt
        # todo: calculate loss on all s_y2x, history for discriminators, single step generators update
        # todo: use elektronn3.metrics?
        # todo: log crossentropy and dice seperately
        dataloader = self.logging_data.y_ground_truth_dataloader_train if \
            is_training_data else self.logging_data.y_ground_truth_dataloader_valid

        self.set_train_eval_mode(False)

        valid_metrics = {
            'val_accuracy_mean': metrics.Accuracy(),
            'val_precision_mean': metrics.Precision(),
            'val_recall_mean': metrics.Recall(),
            'val_DSC_mean': metrics.DSC(),
            'val_IoU_mean': metrics.IoU(),
        }
        if self.args.number_classes > 2:
            valid_metrics.update({
                f'val_IoU_c{i}': metrics.Accuracy(i)
                for i in range(self.args.number_classes)
            })
        stats = {name: [] for name in valid_metrics.keys()}
        for criterion_name in self.logging_data.y_ground_truth_criteria:
            stats['loss_' + criterion_name] = []

        for y_gt in dataloader:
            with torch.no_grad():
                y = y_gt['inp'].to(self.device0)
                if self.args.verbose:
                    self.print_logger.info('y_gt shape:', y.shape)
                y2x = self.G_Y2X(y).to(self.select_device())
                s_y2x = self.S_X(y2x)

                for name, evaluator in valid_metrics.items():
                    # value = evaluator(*center_crop( label_predicted, label_gt))
                    value = evaluator(*center_crop(y_gt['target'].cpu(), s_y2x.cpu()))
                    stats[name].append(value)
                for criterion_name in self.logging_data.y_ground_truth_criteria:
                    stats['loss_' + criterion_name].append(
                        self.logging_data.y_ground_truth_criteria[criterion_name](
                            *center_crop(s_y2x.detach().cpu(), y_gt['target'].long().detach().cpu())))

        for name in stats.keys():
            stats[name] = np.nanmean(stats[name])
            self.logger.experiment.add_scalar(
                f'({"train" if is_training_data else "valid"}) groundtruth s(s_y2x) {name}',
                stats[name],
                global_step=self.global_step)
        if self.args.verbose:
            self.print_logger.info(pprint.pformat(stats, indent=4, width=100, compact=False))

        if not is_training_data:
            if stats['loss_combined'] < self.best_y_segmentation_loss:
                self.best_y_segmentation_loss = stats['loss_combined']

    def gradient_logging(self):
        self.gradient_tb_logging(self.G_X2Y, 'G_X2Y')
        self.gradient_tb_logging(self.G_Y2X, 'G_Y2X')
        self.gradient_tb_logging(self.D_X, 'D_X')
        self.gradient_tb_logging(self.D_SX, 'D_SX')
        self.gradient_tb_logging(self.D_Y, 'D_Y')

    def setup_cycle_consistency_weight(self):
        assert self.args.cycle_consistency_lambda_min > 0
        # start with low weight on cycle consistency loss to avoid intensity inversion
        self.current_cycle_consistency_lambda = self.args.cycle_consistency_lambda_min
        self.cycle_consistency_weight_increasing = True

    def compute_const_segmented_samples(self):
        try:
            with torch.no_grad():
                self.seg_train_x = self.S_X(  # todo: not just constant sample but also others?
                    self.logging_data.const_sample_train_x.to(self.select_device())).detach().cpu()
                self.seg_valid_x = self.S_X(
                    self.logging_data.const_sample_valid_x.to(self.select_device())).detach().cpu()
                self.seg_train_x_from_segmenter_training = self.S_X(
                    self.logging_data.x_preview_batch_train_from_segmenter_training['inp'].to(
                        self.select_device())).detach().cpu()
                self.seg_valid_x_from_segmenter_training = self.S_X(
                    self.logging_data.x_preview_batch_valid_from_segmenter_training['inp'].to(
                        self.select_device())).detach().cpu()
        except RuntimeError as e:
            self.print_logger.info(e)

    def constant_sample_logging(self):

        timer = Timer()
        self.set_train_eval_mode(False)
        self.image_tb_logging_x(self.logging_data.const_sample_train_x, is_training=True,
                                seg_data_sample=self.seg_train_x,
                                ground_truth=None)
        self.image_tb_logging_x(self.logging_data.x_preview_batch_train_from_segmenter_training['inp'],
                                is_training=True,
                                seg_data_sample=self.seg_train_x_from_segmenter_training,
                                ground_truth=self.logging_data.x_preview_batch_train_from_segmenter_training['target'])
        self.image_tb_logging_x(self.logging_data.const_sample_valid_x, is_training=False,
                                seg_data_sample=self.seg_valid_x,
                                ground_truth=None)
        self.image_tb_logging_x(self.logging_data.x_preview_batch_valid_from_segmenter_training['inp'],
                                is_training=False,
                                seg_data_sample=self.seg_valid_x_from_segmenter_training,
                                ground_truth=self.logging_data.x_preview_batch_valid_from_segmenter_training['target'])
        self.image_tb_logging_y(self.logging_data.const_sample_train_y, is_training=True,
                                ground_truth=None)

        self.image_tb_logging_y(self.logging_data.const_sample_valid_y, is_training=False, ground_truth=None)
        if self.args.gt_available:
            self.image_tb_logging_y(self.logging_data.y_constant_preview_batch_ground_truth_train['inp'],
                                    is_training=True,
                                    ground_truth=self.logging_data.y_constant_preview_batch_ground_truth_train[
                                        'target'])
        self.logger.experiment.add_scalar('constant_sample_logging_time', timer.t_passed, global_step=self.global_step)

    def loss_grad_logging(self):
        x = self.last_x.detach()

        mode = 'training'
        x2y = self.G_X2Y(x)
        x2y.retain_grad()
        x2y_2x = self.G_Y2X(x2y)
        d_x2y = self.D_Y(x2y)
        x2y_2x, x = center_crop(x2y_2x, x)
        cycle_consistency_loss = torch.abs(x2y_2x - x).mean()
        discriminator_loss = torch.pow(d_x2y - 1, 2).mean()
        loss_G_Y2X = self.current_cycle_consistency_lambda * cycle_consistency_loss + \
                     self.args.discriminator_loss_weight * discriminator_loss

        x2y.grad = None
        self.G_X2Y.zero_grad()
        self.G_Y2X.zero_grad()
        self.D_Y.zero_grad()
        cycle_consistency_loss.backward(retain_graph=True)
        abs_grad_mean_cycle_consistency_x2y = x2y.grad.abs().mean().detach().cpu().numpy()
        self.gradient_tb_logging(self.G_X2Y, 'G_Y2X only cycle consistency')

        x2y.grad = None
        self.G_X2Y.zero_grad()
        self.G_Y2X.zero_grad()
        self.D_Y.zero_grad()
        discriminator_loss.backward(retain_graph=True)
        abs_grad_mean_discriminator_x2y = x2y.grad.abs().mean().detach().cpu().numpy()
        self.gradient_tb_logging(self.G_X2Y, 'G_Y2X only discriminator loss')

        x2y.grad = None
        self.G_X2Y.zero_grad()
        self.G_Y2X.zero_grad()
        self.D_Y.zero_grad()
        loss_G_Y2X.backward(retain_graph=True)
        abs_grad_mean_total_loss_x2y = x2y.grad.abs().mean().detach().cpu().numpy()
        self.gradient_tb_logging(self.G_X2Y, 'G_Y2X cycle consitency + discriminator')

        if self.args.verbose:
            self.print_logger.info(f'{mode} mean of absolute gradient on x2y from cycle_consistency_loss:'
                                   f' {abs_grad_mean_cycle_consistency_x2y}, from discriminator_loss:'
                                   f' {abs_grad_mean_discriminator_x2y},'
                                   f' from total_loss: {abs_grad_mean_total_loss_x2y}')

        self.logger.experiment.add_scalar(f'{mode} mean absolute gradient x2y_2x cycle_consistency_loss',
                                          abs_grad_mean_cycle_consistency_x2y,
                                          global_step=self.global_step)
        self.logger.experiment.add_scalar(f'{mode} mean absolute gradient x2y discriminator_loss',
                                          abs_grad_mean_discriminator_x2y,
                                          global_step=self.global_step)
        self.logger.experiment.add_scalar(f'{mode} mean absolute gradient x2y total loss',
                                          abs_grad_mean_total_loss_x2y,
                                          global_step=self.global_step)

    def set_train_eval_mode(self, is_training):
        if is_training:
            self.G_X2Y.train()
            self.G_Y2X.train()
            self.D_X.train()
            self.D_SX.train()
            self.D_Y.train()
        else:
            self.G_X2Y.eval()
            self.G_Y2X.eval()
            self.D_X.eval()
            self.D_SX.eval()
            self.D_Y.eval()
        self.S_X.eval()  # S_X is not trained

    def add_get_buffer(self, elem, buffer):
        if self.args.discriminator_buffer_size > 1:
            buffer.append(elem.cpu().detach())
            random.shuffle(buffer)
            if len(buffer) > self.args.discriminator_buffer_size:
                elem = buffer.pop()
            else:
                elem = buffer[0]
            assert len(buffer) <= self.args.discriminator_buffer_size
            elem = elem.to(self.device0)
        return elem

    # todo: replace all prints with log
    def step_discriminators(self, x, y):

        x2y = self.G_X2Y(x)
        y2x = self.G_Y2X(y)
        x2y = self.add_get_buffer(x2y, self.x2y_buffer)
        y2x = self.add_get_buffer(y2x, self.y2x_buffer)

        y, x2y = center_crop(y, x2y)
        x, y2x = center_crop(x, y2x)

        dy_x2y = self.D_Y(x2y)
        dy_y = self.D_Y(y)

        s_y2x = self.S_X(y2x.to(self.select_device()))
        s_x = self.S_X(x.to(self.select_device()))

        ds_s_y2x = self.D_SX(s_y2x.to(self.device0))
        ds_s_x = self.D_SX(s_x.to(self.device0))

        dx_y2x = self.D_X(y2x)
        dx_x = self.D_X(x)

        loss_D_Y = torch.pow(dy_x2y - 0, 2).mean() + torch.pow(dy_y - 1, 2).mean()
        loss_D_X = torch.pow(dx_y2x - 0, 2).mean() + torch.pow(dx_x - 1, 2).mean()
        loss_D_SX = torch.pow(ds_s_y2x - 0, 2).mean() + torch.pow(ds_s_x - 1, 2).mean()

        loss_D_SX_D_X = loss_D_X + loss_D_SX
        loss_D = loss_D_Y + loss_D_SX_D_X

        results = {
            'loss_D': loss_D,
            'loss_D_Y': loss_D_Y,
            'loss_D_SX': loss_D_SX,
            'loss_D_X': loss_D_X,
            'loss_D_SX_D_X': loss_D_SX_D_X,
            'dy_y_mean': dy_y.mean(),
            'dy_x2y_mean': dy_x2y.mean(),
            'dx_x_mean': dx_x.mean(),
            'dx_y2x_mean': dx_y2x.mean(),
            'ds_s_x_mean': ds_s_x.mean(),
            'ds_s_y2x_mean': ds_s_y2x.mean(),
        }

        return loss_D, results

    def step_generators(self, x, y, identity_loss=False):
        self.last_x = x.detach()
        x2y = self.G_X2Y(x)
        x2y_2x = self.G_Y2X(x2y)
        d_x2y = self.D_Y(x2y)

        y2x = self.G_Y2X(y)
        y2x_2y = self.G_X2Y(y2x)
        d_y2x = self.D_X(y2x)
        s_y2x = self.S_X(y2x.to(self.select_device()))
        d_s_y2x = self.D_SX(s_y2x.to(self.device0))

        cycle_consistency_loss_x = torch.abs(subtract(*center_crop(x2y_2x, x))).mean()
        cycle_consistency_loss_y = torch.abs(subtract(*center_crop(y2x_2y, y))).mean()

        from_discriminator_loss_x = torch.pow(d_y2x - 1, 2).mean()
        from_discriminator_loss_sx = torch.pow(d_s_y2x - 1, 2).mean()
        from_discriminator_loss_y = torch.pow(d_x2y - 1, 2).mean()

        loss_G_X2Y = (self.current_cycle_consistency_lambda * cycle_consistency_loss_x) + (
                self.args.discriminator_loss_weight * from_discriminator_loss_y)
        loss_G_Y2X = (self.current_cycle_consistency_lambda * cycle_consistency_loss_y) + (
                self.args.discriminator_loss_weight * from_discriminator_loss_x) + (
                             self.args.discriminator_loss_weight * from_discriminator_loss_sx)
        if identity_loss:
            identity_loss_y = torch.abs(subtract(*center_crop(y, y2x))).mean()
            loss_G_Y2X = loss_G_Y2X + self.current_cycle_consistency_lambda * 10 * identity_loss_y
            identity_loss_x = torch.abs(subtract(*center_crop(x, x2y))).mean()
            loss_G_X2Y = loss_G_X2Y + self.current_cycle_consistency_lambda * 10 * identity_loss_x

        loss_G = loss_G_X2Y + loss_G_Y2X

        results = {
            'loss_G': loss_G,
            'loss_G_X2Y': loss_G_X2Y,
            'loss_G_Y2X': loss_G_Y2X,
            'identity_loss_x': identity_loss_x if identity_loss else 0,
            'identity_loss_y': identity_loss_y if identity_loss else 0,
            'from_discriminator_loss_x': from_discriminator_loss_x,
            'from_discriminator_loss_y': from_discriminator_loss_y,
            'from_discriminator_loss_sx': from_discriminator_loss_sx,
            'intensity_inversion_ratio': detect_intensity_inversion(x2y, x2y_2x),
            'cycle_consistency_loss_x': cycle_consistency_loss_x,
            'cycle_consistency_loss_y': cycle_consistency_loss_y,
        }

        return loss_G, results

    def print_memory_stats(self, idx):

        if self.args.memory_stats:
            # Returns the current GPU memory occupied by tensors in bytes for a given device
            self.print_logger.info(f'device 0 memory status: {idx}', torch.cuda.memory_allocated(self.device0))
            # Returns the current GPU memory managed by the caching allocator in bytes for a given device
            self.print_logger.info(f'device 0 memory reserved: {idx}', torch.cuda.memory_reserved(device=self.device0))
            if self.multi_gpus:
                self.print_logger.info(f'device 1 memory status: {idx}', torch.cuda.memory_allocated(self.device1))
                self.print_logger.info(f'device 1 memory reserved: {idx}',
                                       torch.cuda.memory_reserved(device=self.device1))

    def update_cycle_consistency_lambda(self):
        if self.cycle_consistency_weight_increasing:
            self.current_cycle_consistency_lambda = self.current_cycle_consistency_lambda * \
                                                    (1. + self.args.cycle_consistency_lambda_change_factor)
            if self.current_cycle_consistency_lambda > \
                    (self.args.cycle_consistency_lambda_min + self.args.cycle_consistency_lambda_max):
                self.cycle_consistency_weight_increasing = False
        else:
            self.current_cycle_consistency_lambda = self.current_cycle_consistency_lambda * \
                                                    (1. / (1. + self.args.cycle_consistency_lambda_change_factor))
            if self.current_cycle_consistency_lambda < self.args.cycle_consistency_lambda_min:
                self.cycle_consistency_weight_increasing = True
        if self.args.verbose:
            self.print_logger.info(f'new current_cycle_consistency_lambda: {self.current_cycle_consistency_lambda}')

    def setup_misc(self):
        torch.manual_seed(self.args.seed)
        np.random.seed(self.args.seed)
        random.seed(self.args.seed)
        os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'  # helps in assigning multiple address via os.path.expanduser

        if self.args.deterministic:
            torch.backends.cudnn.deterministic = True
        else:
            torch.backends.cudnn.benchmark = True
        elektronn3.select_mpl_backend('Agg')  # to set agg backend for running matplotlib
        self.select_gpus()

        self.x2y_buffer = []
        self.y2x_buffer = []

        self.best_y_segmentation_loss = float('inf')  # smooth?

        self.setup_cycle_consistency_weight()

    def select_gpus(self):
        self.multi_gpus = False
        if torch.cuda.is_available():
            self.device0 = torch.device('cuda:0')
            if torch.cuda.device_count() == 2:
                self.multi_gpus = True
                if self.args.verbose:
                    self.print_logger.info('Using 2 GPUs')
                self.device1 = torch.device('cuda:1')
                self.print_logger.info(f'Running on devices: {self.device0} and {self.device1}')
            else:
                self.device1 = None
                self.print_logger.info(f'Running on device: {self.device0}')
        else:
            self.device0 = torch.device('cpu')
            self.device1 = None
            self.print_logger.info(f'Running on device: {self.device0}')

    def load_S_X(self, args, device):
        # todo: several S_X (i.e. one for synapsetype another cellorganelle, one FFN for neuron segmentation)
        pre_trained = os.path.expanduser(args.load_segmenter)
        self.S_X = torch.load(pre_trained, map_location=device).to(device)

    def load_models_optimizers(self):

        self.load_S_X(self.args, self.select_device())  # only segmenter to the second gpu

        self.D_SX = ResNet3DFullyConvolutional(
            num_input_channels=self.args.number_classes,
            width_per_group=self.args.resnet_multiplier,
            padding_mode=self.args.discriminator_padding_mode,
            activation=self.args.activation_function,
            normalization_layer=self.args.normalization_layer,
            groupnorm_num_groups=self.args.groupnorm_num_groups,
            input_normalization=self.args.input_normalization,
            input_gaussian_noise=self.args.input_gaussian_noise,
            dropout=self.args.discriminator_dropout,
            final_activation=self.args.final_activation_discriminator,
            final_normalization=self.args.discriminator_final_normalization,
            final_layer_fast_init=self.args.final_layer_fast_init
        ).to(self.device0)
        self.D_Y = ResNet3DFullyConvolutional(num_input_channels=1,
                                              width_per_group=self.args.resnet_multiplier,
                                              padding_mode=self.args.discriminator_padding_mode,
                                              activation=self.args.activation_function,
                                              zero_init_residual=self.args.zero_init_residuals,
                                              normalization_layer=self.args.normalization_layer,
                                              groupnorm_num_groups=self.args.groupnorm_num_groups,
                                              input_normalization=self.args.input_normalization,
                                              input_gaussian_noise=self.args.input_gaussian_noise,
                                              dropout=self.args.discriminator_dropout,
                                              final_activation=self.args.final_activation_discriminator,
                                              final_normalization=self.args.discriminator_final_normalization,
                                              final_layer_fast_init=self.args.final_layer_fast_init
                                              ).to(self.device0)
        self.D_X = ResNet3DFullyConvolutional(num_input_channels=1,
                                              width_per_group=self.args.resnet_multiplier,
                                              padding_mode=self.args.discriminator_padding_mode,
                                              activation=self.args.activation_function,
                                              zero_init_residual=self.args.zero_init_residuals,
                                              normalization_layer=self.args.normalization_layer,
                                              groupnorm_num_groups=self.args.groupnorm_num_groups,
                                              input_normalization=self.args.input_normalization,
                                              input_gaussian_noise=self.args.input_gaussian_noise,
                                              dropout=self.args.discriminator_dropout,
                                              final_activation=self.args.final_activation_discriminator,
                                              final_normalization=self.args.discriminator_final_normalization,
                                              final_layer_fast_init=self.args.final_layer_fast_init
                                              ).to(self.device0)

        self.G_Y2X = GeneratorNet(num_blocks=self.args.num_generator_blocks,
                                  planes=self.args.planes_generator,
                                  padding_mode=self.args.generator_padding_mode,
                                  activation=self.args.activation_function,
                                  zero_init_residual=self.args.zero_init_residuals,
                                  resnet_weight_init=self.args.generator_resnet_weight_init,
                                  final_activation=self.args.final_activation_generator,
                                  normalization_layer=self.args.normalization_layer,
                                  groupnorm_num_groups=self.args.groupnorm_num_groups,
                                  identity_init_last_layer=self.args.identity_init_last_layer
                                  ).to(
            self.device0)
        self.G_X2Y = GeneratorNet(num_blocks=self.args.num_generator_blocks,
                                  planes=self.args.planes_generator,
                                  padding_mode=self.args.generator_padding_mode,
                                  activation=self.args.activation_function,
                                  zero_init_residual=self.args.zero_init_residuals,
                                  resnet_weight_init=self.args.generator_resnet_weight_init,
                                  final_activation=self.args.final_activation_generator,
                                  normalization_layer=self.args.normalization_layer,
                                  groupnorm_num_groups=self.args.groupnorm_num_groups,
                                  identity_init_last_layer=self.args.identity_init_last_layer
                                  ).to(self.device0)
        self.configure_optimizers()
        self.print_num_params()

    def configure_optimizers(self):
        self.print_logger.info('Using generator optimizer:', self.args.generator_optimizer)
        generator_optimizer_class = get_optimizer_class(self.args.generator_optimizer)
        self.print_logger.info('Using discriminator optimizer:', self.args.discriminator_optimizer)
        discriminator_optimizer_class = get_optimizer_class(self.args.discriminator_optimizer)

        self.optimizers_G = generator_optimizer_class(
            list(self.G_X2Y.parameters()) + list(self.G_Y2X.parameters()),
            self.args.learning_rate_generator,
            weight_decay=self.args.weight_decay)

        self.optimizers_D = discriminator_optimizer_class(
            list(self.D_Y.parameters()) + list(self.D_X.parameters()) + list(self.D_SX.parameters()),
            self.args.learning_rate_discriminator,
            weight_decay=self.args.weight_decay)

        return [self.optimizers_D, self.optimizers_G], []

    def select_device(self):
        return self.device1 if self.multi_gpus else self.device0

    def print_num_params(self):
        self.print_logger.info('Number of parameters:')
        self.print_logger.info(f'D_X: {calculate_num_parameters(self.D_X)}')
        self.print_logger.info(f'D_Y: {calculate_num_parameters(self.D_Y)}')
        self.print_logger.info(f'D_SX: {calculate_num_parameters(self.D_SX)}')
        self.print_logger.info(f'G_Y2X: {calculate_num_parameters(self.G_Y2X)}')
        self.print_logger.info(f'G_X2Y: {calculate_num_parameters(self.G_X2Y)}')
        self.print_logger.info(f'S_X (is not trained): {calculate_num_parameters(self.S_X)}')

    def log_hparams(self, current_loss=-1):
        hparams = {'lrd': str(self.args.learning_rate_discriminator), 'lrg': str(self.args.learning_rate_generator),
                   'cycle_consistency_lambda_min': str(self.args.cycle_consistency_lambda_min),
                   'cycle_consistency_lambda_max': str(self.args.cycle_consistency_lambda_max),

                   'batch_size': str(self.args.batch_size),
                   }
        hparams.update(self.args.__dict__)

        # todo: add valid_metrics (metrics.Accuracy(), metrics.Precision() ...) for s_x2y_2x and s_y2x (if GT available)

        hparams = {key: str(value) if (type(value) is not int and type(value) is not float) else value for key, value in
                   hparams.items()}
        self.logger.experiment.add_hparams(hparam_dict=hparams,
                                           metric_dict={'hparam/current_loss': current_loss})

    def image_tb_logging_x(self, x, is_training, seg_data_sample, ground_truth):
        try:
            self.set_train_eval_mode(False)

            x = x.to(self.device0)
            mode = ('training' if is_training else 'validation') + '_from_' + (
                'secgan_training' if ground_truth is None else 'segmenter_training')

            with torch.no_grad():
                x2y = self.G_X2Y(x)
                x2y_2x = self.G_Y2X(x2y)
                s_x2y_2x = self.S_X(x2y_2x.to(self.select_device()))
                dx_x = self.D_X(x)
                dx_x2y_2x = self.D_X(x2y_2x)
                dy_x2y = self.D_Y(x2y)
                ds_x_s_x2y_2x = self.D_SX(s_x2y_2x.to(self.device0))
                ds_x_s_x = self.D_SX(seg_data_sample.to(self.device0))
            x_cropped, x2y_2x_cropped = center_crop(x, x2y_2x)
            x_diff_x2y_2x = ((x_cropped - x2y_2x_cropped) + 1) / 2  # to ensure value range in [0,1]

            if self.global_step == 0:
                self.log_img_center(mode, 'x', x)
                self.log_segmentation(mode, 's_x', seg_data_sample, None, True, False)
                self.log_segmentation(mode, 's_x overlay', x, seg_data_sample, False, True)
                if ground_truth is not None:
                    self.log_segmentation(mode, 'target (for x)', ground_truth, None, False, False)
                    self.log_segmentation(mode, 'x target overlay', x, ground_truth, False, False)

                    self.logger.experiment.add_scalar(f'{mode} s(x) loss',
                                                      self.logging_data.s_x_training_criterion(seg_data_sample,
                                                                                               ground_truth),
                                                      global_step=self.global_step)
                    # this loss should be close to the loss in the training of s(x)

            self.log_img_center(mode, 'x2y', x2y)
            self.log_img_center(mode, 'x2y_2x', x2y_2x)
            self.log_img_center(mode, 'dx_x', dx_x)
            self.log_img_center(mode, 'dy_x2y', dy_x2y)
            self.log_img_center(mode, 'dx_x2y_2x', dx_x2y_2x)
            self.log_img_center(mode, 'x_diff_x2y_2x', x_diff_x2y_2x)
            self.log_img_center(mode, 'ds_x_s_x2y_2x', ds_x_s_x2y_2x)
            self.log_img_center(mode, 'ds_x_s_x', ds_x_s_x)
            self.log_segmentation(mode, 's_x2y_2x', s_x2y_2x,
                                  None, True, False)  # for sanity check: how far away is the segmentation?
            self.log_segmentation(mode, 's_x2y_2x overlay', x2y_2x, s_x2y_2x, False, True)

            if ground_truth is not None:
                self.logger.experiment.add_scalar(f'{mode} s(x2y_2x) loss',
                                                  self.logging_data.s_x_training_criterion(
                                                      *center_crop(s_x2y_2x.detach().cpu(),
                                                                   ground_truth.detach().cpu())),
                                                  global_step=self.global_step)

        except RuntimeError as e:
            self.print_logger.info(e)

    def image_tb_logging_y(self, y, is_training, ground_truth):
        try:
            self.set_train_eval_mode(False)
            y = y.to(self.device0)
            mode = ('training' if is_training else 'validation') + '_from_' + (
                'secgan_training' if ground_truth is None else 'y_ground_truth')

            with torch.no_grad():
                y2x = self.G_Y2X(y)
                y2x_2y = self.G_X2Y(y2x)
                s_y2x = self.S_X(y2x.to(self.select_device()))
                dy_y = self.D_Y(y)
                dy_y2x_2y = self.D_Y(y2x_2y)
                dx_y2x = self.D_X(y2x)
                dsx_s_y2x = self.D_SX(s_y2x.to(self.device0))

            y_cropped, y2x_2y_cropped = center_crop(y, y2x_2y)
            y_diff_y2x_2y = ((y_cropped - y2x_2y_cropped) + 1) / 2

            if self.global_step == 0:
                self.log_img_center(mode, 'y', y)
                if ground_truth is not None and self.args.gt_available:
                    self.log_segmentation(mode, 'target (for y)', ground_truth, None, False, False)
                    self.log_segmentation(mode, 'target (for y) overlay', y, ground_truth, False, False)

            self.log_img_center(mode, 'y2x', y2x)
            self.log_img_center(mode, 'y2x_2y', y2x_2y)
            self.log_img_center(mode, 'dy_y', dy_y)
            self.log_img_center(mode, 'dy_y2x_2y', dy_y2x_2y)
            self.log_img_center(mode, 'dx_y2x', dx_y2x)
            self.log_img_center(mode, 'dsx_s_y2x', dsx_s_y2x)
            self.log_img_center(mode, 'y_diff_y2x_2y', y_diff_y2x_2y)

            self.log_segmentation(mode, 's_y2x', s_y2x, None, True, False)
            self.log_segmentation(mode, 's_y2x overlay', y2x, s_y2x, False, True)

            if ground_truth is not None and self.args.gt_available:
                y_segmentation_loss = self.logging_data.y_ground_truth_criteria['combined'](
                    *center_crop(s_y2x.detach().cpu(), ground_truth.long().detach().cpu()))
                self.logger.experiment.add_scalar(f'{mode} s(s_y2x) loss',
                                                  y_segmentation_loss,
                                                  global_step=self.global_step)
                if y_segmentation_loss < self.best_y_segmentation_loss:
                    self.best_y_segmentation_loss = y_segmentation_loss

            torch.cuda.empty_cache()
        except RuntimeError as e:
            self.print_logger.info(e)

    def log_img_center(self, mode, name, img):
        self.logger.experiment.add_image(f'{mode} {name}',
                                         torchvision.utils.make_grid(img[:, :, img.shape[2] // 2, :, :],
                                                                     nrow=img.shape[0]),
                                         global_step=self.global_step)

    def log_segmentation(self, mode, name, img, overlay_raw, img_argmax=False, overlay_argmax=False):
        image = img[:, :, img.shape[2] // 2, :, :].cpu().data.numpy() \
            if len(img.shape) == 5 else img[:, img.shape[1] // 2, :, :].cpu().data.numpy()
        if overlay_raw is not None:
            overlay = overlay_raw[:, :, overlay_raw.shape[2] // 2, :, :].cpu().data.numpy() \
                if len(overlay_raw.shape) == 5 else overlay_raw[:, overlay_raw.shape[1] // 2, :, :].cpu().data.numpy()
        self.logger.experiment.add_figure(f'{mode} {name}',
                                          [plot_image(
                                              image=image[i].argmax(0) if img_argmax else image[i].squeeze(),
                                              overlay=(overlay[i].argmax(0) if overlay_argmax else overlay[i].squeeze())
                                              if overlay_raw is not None else None,
                                              vmin=0,
                                              vmax=self.args.number_classes,
                                              cmap=get_cmap(self.args.number_classes),
                                              overlay_alpha=0.4) for i in
                                              range(img.shape[0])],
                                          global_step=self.global_step)

    def gradient_tb_logging(self, model, tag):
        for name, param in model.named_parameters():
            self.logger.experiment.add_histogram(tag=tag + f' param/{name}', values=param, global_step=self.global_step)
            grad = param.grad if param.grad is not None else torch.tensor(0)
            self.logger.experiment.add_histogram(tag=tag + f' grad/{name}', values=grad, global_step=self.global_step)

# todo: deal with resolution mismatch (different voxel sizes) -> (biqubic?) upsample
#  to small voxel-size?/downsample to big? Or adjust model architecture accordingly such that both patches from X and
#  Y cover (almost) exactly the same real-world volume whilst having a different number of voxels (and also different
#  height/width ratio)?
# todo: restart with incremented seed in case of intensity inversion
