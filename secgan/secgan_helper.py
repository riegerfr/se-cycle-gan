# ELEKTRONN3 - Neural Network Toolkit
#
# Copyright (c) 2017 - now
# Max Planck Institute of Neurobiology, Munich, Germany
# Authors: Anushka Vashishtha and Franz Rieger


import math
from collections import deque

import numpy as np
import torch
from pytorch_lightning import Callback


class SECGANConfig:
    def __init__(self, load_segmenter: str,
                 activation_function='swish', batch_size=2, batch_size_logging=2,
                 cycle_consistency_lambda_change_factor=0.1,
                 cycle_consistency_lambda_max=1.0, cycle_consistency_lambda_min=0.5,
                 deterministic=False, discriminator_buffer_size=3,
                 discriminator_dropout=0.0, discriminator_final_normalization=False, discriminator_loss_weight=0.1,
                 discriminator_optimizer='Adam', discriminator_padding_mode='same',
                 final_activation_discriminator='none',
                 final_activation_generator='sigmoid', final_layer_fast_init=True, generator_optimizer='Adam',
                 generator_padding_mode='valid', generator_resnet_weight_init=False,
                 groupnorm_num_groups=2, gt_available=True, identity_init_last_layer=True,
                 input_gaussian_noise=0.0001, input_normalization=False, inversion_epochs=2,
                 inversion_threshold=0.7,
                 learning_rate_discriminator=0.001, learning_rate_generator=0.001,
                 normalization_layer='batchnorm',
                 num_generator_blocks=4, number_classes=4,
                 planes_generator=64,
                 resnet_multiplier=8, seed=0,
                 verbose=False,
                 weight_decay=1e-08,
                 zero_init_residuals=True, **kwargs
                 ):
        """
        The configuration for the Segmentation-Enhanced CycleGAN

        :param activation_function: What activation function to use for the models.
            One of "relu", "leaky_relu" or "swish"
        :param batch_size: The batch size for training
        :param batch_size_logging: The batch size for logging
        :param cycle_consistency_lambda_change_factor: Multiply/divide cycle_consistency_lambda with/by this value
            after each epoch
        :param cycle_consistency_lambda_max: The maximal increase over cycle_consistency_lambda_min
        :param cycle_consistency_lambda_min: The initial cycle_consistency_lambda
        :param deterministic: Whether to train deterministically
        :param discriminator_buffer_size: Buffer size of the discriminators
        :param discriminator_dropout: The dropout used in the discriminators
        :param discriminator_final_normalization: Whether to add a normalization layer after the discriminators
        :param discriminator_loss_weight: Weight of the discriminator loss for the generators
        :param discriminator_optimizer: Optimizer of the discriminators. One of "Adam" or "SGD"
        :param discriminator_padding_mode: The padding mode of the convolutions of the discriminators.
            One of "same" or "valid"
        :param final_activation_discriminator: Final activation function of the discriminators.
            One of "sigmoid" or "none"
        :param final_activation_generator: Final activation function of the generators.
            One of ['sigmoid', 'tanh', 'algebraic_sigmoid', 'none', 'rescale']
        :param final_layer_fast_init: Whether to initialize the final layer of the discriminators s.t. they produce
        the trivially optimal output (0.5) initially (when using "none" for final_activation_discriminator)
        :param generator_optimizer: Optimizer of the generators. One of "Adam" or "SGD"
        :param generator_padding_mode: The padding mode of the convolutions of the generators.
            One of "same" or "valid"
        :param generator_resnet_weight_init: Whether to initialize the weight of the generator blocks like Resnet
        :param groupnorm_num_groups: The number of groups in case group normalization is used
        :param gt_available: Whether ground truth is available for logging and evaulation
        :param identity_init_last_layer: Initialize the last layer of the generators as the identity s.t. they produce
            the identity initially to avoid intnesity inversion
        :param input_gaussian_noise: The variance of the Gaussian Noise on the inputs of the discriminators
        :param input_normalization: Whether to normalize mean and variance of the discriminators' inputs
        :param inversion_epochs: Compute the average intensity inversion over this many epochs
        :param inversion_threshold: The threshold of the intensity inversion ratio s.t. it is detected
        :param learning_rate_discriminator: The learning rate for the discriminators
        :param learning_rate_generator: The learning rate for the generators
        :param load_segmenter: The path to the stored segmenter model
        :param normalization_layer: The normalization to use in the generators and discriminators.
            One of "batchnorm" or "groupnorm"
        :param num_generator_blocks: The number of residual blocks in the generators
        :param number_classes: The number of classes the segmentation model classifies
        :param planes_generator: The number of planes in the generators
        :param resnet_multiplier: The multiplier in the discriminators
        :param seed: The RNG seed
        :param verbose: Whether to print additional information
        :param weight_decay: The weight decay for the optimizers
        :param zero_init_residuals: Whether to init the residual blocks of the generators s.t. they produce the
            identity to avoid intensity inversion (when combined with identity_init_last_layer)
        """
        self.activation_function = activation_function
        self.batch_size = batch_size
        self.batch_size_logging = batch_size_logging
        self.cycle_consistency_lambda_change_factor = cycle_consistency_lambda_change_factor
        self.cycle_consistency_lambda_max = cycle_consistency_lambda_max
        self.cycle_consistency_lambda_min = cycle_consistency_lambda_min
        self.deterministic = deterministic
        self.discriminator_buffer_size = discriminator_buffer_size
        self.discriminator_dropout = discriminator_dropout
        self.discriminator_final_normalization = discriminator_final_normalization
        self.discriminator_loss_weight = discriminator_loss_weight
        self.discriminator_optimizer = discriminator_optimizer
        self.discriminator_padding_mode = discriminator_padding_mode
        self.final_activation_discriminator = final_activation_discriminator
        self.final_activation_generator = final_activation_generator
        self.final_layer_fast_init = final_layer_fast_init
        self.generator_optimizer = generator_optimizer
        self.generator_padding_mode = generator_padding_mode
        self.generator_resnet_weight_init = generator_resnet_weight_init
        self.groupnorm_num_groups = groupnorm_num_groups
        self.gt_available = gt_available
        self.identity_init_last_layer = identity_init_last_layer
        self.input_gaussian_noise = input_gaussian_noise
        self.input_normalization = input_normalization
        self.inversion_epochs = inversion_epochs
        self.inversion_threshold = inversion_threshold
        self.learning_rate_discriminator = learning_rate_discriminator
        self.learning_rate_generator = learning_rate_generator
        self.load_segmenter = load_segmenter
        self.normalization_layer = normalization_layer
        self.num_generator_blocks = num_generator_blocks
        self.number_classes = number_classes
        self.planes_generator = planes_generator
        self.resnet_multiplier = resnet_multiplier
        self.seed = seed
        self.verbose = verbose
        self.weight_decay = weight_decay
        self.zero_init_residuals = zero_init_residuals


def get_optimizer_class(optimizer_name):
    if optimizer_name == 'Adam':
        optimizer_class = torch.optim.Adam  # set beta1 to 0.5? see:
        # https://www.kaggle.com/c/generative-dog-images/discussion/98993
        # or https://machinelearningmastery.com/how-to-train-stable-generative-adversarial-networks/
    elif optimizer_name == 'SGD':
        optimizer_class = torch.optim.SGD
    else:
        raise ValueError(f"optimizer must be either 'Adam' or 'SGD', not {optimizer_name}")
    return optimizer_class


class IntensityInversionStoppingCallback(Callback):
    def __init__(self, inversion_epochs=10, inversion_threshold=0.9):
        super().__init__()
        self.inversion_count_train = deque()
        self.inversion_epochs = inversion_epochs
        self.inversion_threshold = inversion_threshold

    def on_validation_end(self, trainer, pl_module):

        logs = trainer.callback_metrics
        if 'validation_intensity_inversion_ratio' in logs.keys():
            intensity_inversion_ratio = logs['validation_intensity_inversion_ratio']
            self.inversion_count_train.append(intensity_inversion_ratio)
            if len(self.inversion_count_train) >= self.inversion_epochs:
                if np.array(self.inversion_count_train).mean() > self.inversion_threshold:
                    self.secgan_logger.info('stopping training due to intensity inversion')
                    trainer.should_stop = True
                self.inversion_count_train.popleft()


def calculate_num_parameters(model):
    num_params = sum(p.numel() for p in model.parameters() if p.requires_grad)
    return num_params


def subtract(a, b):
    return a - b


def center_crop(a, b):
    a_dim = list(a.size())
    b_dim = list(b.size())

    for i in range(-1, -4, -1):
        # take the last 3 dimensions. a and b don't need to have the same number of dimensions
        # (i.e. only one has channels)
        if a_dim[i] != b_dim[i]:
            if a_dim[i] > b_dim[i]:
                crop_val = (a_dim[i] - b_dim[i]) / 2
            else:
                crop_val = (b_dim[i] - a_dim[i]) / 2

            left = math.floor(crop_val)
            right = -math.ceil(crop_val)

            if a_dim[i] > b_dim[i]:
                slice_window = tuple(
                    [slice(None)] * (len(a.size()) - 3) + [slice(left, right) if i == j else slice(None) for j in
                                                           range(-1, -4, -1)])
                a = a[slice_window]
            else:
                slice_window = tuple(
                    [slice(None)] * (len(b.size()) - 3) + [slice(left, right) if i == j else slice(None) for j in
                                                           range(-1, -4, -1)])
                b = b[slice_window]

    return a, b


def detect_intensity_inversion(x2y, x2y_2x):
    # compute possible intensity inversion
    x2y_center_cropped, x2y_2x_center_cropped = center_crop(x2y, x2y_2x)
    # original voxels as center_crop(x, x2y_2x) in the cycle consistency loss calculation
    batchsize = x2y.shape[0]
    max_indices_batch_x2y = torch.argmax(x2y_center_cropped.reshape(batchsize, -1), dim=1)
    min_indices_batch_x2y = torch.argmin(x2y_center_cropped.reshape(batchsize, -1), dim=1)
    x2y_2x_max = x2y_2x_center_cropped.reshape(batchsize, -1)[range(batchsize), max_indices_batch_x2y]
    x2y_2x_min = x2y_2x_center_cropped.reshape(batchsize, -1)[range(batchsize), min_indices_batch_x2y]
    return (x2y_2x_max < x2y_2x_min).cpu().data.numpy().mean()
