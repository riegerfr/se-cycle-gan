# ELEKTRONN3 - Neural Network Toolkit
#
# Copyright (c) 2017 - now
# Max Planck Institute of Neurobiology, Munich, Germany
# Authors: Anushka Vashishtha and Franz Rieger


import logging
import os
from datetime import datetime

import pytorch_lightning as pl
from pytorch_lightning.loggers import TensorBoardLogger
from pytorch_lightning.profiler import AdvancedProfiler

from secgan.dataset_specific.parsing import parse_args
from secgan.secgan_data import SECGANDataConfig, SECGANDataModule, setup_logging_data
from secgan.secgan_helper import SECGANConfig, IntensityInversionStoppingCallback
from secgan.secgan_module import SECGANModule


def train():
    args = parse_args()
    if args.experiment_name:
        exp_name = args.experiment_name
    else:
        exp_name = datetime.now().strftime('%y-%m-%d_%H-%M-%S-%f')

    secgan_logger = logging.getLogger('se_cycle_gan_logs_' + exp_name)

    save_path = args.save_path if os.getenv('CLUSTER') == 'WHOLEBRAIN' else os.path.expanduser(args.save_path)
    save_dir = os.path.join(save_path, exp_name + '/')
    os.makedirs(save_dir, exist_ok=True)
    print(f"save dir: {save_dir}")
    tb_logger = TensorBoardLogger(save_dir=save_dir, name=exp_name)
    tb_logger.experiment.add_text('args', str(args))
    tb_logger.experiment.add_text('save dir', save_dir)

    # set up data
    secgan_data_config = SECGANDataConfig(**vars(args))
    data_module = SECGANDataModule(secgan_data_config)
    logging_data = setup_logging_data(secgan_data_config)

    secgan_config = SECGANConfig(**vars(args))
    model = SECGANModule(config=secgan_config, logging_data=logging_data, print_logger=secgan_logger)
    if args.profile:
        profiler = AdvancedProfiler(output_filename=save_dir + "profiler_output.txt")
    trainer = pl.Trainer(logger=tb_logger,
                         callbacks=[IntensityInversionStoppingCallback()],
                         profiler=profiler if args.profile else None)  # , gpus=torch.cuda.device_count() )
    trainer.fit(model=model, datamodule=data_module)


if __name__ == '__main__':
    train()
