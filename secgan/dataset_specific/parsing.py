import argparse
from argparse import ArgumentParser

from secgan.dataset_specific.constants import KNOSSOS_J0126_PATH, BOUNDS_J0126, BOUNDS_J0251, \
    TRAIN_KNOSSOS_BOUNDS_J0251, \
    VAL_KNOSSOS_BOUNDS_J0251, DEBUG_KNOSSOS_BOUNDS_J0251


def parse_args():
    parser: ArgumentParser = argparse.ArgumentParser(description='Train a SECGAN.')

    parser.add_argument(
        '-ls', '--load-segmenter', metavar='PATH',
        default='/scratch/riegerfr/e3training/Sequential__21-01-25_15-49-21/model_final.pt',
        help='path to the model.pt file of the required pretrained segmentation network'
    )
    parser.add_argument(
        '-kx', '--knossos-path-x', metavar='PATH',
        default=KNOSSOS_J0126_PATH,
        help='Path to the X dataset (knossos config)'
    )
    parser.add_argument(
        '-ky', '--knossos-path-y', metavar='PATH',
        default='/wholebrain/songbird/j0251/j0251_72_clahe2/mag1/knossos.conf',
        help='Path to the Y dataset (knossos config)'
    )
    parser.add_argument(
        '-ygtd', '--y-ground-truth-dir', metavar='PATH',
        default='/wholebrain/songbird/j0251/groundtruth/segmentation_gt',
        help='Path to the ground truth of Y (if available) for logging purposes and for measuring performance'
    )
    parser.add_argument(
        '-sp', '--save-path', metavar='PATH', default='/wholebrain/scratch/riegerfr/se_cycle_gan_training_pl/',
        help='Path to save the model at'
    )
    parser.add_argument(
        '-en', '--experiment-name', default=None,
        help='Experiment name'
    )
    parser.add_argument(
        '--deterministic', action='store_true',
        help='Run in fully deterministic mode (at the cost of execution speed).'
    )
    parser.add_argument(
        '-est', '--epoch-size-train', type=int, default=500,
        help='How many training samples to process in a train epoch'
    )
    parser.add_argument(
        '-esv', '--epoch-size-val', type=int, default=20,
        help='How many validation samples to process in a val epoch'
    )
    parser.add_argument(
        '-gtes', '--ground-truth-epoch-size', type=int, default=64,
        help='How many validation samples to process for loss on gt  calculation'
    )

    parser.add_argument(
        '-lrg', '--learning-rate-generator', type=float, default=0.001,
        help='Learning rate used in optimizers for generators'
    )
    parser.add_argument(
        '-lrd', '--learning-rate-discriminator', type=float, default=0.001,
        help='Learning rate used in optimizers for discriminators'
    )

    parser.add_argument(
        '-clc', '--cycle-consistency-lambda-change-factor', type=float, default=0.1,
        help='Change factor for the cycle-consistency weight per epoch'
    )
    parser.add_argument(
        '-dlw', '--discriminator-loss-weight', type=float, default=0.1,
        help='Weight of the discriminator loss'
    )
    parser.add_argument(
        '-b', '--batch-size', type=int, default=2,
        help='Batch size'
    )
    parser.add_argument(
        '-bl', '--batch-size-logging', type=int, default=4,
        help='Batch size for logging the constant samples'
    )
    parser.add_argument(
        '-se', '--seed', type=int, default=0,
        help='Seed for RNGs'
    )
    parser.add_argument(
        '-nc', '--number-classes', type=int, default=4,
        help='Number of classes for the segmentation model, 4 for subcellular data, 3 for synapse type data'
    )
    parser.add_argument(
        '-ng', '--num-generator-blocks', type=int, default=4,
        # in paper it was 8 but with a patchshape of 44*88*88 and valid padding this requires too much memory
        help='Number of residual blocks in the generators'
    )
    parser.add_argument(
        '-np', '--planes-generator', type=int, default=64,  # was 32 in paper, but we use fewer blocks
        help='Number of planes in the generators'
    )
    parser.add_argument(
        '-rm', '--resnet-multiplier', type=int, default=8,
        help='Multiplier for the number of features in the resnets'
    )
    parser.add_argument(
        '-clmi', '--cycle-consistency-lambda-min', type=float, default=0.5,
        help='Minimal weight of the cycle-consistency loss of the generators (other loss part: from discriminators)'
    )
    parser.add_argument(
        '-clma', '--cycle-consistency-lambda-max', type=float, default=1.0,
        help='Maximal weight of the cycle-consistency loss (added to minimal weight), cycles'
    )
    parser.add_argument(
        '-af', '--aniso-factor', type=int, default=2,
        help='the aniso factor for the dataset'
    )
    parser.add_argument(
        '-ps', '--patch-shape', nargs='+', type=int, default=[88, 88, 88],
        help='The shape of the patches which are sampled, zxy'
    )
    parser.add_argument(
        '-ln', '--label-names', nargs='+', type=str, default=['sj', 'vc', 'mitos'],
        help='The names of the labels'
    )
    parser.add_argument(
        '-psl', '--patch-shape-logging', nargs='+', type=int, default=[64, 200, 200],
        help='The shape of the patches for tensorboard logging (and ground truth loss calc.) which are sampled, zxy'
    )
    parser.add_argument(
        '-pslt', '--patch-shape-logging-segmenter-training-constant-samples-X', nargs='+', type=int,
        default=[44, 88, 88],
        help='The shape of the patches for tensorboard logging for previous training of segmenter'
    )
    parser.add_argument(
        '-nw', '--num-workers', type=int, default=5,
        help='how many subprocesses to use for data loading. 0 means that the data will be loaded in the main process'
    )
    parser.add_argument(
        '-vb', '--verbose', action='store_true',
        help='ensures logging/printing all statements'
    )
    parser.add_argument(
        '-gta', '--gt-available', action='store_true',
        help='if GT for Y available, do extra logging'
    )
    parser.add_argument(
        '-ms', '--memory-stats', action='store_true',
        help='prints all memory statuses for all devices available'
    )
    parser.add_argument(
        '-sa', '--skip-asserts', action='store_true',
        help='for debugging: skip assertions to ensure data in meaningful value range'
    )
    parser.add_argument(
        '-xtb', '--x-train-bounds', nargs='+', type=int, default=BOUNDS_J0126,
        help='Bounds for the train X, format: x y z corner x y z opposite corner'
    )
    parser.add_argument(
        '-ytb', '--y-train-bounds', nargs='+', type=int, default=BOUNDS_J0251,
        help='Bounds for the train Y'
    )
    parser.add_argument(
        '-xvb', '--x-val-bounds', nargs='+', type=int, default=BOUNDS_J0126,
        help='Bounds for the val X'
    )
    parser.add_argument(
        '-yvb', '--y-val-bounds', nargs='+', type=int, default=BOUNDS_J0251,
        help='Bounds for the val Y'  # todo: set different bound to avoid data leakage
    )
    parser.add_argument(
        '-cs', '--cache-size', type=int, default=1,
        help='Number of elements (patches) in the dataloaders\' caches'
    )
    parser.add_argument(
        '-', '--discriminator-buffer-size', type=int, default=50,
        help='Number of batches in the buffer for discriminator training. For deactivating, set to 0'
    )
    parser.add_argument(
        '-cr', '--cache-reuses', type=int, default=1,
        help='How often to reuse the elements in the caches of the dataloaders'
    )
    parser.add_argument(
        '-vi', '--valid-indices', type=int, default=2,
        help='index referring to the number of h5 file chosen for validation'
    )
    parser.add_argument(
        '-im', '--in-memory', action='store_true',
        help='from KnossosRawData: If True (default), the dataset (or the subregion that is constrained by '
             'bounds) is pre-loaded into memory on initialization '
    )
    parser.add_argument(
        '-at', '--deactivate-transforms', action='store_true',
        help='activate transforms for data loading.'
    )
    parser.add_argument('-zir', '--zero-init-residuals', type=bool, default=True,
                        help='initialize residual blocks s.t. they perform identity')

    parser.add_argument('-iill', '--identity-init-last-layer', type=bool, default=True,
                        help='initialize last layer of generators s.t. they perform identity '
                             'if ``zero-init-residuals``==True')

    parser.add_argument(
        '-grwi', '--generator-resnet-weight-init', action='store_true',
        help='initialize generator like resnet'
    )
    parser.add_argument(
        '-ie', '--inversion-epochs', type=int, default=2,
        help='checks intensity inversion in the last "x" epochs'
    )
    parser.add_argument(
        '-dd', '--discriminator-dropout', type=float, default=0.0,
        help='dropout for the discriminators'
    )
    parser.add_argument(
        '-it', '--inversion-threshold', type=float, default=0.7,
        help='inversion threshold for restarting training'
    )
    parser.add_argument(
        '-wd', '--weight-decay', type=float, default=1e-8,
        help='Weight decay for optimizers'
    )
    parser.add_argument(
        '-ign', '--input-gaussian-noise', type=float, default=1e-4,
        help='Gaussian noise as input for discriminators'
    )
    parser.add_argument(
        '-in', '--input-normalization', type=bool, default=False,
        help='Input normalization for discriminators'
    )
    parser.add_argument(
        '-dfn', '--discriminator-final-normalization', type=bool, default=False,
        help='Normalize before final layer of discriminator'
    )
    parser.add_argument(
        '-flfi', '--final-layer-fast-init', type=bool, default=True,
        help='Initialize discriminator close to trivial optimum'
    )
    parser.add_argument(
        '-go', '--generator-optimizer', type=str, default='Adam',
        help='optimiser used in cycleGAN for generators'
    )
    parser.add_argument(
        '-do', '--discriminator-optimizer', type=str, default='Adam',
        help='optimiser used in cycleGAN for discriminators'
    )
    parser.add_argument(
        '-dat', '--datatype', type=str, default='cellorganelle',
        help='the type of the data'
    )
    parser.add_argument(
        '-gpm', '--generator-padding-mode', type=str, default="valid",
        help='padding mode for the generator models. One of "valid" or "same"'
    )
    parser.add_argument(
        '-dpm', '--discriminator-padding-mode', type=str, default="same",
        help='padding mode for the discriminator models. One of "valid" or "same"'
    )
    parser.add_argument(
        '-act', '--activation-function', type=str, default="leaky_relu",
        help='what activation function to use. One of "relu", "leaky_relu" or "swish"'
    )
    parser.add_argument(
        '-factg', '--final-activation-generator', type=str, default="sigmoid",
        help='what activation function to use in the last layer of the generators. One of "sigmoid", "tanh", '
             '"algebraic_sigmoid", "rescale" or "none". Make sure the value range is appropriate'
    )
    parser.add_argument(
        '-factd', '--final-activation-discriminator', type=str, default="none",
        help='what activation function to use in the last layer of the discriminators. One of "sigmoid" or "none"'
    )
    parser.add_argument(
        '-nl', '--normalization-layer', type=str, default="batchnorm",
        help='what normalization layer to use. One of "batchnorm" or "groupnorm"'
    )
    parser.add_argument(
        '-gnng', '--groupnorm-num-groups', type=int, default=2,
        help='if normalization_layer="groupnorm", number of groups'
    )
    parser.add_argument('-lo', '--label-offset', type=int, default=0,
                        help='Offset for knossos labels'
                        )
    parser.add_argument('-tb', '--train-bounds', type=list, default=TRAIN_KNOSSOS_BOUNDS_J0251,
                        help='bounds for training data in knossos labels'
                        )
    parser.add_argument('-vbounds', '--valid-bounds', type=list, default=VAL_KNOSSOS_BOUNDS_J0251,
                        help='bounds for validation data in knossos labels'
                        )
    parser.add_argument(
        '-lao', '--label-order', nargs='+', type=int, default=(0, 1, 3, 2),
        help='The order of the labels for j0251'
    )
    parser.add_argument(
        '-d', '--debug', type=bool, default=False,
        help='faster debug run'
    )
    parser.add_argument(
        '-dnx', '--dataset-name-x', type=str, default="j0126",
        help='The dataset to use for x: "j0251" or "j0126"'
    )
    parser.add_argument(
        '-dny', '--dataset-name-y', type=str, default="j0251",
        help='The dataset to use for y: "j0251" or "j0126"'
    )

    parser.add_argument('-nzo', '--normalize-zero-one', action='store_true',
                        help='Normalize the data to [0,1]'
                        )
    parser.add_argument('-pr', '--profile', action='store_true',
                        help='Profile run'
                        )

    args = parser.parse_args()

    if args.debug:
        args.train_bounds = DEBUG_KNOSSOS_BOUNDS_J0251
    print(args)
    return args
