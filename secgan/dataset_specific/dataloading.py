# ELEKTRONN3 - Neural Network Toolkit
#
# Copyright (c) 2017 - now
# Max Planck Institute of Neurobiology, Munich, Germany
# Authors: Anushka Vashishtha and Franz Rieger


import os
from pathlib import Path

import numpy as np
import torch
from elektronn3.data import transforms, get_preview_batch, utils, PatchCreator
from elektronn3.data.knossos import KnossosRawData
from elektronn3.data.knossos_labels import KnossosLabels
from elektronn3.data.utils import _to_full_numpy
from torch.utils.data import DataLoader, Dataset

from secgan.dataset_specific.constants import BOUNDS_J0251


def normalize_zero_one(inp, target):
    return normalize_zero_one_no_target(inp), target


def normalize_zero_one_no_target(inp):
    eps = 1e-10
    inp_min = inp.min()
    inp_max = inp.max()
    inp = (inp - inp_min) / (inp_max - inp_min + eps)
    # assert inp.min() >= 0.0 and inp.max() <= 1.0
    return inp


def get_transforms(deactivate_transforms, do_normalize_zero_one=True, divide=1):
    # Transformations to be applied to samples before feeding them to the network

    common_transforms = [
        transforms.SqueezeTarget(dim=None),  # Workaround for neuro_data_cdhw
        # transforms.Lambda(normalize_zero_one)
        transforms.Lambda(lambda x, y: (x / divide, y)),
        # map from uint8 (in the knossos files), i.e. [1,255] to [0,1]
        # transforms.Normalize(mean=dataset_mean, std=dataset_std)
    ]

    if deactivate_transforms:
        active_transforms = []
    else:
        active_transforms = [transforms.RandomRotate2d(prob=0.9),
                             transforms.RandomGrayAugment(channels=[0], prob=0.3),
                             transforms.RandomGammaCorrection(gamma_std=0.25, gamma_min=0.25, prob=0.3),
                             transforms.AdditiveGaussianNoise(sigma=0.1, channels=[0], prob=0.3),
                             ]
    if do_normalize_zero_one:
        common_transforms.append(transforms.Lambda(normalize_zero_one))
        active_transforms.append(transforms.Lambda(normalize_zero_one))

    train_transform = transforms.Compose(common_transforms + active_transforms)
    valid_transform = transforms.Compose(common_transforms)

    return train_transform, valid_transform


def load_labeled_dataset(datatype, dataset_name, calc_stat, class_wt_mode, label_offset=0,
                         knossos_bounds=None, aniso_factor=2, patch_shape=(44, 88, 88),
                         deactivate_random_transforms=True, epoch_size_valid=10, epoch_size_train=10,
                         label_order=(0, 1, 3, 2), normalize_zero_one=True, label_names=('sj', 'vc', 'mitos')):
    train_transform, valid_transform = get_transforms(deactivate_transforms=deactivate_random_transforms,
                                                      do_normalize_zero_one=normalize_zero_one)

    common_data_kwargs = {  # Common options for training and valid sets.
        'aniso_factor': aniso_factor,
        'patch_shape': patch_shape,
    }
    preview_shape = (32, 320, 320)
    if dataset_name == "j0126":
        if datatype == 'mixed':
            datatypes = ("golgi", "er")
            datatypes_weight = (0.5, 0.5)
            aniso_factor = 2

            train_dataset = MixedDataset(train_transform, training=True, datatypes=datatypes,
                                         datatypes_weight=datatypes_weight, aniso_factor=aniso_factor,
                                         size=epoch_size_train,
                                         common_data_kwargs=common_data_kwargs)

            valid_dataset = MixedDataset(valid_transform, training=False, datatypes=datatypes,
                                         datatypes_weight=datatypes_weight, aniso_factor=aniso_factor,
                                         size=epoch_size_valid,
                                         common_data_kwargs=common_data_kwargs)
            preview_batch = None
        else:
            input_h5data, valid_indices, train_dataset, valid_dataset = load_h5_dataset(common_data_kwargs, datatype,
                                                                                        epoch_size_train,
                                                                                        train_transform,
                                                                                        valid_transform, aniso_factor,
                                                                                        dataset_name)
            # Use first validation cube for previews. Can be set to any other data source.
            preview_batch = get_preview_batch(  # todo: get different preview batch for knossos_dataset!
                h5data=input_h5data[valid_indices[0]],  # todo: fix this and find usage
                preview_shape=preview_shape,
            )  # todo: compare this with very same batch in secgan trainer (plot)


    elif dataset_name == 'j0251':
        train_dataset, valid_dataset, preview_batch = load_knossos_labeled_dataset(label_order=label_order,
                                                                                   common_data_kwargs=common_data_kwargs,
                                                                                   epoch_size_train=epoch_size_train,
                                                                                   knossos_bounds=knossos_bounds,
                                                                                   label_offset=label_offset,
                                                                                   train_transform=train_transform,
                                                                                   valid_transform=valid_transform,
                                                                                   dataset_name=dataset_name,
                                                                                   preview_shape=preview_shape,
                                                                                   epoch_size_valid=epoch_size_valid,
                                                                                   label_names=label_names)


    else:
        raise NotImplementedError(f"Dataset {dataset_name} not implemented")

    if calc_stat:
        class_weights_normalised = calc_dataset_stat(class_wt_mode, dataset_name,
                                                     train_dataset,
                                                     label_order=label_order,
                                                     datatype=datatype)
    else:
        class_weights_normalised = []

    return class_weights_normalised, train_dataset, valid_dataset, preview_batch


def calc_dataset_stat(class_wt_mode, dataset_name, train_dataset, label_order=(0, 1, 3, 2), datatype="other"):
    # datatype: mixed or other
    # utils can also be used to calculate mean and std
    if dataset_name == 'j0251':
        class_weights = torch.tensor(
            utils.calculate_class_weights([target['data'] for inp, target in train_dataset.inp_targets],
                                          mode=class_wt_mode))  # todo: move this to knossos labels?
        class_weights = class_weights[list(label_order)]
    else:  # j0126
        if datatype == "mixed":
            ratios = []
            for dataset in train_dataset.datasets:
                targets = np.concatenate([_to_full_numpy(target).flatten() for target in dataset.targets])

                classes = np.arange(0, targets.max() + 1)
                assert len(classes) == 2
                # Count total number of labeled elements per class
                num_labeled = np.array([
                    np.sum(np.equal(targets, c))
                    for c in classes
                ], dtype=np.float32)
                # class_weights = (targets.size / (num_labeled + eps)).astype(np.float32)
                ratio = num_labeled / num_labeled.sum()
                ratios.append(ratio[1])

            assert sum(ratios) < 1  # assuming non overlapping, background is not empty
            ratios.insert(0, 1 - sum(ratios))  # or 1 -(sum(ratios)*len(ratios)) (and then normalize?)
            ratios = torch.tensor(ratios)
            class_weights = 1 / ratios
        else:
            class_weights = torch.tensor(utils.calculate_class_weights(train_dataset.targets, mode=class_wt_mode))

    class_weights_normalised = torch.tensor(
        [wt.numpy() / np.sum(class_weights.numpy()) for wt in class_weights])
    print("calculated normalized class weights", class_weights_normalised)
    return class_weights_normalised


def load_h5_dataset(common_data_kwargs, datatype, epoch_size, train_transform, valid_transform, aniso_factor, dataset):
    if dataset == "j0126":
        input_h5data, target_h5data, valid_indices = get_target_data_indices_j0126(datatype)
    else:
        raise NotImplementedError

    # todo: use load dataset to load datasets for the complete project
    train_dataset = PatchCreator(
        input_sources=[input_h5data[i] for i in range(len(input_h5data)) if i not in valid_indices],
        target_sources=[target_h5data[i] for i in range(len(input_h5data)) if i not in valid_indices],
        train=True,
        epoch_size=epoch_size,
        warp_prob=0.2,
        warp_kwargs={
            'sample_aniso': aniso_factor != 1,
            'perspective': True,
            'warp_amount': 1.0,
        },
        transform=train_transform,
        **common_data_kwargs
    )
    valid_dataset = None if not valid_indices else PatchCreator(
        input_sources=[input_h5data[i] for i in range(len(input_h5data)) if i in valid_indices],
        target_sources=[target_h5data[i] for i in range(len(input_h5data)) if i in valid_indices],
        train=False,
        epoch_size=10,  # How many samples to use for each validation run
        warp_prob=0,
        warp_kwargs={'sample_aniso': aniso_factor != 1},
        transform=valid_transform,
        **common_data_kwargs
    )
    return input_h5data, valid_indices, train_dataset, valid_dataset


def get_target_data_indices_j0126(datatype):
    local_path = get_local_path()
    if datatype == 'synapsetype':
        data_root = local_path + '/wholebrain/songbird/j0126/GT/synapsetype_gt/Segmentierung_von_Synapsentypen_v4/'
        fnames = sorted([f for f in os.listdir(data_root) if f.endswith('.h5')])
        input_h5data = [(os.path.join(data_root, f), 'raw') for f in fnames]
        target_h5data = [(os.path.join(data_root, f), 'label') for f in fnames]
        valid_indices = [2]
    elif datatype == 'cellorganelle':
        data_root = local_path + '/wholebrain/songbird/j0126/GT/cellorganelle_gt/'
        fnames_data = sorted([f for f in os.listdir(data_root) if f.endswith('e3.h5') and f.startswith('d')])
        fnames_labels = sorted([f for f in os.listdir(data_root) if f.endswith('e3.h5') and f.startswith('l')])
        input_h5data = [(os.path.join(data_root, f), 'raw') for f in fnames_data]
        target_h5data = [(os.path.join(data_root, f), 'label') for f in fnames_labels]
        # This is the h5 file with index 6. Check input_data_debug for corresponding indices
        valid_indices = [11]
    elif datatype == 'golgi':
        data_root = local_path + '/wholebrain/scratch/riegerfr/Golgi GT/'  # '/wholebrain/songbird/j0126/GT/Golgi GT/'#todo: introduce arg
        fnames = sorted([f for f in os.listdir(data_root) if f.endswith('.h5')])  # todo: exclude golgi
        input_h5data = [(os.path.join(data_root, f), 'raw') for f in fnames]
        target_h5data = [(os.path.join(data_root, f), 'label') for f in fnames]
        valid_indices = [2]  # todo: set different?
    elif datatype == 'er':
        data_root = local_path + '/wholebrain/songbird/j0126/GT/ER-ground_truth/'
        fnames = sorted([f for f in os.listdir(data_root) if
                         f.endswith('.h5')])  # todo: exclude jhendricks/negative cube negative training
        input_h5data = [(os.path.join(data_root, f), 'raw') for f in fnames]
        target_h5data = [(os.path.join(data_root, f), 'label') for f in fnames]
        valid_indices = [2]  # todo: set different?

    else:
        raise NotImplementedError
    # else:
    #     print("Using publicly available neuro_data_cdhw dataset")
    #     data_root = os.path.expanduser('~/neuro_data_cdhw/')
    #     input_h5data = [(os.path.join(data_root, f'raw_{i}.h5'), 'raw') for i in range(3)]
    #     target_h5data = [(os.path.join(data_root, f'barrier_int16_{i}.h5'), 'lab') for i in range(3)]
    #     valid_indices = [2]
    return input_h5data, target_h5data, valid_indices


def load_knossos_labeled_dataset(common_data_kwargs, epoch_size_train, knossos_bounds, label_offset, train_transform,
                                 valid_transform, dataset_name, preview_shape, epoch_size_valid=10,
                                 label_order=(0, 1, 3, 2), label_names=('sj', 'vc', 'mitos')):  # todo: use aniso_factor
    conf_path_label, conf_path_raw_data, dir_path_label = get_conf_dirs(dataset_name)

    train_dataset = KnossosLabels(label_order=label_order, conf_path_label=conf_path_label,
                                  # todo: label_order in argument
                                  conf_path_raw_data=conf_path_raw_data,
                                  dir_path_label=dir_path_label,
                                  # todo: check if conf file is always in dir_path_label
                                  patch_shape=common_data_kwargs['patch_shape'],
                                  transform=train_transform,
                                  mag=1, epoch_size=epoch_size_train, label_names=label_names,
                                  knossos_bounds=knossos_bounds['train_bounds'],
                                  # todo: use indices of sorted bounds instead?
                                  label_offset=label_offset)
    valid_dataset = KnossosLabels(label_order=label_order, conf_path_label=conf_path_label,
                                  conf_path_raw_data=conf_path_raw_data,
                                  dir_path_label=dir_path_label,
                                  patch_shape=common_data_kwargs['patch_shape'],
                                  transform=valid_transform,
                                  mag=1, epoch_size=epoch_size_valid, label_names=label_names,
                                  knossos_bounds=knossos_bounds['valid_bounds'],
                                  label_offset=label_offset)

    if dataset_name == "j0251":
        preview_dataset = KnossosRawData(conf_path=conf_path_raw_data,
                                         patch_shape=preview_shape,  # [z]yx
                                         transform=valid_transform,
                                         bounds=tuple2bounds(BOUNDS_J0251),  # xyz
                                         mag=1,
                                         mode="disk")
    else:
        raise NotImplementedError

    preview_loader = DataLoader(preview_dataset, batch_size=1, shuffle=True)  # todo: different batch size?
    preview_batch = next(iter(preview_loader))['inp']
    return train_dataset, valid_dataset, preview_batch


def get_conf_dirs(dataset):
    if dataset != "j0251":
        raise NotImplementedError

    if os.getenv('CLUSTER') == 'WHOLEBRAIN':
        conf_path_label = '/wholebrain/songbird/j0251/groundtruth/j0251.conf'
        conf_path_raw_data = '/wholebrain/songbird/j0251/j0251_72_clahe2/mag1/knossos.conf'
        dir_path_label = '/wholebrain/songbird/j0251/groundtruth/segmentation_gt'  # todo: add ? /segmentation_gt
    else:
        # only applicable if wholebrain mount is in the home directory
        # todo: mention the configuration for local run?
        conf_path_label = str(Path.home()) + '/scratch/groundtruth/j0251.conf'
        conf_path_raw_data = str(
            Path.home()) + '/wholebrain_mount/wholebrain/songbird/j0251/j0251_72_clahe2/mag1/knossos.conf'
        dir_path_label = str(Path.home()) + '/scratch/groundtruth'
    return conf_path_label, conf_path_raw_data, dir_path_label


class MixedDataset(Dataset):
    def __init__(self, transform, training: bool, datatypes=("golgi", "er"), datatypes_weight=(0.5, 0.5),
                 aniso_factor=2,
                 size=10,
                 **common_data_kwargs):
        self.datasets = []
        for datatype in datatypes:
            input_h5data, target_h5data, valid_indices = get_target_data_indices_j0126(datatype)
            class_dataset = PatchCreator(
                # patch_shape=
                input_sources=[input_h5data[i] for i in range(len(input_h5data)) if (i in valid_indices) - training],
                target_sources=[target_h5data[i] for i in range(len(input_h5data)) if (i in valid_indices) - training],
                train=training,
                epoch_size=size,
                warp_prob=0.2,
                warp_kwargs={
                    'sample_aniso': aniso_factor != 1,
                    'perspective': True,
                    'warp_amount': 1.0,
                },
                transform=transform,
                patch_shape=common_data_kwargs['common_data_kwargs']['patch_shape']
            )
            self.datasets.append(class_dataset)

        self.dataset_weights = np.array(datatypes_weight)
        self.size = size

    def __getitem__(self, idx):
        datatype_id = np.random.choice(np.arange(start=0, stop=len(self.datasets)), p=self.dataset_weights)
        sample = self.datasets[datatype_id].__getitem__(idx)
        assert len(np.unique(sample['target'])) <= 2  # background and that class
        # sample['target'] = torch.stack(
        #    [sample['target'], torch.ones_like(sample['target']) * (datatype_id + 1)])  # todo
        # TensorList([sample['target'], torch.ones_like(sample['target'])*(datatype_id+1)])#torch.stack([sample['target'], torch.ones_like(sample['target'])*(datatype_id+1)]) #todo: this is a bit wasteful? #{'target_tensor': sample['target'], 'target_id':datatype_id}#TargetModule(sample['target'], datatype_id)
        # sample['target'][0][sample['target'][0] != 0] = datatype_id + 1  # 0 is for background #todo: don't change here!
        sample['class'] = datatype_id + 1
        return sample

    def __len__(self):
        return self.size

    # def get_ratios(self):


def get_local_path():
    if os.getenv('CLUSTER') == 'WHOLEBRAIN':
        local_path = ""
    else:
        local_path = str(Path.home()) + "/wholebrain_mount"
    return local_path


def tuple2bounds(coordinate_list):
    return coordinate_list[:3], coordinate_list[3:]
