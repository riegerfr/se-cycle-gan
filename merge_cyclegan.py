import argparse

import torch

from secgan.dataset_specific.dataloading import get_local_path

parser: argparse.ArgumentParser = argparse.ArgumentParser(description='merge cyclegan.')

parser.add_argument(
    '-mp', '--model-path', metavar='PATH',
    default='/wholebrain/scratch/anushkav/se_cycle_gan_training/20-09-07_01-20-03-289631/epoch_664.pt',
    help='path to the model.pt file of the segmentation network'
)

args = parser.parse_args()

print(args)

local_path = get_local_path()

state = torch.load(local_path + args.model_path, map_location="cpu")

model = torch.nn.Sequential(state['G_Y2X'], state['S_X']).eval()

inp = torch.ones(2, 1, 30, 30, 30)

with torch.no_grad():
    traced_cell = torch.jit.trace(model, (inp))
torch.jit.save(traced_cell, local_path + args.model_path + "merged.pts")
# torch.save(model, local_path+args.model_path+"merged.pt")
#
# test_loaded = torch.load(local_path + args.model_path + "merged.pt")
#
# print(test_loaded)
#
# inp = torch.ones(2,1, 50,50,50)
# outp = test_loaded(inp)
# print(outp)
