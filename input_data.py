import argparse
import os
from typing import Sequence

import h5py
import matplotlib.pyplot as plt
import numpy as np
import torch
from elektronn3.data import PatchCreator, transforms, utils
from elektronn3.data.knossos import KnossosRawData
from elektronn3.data.sources import HDF5DataSource

parser = argparse.ArgumentParser(description='getting info on input data')
parser.add_argument('-dt', '--datatype', type=str, default='synapsetype',
                    help='datatype on which the segmenter is trained'
                    )
args = parser.parse_args()


def get_data_details(X_h5):
    tensor_list = []
    for i in range(X_h5.__len__()):
        tensor_list.append(X_h5.__getitem__(i)['inp'])

    data_h5 = torch.cat(tensor_list)
    img_h5 = data_h5.numpy()[0, data_h5.numpy().shape[1] // 2, :, :]
    plt.imshow(img_h5, cmap='gray')
    plt.savefig('image_h5.png')

    print('max value: ', torch.max(data_h5))
    print('min value: ', torch.min(data_h5))
    print('type value: ', torch.max(data_h5).dtype)
    print('std_mean value: ', torch.std_mean(data_h5))


def get_num_classes(targets: Sequence[np.ndarray]):
    targets = np.concatenate([
        utils._to_full_numpy(target).flatten()  # Flatten every dim except C
        for target in targets
    ])
    classes = np.arange(0, targets.max() + 1)
    # Count total number of labeled elements per class
    num_labeled = np.array([val / sum([
        np.sum(np.equal(targets, c))
        for c in classes
    ]) for val in [
                                np.sum(np.equal(targets, c))
                                for c in classes
                            ]], dtype=np.float32)

    return num_labeled


print("Starting input_data.py ........")

common_transforms_h5 = [
    transforms.SqueezeTarget(dim=0),
    # transforms.Normalize(mean=dataset_mean, std=dataset_std)
]
common_transforms_knossos = [
    transforms.SqueezeTarget(dim=0),
    transforms.Lambda(lambda x, y: (x / 255., y)),
    # transforms.Normalize(mean=dataset_mean, std=dataset_std)
]
train_transform_h5 = transforms.Compose(common_transforms_h5 + [
    # transforms.RandomRotate2d(prob=0.9),
    transforms.RandomGrayAugment(channels=[0], prob=0.3),
    # transforms.RandomGammaCorrection(gamma_std=0.25, gamma_min=0.25, prob=0.3),
    # transforms.AdditiveGaussianNoise(sigma=0.1, channels=[0], prob=0.3),
])
valid_transform_h5 = transforms.Compose(common_transforms_h5 + [])

train_transform_knossos = transforms.Compose(common_transforms_knossos)
common_data_kwargs = {  # Common options for training and valid sets.
    'aniso_factor': 2,
    'patch_shape': (49, 199, 199),  # (50,200,200),
    # 'offset': (8, 20, 20),
    # 'in_memory': True  # Uncomment to avoid disk I/O (if you have enough host memory for the data)
}

print('Getting data from ', args.datatype)
if args.datatype == 'cellorganelle':
    print("Using patch shape of ", common_data_kwargs['patch_shape'])

    # check index id corresponding to the file in the end comments
    valid_indices = [7]

    print("Securing data from h5 files with valid index id 2")

    data_root = '/wholebrain/songbird/j0126/GT/cellorganelle_gt/'
    fnames_data = sorted([f for f in os.listdir(data_root) if f.endswith('e3.h5') and f.startswith('d')])
    input_h5data = [(os.path.join(data_root, f), 'raw') for f in fnames_data]
    # fnames_labels = sorted([f for f in os.listdir(data_root) if f.endswith('e3.h5') and f.startswith('l')])
    # target_h5data = [(os.path.join(data_root, f), 'label') for f in fnames_labels]

    # Calculating the shape for all the h5 files
    for i, data in enumerate(input_h5data):
        inp_source = HDF5DataSource(fname=data[0], key=data[1], in_memory=False)
        print(f"For file {data[0][-10:]} shape is {inp_source.shape}")
        if i in valid_indices:
            valid_h5 = HDF5DataSource(fname=data[0], key=data[1], in_memory=False)

    # Saving an h5 file locally with length of patch shape
    f = h5py.File(f'unet_input.h5', 'w')
    f.create_dataset(name="raw", data=valid_h5[:199, :199, :49], dtype='f4')
    f.close()

    # reading locally saved h5file
    local_root = './'
    fnames_local = sorted([f for f in os.listdir(local_root) if f.endswith('unet_input.h5')])
    input_h5data_local = [(os.path.join(local_root, f), 'raw') for f in fnames_local]

    # currently reading h5 files from cluster change to locally if required
    print("Creating X_h5 for getting data used for the segmenter")
    X_h5 = PatchCreator(
        input_sources=[input_h5data[i] for i in range(len(input_h5data))],
        # target_sources=[target_h5data[i] for i in range(len(input_h5data)) if i in valid_indices],
        train=False,
        epoch_size=10,
        warp_prob=0,
        warp_kwargs={
            'sample_aniso': 2,
            'perspective': True,
            'warp_amount': 1.0,
        },
        transform=train_transform_h5,
        **common_data_kwargs
    )

    print('Details from h5py file')
    get_data_details(X_h5)

    # data from knossos used in se_cycle_gan

    print("Securing data from knossos and creating X_knossos")

    X_knossos = KnossosRawData(conf_path=  # '/Users/anushkavashishtha/e1088_small/mag1/knossos.conf',
                               '/wholebrain/songbird/j0126/areaxfs_v5/knossosdatasets/mag1/knossos.conf',
                               patch_shape=common_data_kwargs['patch_shape']
                               , transform=train_transform_knossos,
                               bounds=  # ((3577, 2850, 4950), (3877, 3150, 5050)),
                               ((4536, 515, 969), (4736, 715, 1019)),  # for id = 2
                               # ((969,880,941),(1134,1000,1016)), for debugging locally
                               mag=1, in_memory=True, epoch_size=10,
                               # vx size (200,200,50) in bounds
                               # (300,300,100) patch size for id = 25
                               disable_memory_check=False, verbose=False)

    print('Details from knossos dataset')
    get_data_details(X_knossos)


elif args.datatype == 'synapsetype':

    print("Using patch shape of ", common_data_kwargs['patch_shape'])
    data_root = '/wholebrain/songbird/j0126/GT/synapsetype_gt/Segmentierung_von_Synapsentypen_v4/'
    fnames = sorted([f for f in os.listdir(data_root) if f.endswith('.h5')])
    input_h5data = [(os.path.join(data_root, f), 'raw') for f in fnames]
    target_h5data = [(os.path.join(data_root, f), 'label') for f in fnames]
    valid_indices = [2]

    train_dataset = PatchCreator(
        input_sources=[input_h5data[i] for i in range(len(input_h5data)) if i not in valid_indices],
        target_sources=[target_h5data[i] for i in range(len(input_h5data)) if i not in valid_indices],
        train=True,
        epoch_size=10,
        warp_prob=0.2,
        warp_kwargs={
            'sample_aniso': 2,
            'perspective': True,
            'warp_amount': 0.8,
        },
        transform=train_transform_h5,
        **common_data_kwargs
    )

    valid_dataset = None if not valid_indices else PatchCreator(
        input_sources=[input_h5data[i] for i in range(len(input_h5data)) if i in valid_indices],
        target_sources=[target_h5data[i] for i in range(len(input_h5data)) if i in valid_indices],
        train=False,
        epoch_size=10,  # How many samples to use for each validation run
        warp_prob=0,
        warp_kwargs={'sample_aniso': 2},
        transform=valid_transform_h5,
        **common_data_kwargs
    )

    # print('Details from train_dataset file')
    # get_data_details(train_dataset)
    # print('Details from valid_dataset file')
    # get_data_details(valid_dataset)

    # num_labeled = [(i,get_num_classes([target])) for i,target in enumerate(train_dataset.targets)]
    for i in range(len(input_h5data)):
        temp_dataset = PatchCreator(
            input_sources=[input_h5data[i]],
            target_sources=[target_h5data[i]],
            train=True,
            epoch_size=10,
            warp_prob=0.2,
            warp_kwargs={
                'sample_aniso': 2,
                'perspective': True,
                'warp_amount': 0.8,
            },
            transform=train_transform_h5,
            **common_data_kwargs
        )
        num_labeled_class = get_num_classes(temp_dataset.targets)
        print(f"Number of labeled elements per class {i} are {num_labeled_class}")

    class_weights = torch.tensor(utils.calculate_class_weights(train_dataset.targets))
    print(class_weights)

# experiment name "j0126_realigned_mag1";
# boundary x 10664;
# boundary y 10913;
# boundary z 5700;
# scale x 10.0;
# scale y 10.0;
# scale z 20.0;
# magnification 1;

# Securing data from h5 files with valid index id  [2]
# 0 ('/wholebrain/songbird/j0126/GT/cellorganelle_gt/d_0_e3.h5', 'raw')
# 1 ('/wholebrain/songbird/j0126/GT/cellorganelle_gt/d_19_e3.h5', 'raw')
# 2 ('/wholebrain/songbird/j0126/GT/cellorganelle_gt/d_1_e3.h5', 'raw')
# 3 ('/wholebrain/songbird/j0126/GT/cellorganelle_gt/d_20_e3.h5', 'raw')
# 4 ('/wholebrain/songbird/j0126/GT/cellorganelle_gt/d_25_e3.h5', 'raw')
# 5 ('/wholebrain/songbird/j0126/GT/cellorganelle_gt/d_28_e3.h5', 'raw')
# 6 ('/wholebrain/songbird/j0126/GT/cellorganelle_gt/d_29_e3.h5', 'raw')
# 7 ('/wholebrain/songbird/j0126/GT/cellorganelle_gt/d_2_e3.h5', 'raw')
# 8 ('/wholebrain/songbird/j0126/GT/cellorganelle_gt/d_3_e3.h5', 'raw')
# 9 ('/wholebrain/songbird/j0126/GT/cellorganelle_gt/d_4_e3.h5', 'raw')
# 10 ('/wholebrain/songbird/j0126/GT/cellorganelle_gt/d_5_e3.h5', 'raw')
# 11 ('/wholebrain/songbird/j0126/GT/cellorganelle_gt/d_6_e3.h5', 'raw')

#
# # These statistics are computed from the training dataset.
# # Remember to re-compute and change them when switching the dataset.
# dataset_mean = (0.60082027636094304,)  # todo: (and this includes validation!!!) todo: remove this?
# dataset_std = (0.17261124432753927,)
# # Class weights for imbalanced dataset
# class_weights = torch.tensor([0.99452304, 0.00165561, 0.00382135]).to(
#     self.device)  # todo: not needed (unless for evaluating feasibility of this approach, Y also has labels)
# optimizer_state_dict = None
#
# ########## data loading from h5 files

# if os.getenv('CLUSTER') == 'WHOLEBRAIN':  # Use bigger, but private data set.
#
#     fnames_x = sorted([f for f in os.listdir(self.args.data_root_x) if f.endswith('.h5')])
#     fnames_y = sorted([f for f in os.listdir(self.args.data_root_y) if f.endswith('.h5')])
#     input_h5data_x = [(os.path.join(self.args.data_root_x, f), 'raw') for f in fnames_x]
#     input_h5data_y = [(os.path.join(self.args.data_root_y, f), 'raw') for f in fnames_y]
#
# else:
#     # used for debugging locally
#     data_root = './'
#     fnames_x = sorted([f for f in os.listdir(data_root) if f.endswith('_x.h5')])
#     fnames_y = sorted([f for f in os.listdir(data_root) if f.endswith('_y.h5')])
#     input_h5data_x = [(os.path.join(data_root, f), 'raw_x') for f in fnames_x]
#     input_h5data_y = [(os.path.join(data_root, f), 'raw_y') for f in fnames_y]
#
# valid_indices = [self.args.valid_indices]
# X_train =     PatchCreator(
#     input_sources=[input_h5data_x[i] for i in range(len(input_h5data_x)) if i not in valid_indices],
#     # target_sources=[target_h5data_x[i] for i in range(len(target_h5data_x)) if i not in valid_indices],
#     train=True,
#     epoch_size=self.args.epoch_size,
#     warp_prob=0.2,
#     warp_kwargs={
#         'sample_aniso': self.common_data_kwargs['aniso_factor'] != 1,
#         'perspective': True,
#         'warp_amount': 0,  # 1.0,
#     },
#     transform=train_transform,
#     **self.common_data_kwargs
# )  # todo: load data

# Y_train = PatchCreator(
#     input_sources=[input_h5data_y[i] for i in range(len(input_h5data_y)) if i not in valid_indices],
#     # target_sources = [target_h5data[i] for i in range (len (input_h5data)) if i not in valid_indices],
#     train=True,
#     epoch_size=self.args.epoch_size,
#     warp_prob=0.2,
#     warp_kwargs={
#         'sample_aniso': self.common_data_kwargs['aniso_factor'] != 1,
#         'perspective': True,
#         'warp_amount': 0,  # 1.0,
#     },
#     transform=train_transform,
#     **self.common_data_kwargs
# )
# X_valid = None if not valid_indices else PatchCreator(
#     input_sources=[input_h5data_x[i] for i in range(len(input_h5data_x)) if i in valid_indices],
#     # target_sources=[target_h5data[i] for i in range(len(input_h5data)) if i in valid_indices],
#     train=False,
#     epoch_size=self.args.epoch_size,  # How many samples to use for each validation run
#     warp_prob=0,
#     warp_kwargs={'sample_aniso': self.common_data_kwargs['aniso_factor'] != 1},
#     transform=valid_transform,
#     **self.common_data_kwargs
# )
# Y_valid = None if not valid_indices else PatchCreator(
#     input_sources=[input_h5data_y[i] for i in range(len(input_h5data_y)) if i in valid_indices],
#     # target_sources=[target_h5data[i] for i in range(len(input_h5data)) if i in valid_indices],
#     train=False,
#     epoch_size=self.args.epoch_size,
#     # How many samples to use for each validation run #todo: different arg epoch size val
#     warp_prob=0,
#     warp_kwargs={'sample_aniso': self.common_data_kwargs['aniso_factor'] != 1},
#     transform=valid_transform,
#     **self.common_data_kwargs
# )
##########################
